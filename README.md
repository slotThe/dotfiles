# Dotfiles

What could you possibly want to know about a random dotfiles repo? Feel
free to use this stuff for anything you want, respecting the LICENSE of
course ":D". HFHF

## Specs

  - **OS**: [NixOS](https://nixos.org/)
  - **WM**: [xmonad](https://xmonad.org/)
  - **Bar**: [xmobar](https://codeberg.org/xmobar/xmobar)
  - **Editor**: [GNU Emacs](https://www.gnu.org/software/emacs/)
  - **Launcher**: [hmenu](https://gitlab.com/slotThe/hmenu)
    (wrapper around [dmenu](https://tools.suckless.org/dmenu/))

## READMEs for individual programs

The configuration for some programs is so extensive that an individual
README makes sense for them. Much better than trying to cram it all into
this document!

  - [XMonad](xmonad/README.md)
  - [Emacs](emacs/README.md)

## Removed Configs

Go dig in the `git` history if you want :)

  - [(neo)vim](https://neovim.io) — replaced by the one and only GNU Emacs
  - [(neo)mutt](https://neomutt.org/) — replaced by `notmuch.el`
