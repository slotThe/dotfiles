# Hopf Emacs

There are many like it, but this one is mine.

I use Emacs for (too) many things: organising [my
research][blog:research-workflow], knowledge management, writing code
(mainly Haskell), writing prose, email, IRC, …  Needless to say, I
basically live inside of it.  As a result, my config has accumulated
quite a few useful bits and pieces—and a lot of cruft, no doubt—which
may or may not be of interested to other people; hence, this document.

I've written about various aspects of this configuration quite a bit on my website.
An article where I try to collect all of these bits and pieces is available [here][blog:potpourri].

## Quick overview

My [init.el](./init.el) contains a mostly self-explanatory list of local
imports (`org-config.el`, `email.el`, …) which can be perused
individually.  Below I've also collected some custom functions in my
config that could be of interest to other people.  This mostly
concentrates on more homemade pieces: stuff that's not just
`use-package` declarations of packages.

- I use Nix's
  [emacs-overlay](https://github.com/nix-community/emacs-overlay)
  to compile Emacs from source,
  and depend on certain function and settings
  that are not available in released versions.
  Evaluating the `system-configuration-features` variables returns

  ``` console
  $ (symbol-value 'system-configuration-features)
  CAIRO DBUS FREETYPE GIF GLIB GMP GNUTLS GPM GSETTINGS HARFBUZZ JPEG LIBOTF LIBSELINUX LIBSYSTEMD LIBXML2 M17N_FLT MODULES NATIVE_COMP NOTIFY INOTIFY PDUMPER PNG RSVG SECCOMP SOUND SQLITE3 THREADS TIFF TOOLKIT_SCROLL_BARS TREE_SITTER WEBP X11 XAW3D XDBE XIM XINPUT2 XPM LUCID ZLIB
  ```

- I use use-package's new [`:vc` keyword][emacs:vc] in order to install
  certain packages directly from their upstream source.  This uses the
  new (in Emacs 29) `package-vc.el` library.  This is very new
  functionality, but a library emulating this exists with
  [vc-use-package].

- In particular, I also depend on an unreleased version of Org.
  This is to leverage the new LaTeX preview mechanism.
  Thankfully, this is made almost trivial with `package-vc.el`!

- While I start Emacs as a server so that bringing up a frame is near
  instantaneous, some care was taken with regard to startup time.  This
  is mostly related to lazy-loading as many things as possible via
  `use-package`—which I make use of quite a bit—and disabling visual
  gimmicks in the [early-init.el](./early-init.el) file.

  The resulting startup time is decent enough:

  ``` console
  $ rg "^\s*[^;]*(\(use-package|\(require)" early-init.el init.el lisp/ lisp-pkgs/ | wc -l
  173

  $ emacs --eval "(progn (princ (emacs-init-time) #'external-debugging-output) (kill-emacs))"
  0.388776 seconds
  ```

- I've written the [`defrepeatmap` macro][defrepeatmap] to more
  ergonomically specify repeat maps.  Usage examples are given, for
  example, [here][repeat-map:ex1] and [here][repeat-map:ex2].

- The `query-replace` function has been augmented to support [multiple
  matches].  A writeup of this, including some usage examples, is
  available [here][blog:query-replace].  It is also packaged on
  [GitHub][gh:query-replace], as well as [GitLab][gl:query-replace].

- Over time I've accumulated some utility keybindings that I find very
  useful, like a vim-inspired [invert-char] function, a dwim-style
  version of [back-to-indentation], as well as a
  [function][key:backward-kill-dwim] that unifies `backward-kill-word`,
  `join-line`, and a few other things.

- My [ERC setup](./lisp/erc-config.el) contains hacks to, for example,
  [mark the current frame as urgent][erc:urgent] when I get highlighted
  or to show the [number of users][erc:prompt] in the prompt.

- For inserting links when composing markdown documents (e.g., GitHub
  and Reddit posts) I have a way too complicated
  [mechanism][gfm:link-inserter] to prompt me for items that I need to
  link to with some frequency, like XMonad modules or links to my
  horrible blog posts.

- In my [programming](/lisp/programming.el) and, in particular,
  [lsp-mode][lsp] configuration one can find some custom code for
  inserting [type signatures][lsp:haskell:type-sig] and
  [pragmas][haskell:pragma] in Haskell, as well as properly showing
  [types in the modeline][lsp:types-modeline] in Haskell and Rust
  (working around [this issue][gh:type-sig-issue]).

- Writing a lot of LaTeX, I have many convenience functions to make text
  entry and other often used tasks a bit more enjoyable.  For example,
  this includes quickly opening [letter templates][LaTeX:letter],
  dwim-inserting [blanks][LaTeX:dwim-blank], [folding commutative
  diagrams][LaTeX:folding], and a hacked together
  [self-insert-command][LaTeX:self-insert] function so I don't have to
  type as many dollars.  All of the maths entry stuff is in
  [latex-math.el](./lisp/latex-math.el) so I can use it from both
  `org-mode` and `TeX-mode`.

- My org config contains a few niceties like quickly [inserting images]
  in `org-roam` or [working around][org:export-roam] that exporting
  roam-style references doesn't really work all that well.

## About the name

I think that configs need a name, if only so configuration files don't
have to awkwardly append `-config` to the name of a file—can't have
something called `org.el` as an Org config! I chose "Hopf" because I
like [monoidal categories][nlab:monoidal-category] and, due to that,
[Hopf algebras][nlab:hopf-algebra] and [Hopf monads][nlab:hopf-monad] :)

[LaTeX:dwim-blank]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/latex-config.el#L46
[LaTeX:folding]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/latex-config.el#L55
[LaTeX:letter]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/latex-config.el#L17
[LaTeX:self-insert]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/latex-config.el#L67
[back-to-indentation]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/keybindings.el#L94
[blog:emacs-xmonad]: https://tony-zorman.com/posts/2022-05-25-calling-emacs-from-xmonad.html
[blog:potpourri]: https://tony-zorman.com/posts/emacs-potpourri.html
[blog:query-replace]: https://tony-zorman.com/posts/query-replace/2022-08-06-query-replace-many.html
[blog:research-workflow]: https://tony-zorman.com/posts/phd-workflow/2022-05-01-my-phd-workflow.html
[defrepeatmap]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/sensible-defaults.el#L68
[emacs:vc]: https://git.savannah.gnu.org/cgit/emacs.git/commit/?id=2ce279680bf9c1964e98e2aa48a03d6675c386fe
[erc:prompt]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/erc-config.el#L47
[erc:urgent]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/erc-config.el#L30-L45
[gfm:link-inserter]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/programming.el#L151
[gh:query-replace]: https://github.com/slotThe/query-replace-many
[gh:type-sig-issue]: https://github.com/emacs-lsp/lsp-haskell/issues/151
[gl:query-replace]: https://gitlab.com/slotThe/query-replace-many
[haskell:pragma]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/programming.el#L36
[here]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/window-management.el#L43
[inserting images]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/org-setup.el#L130
[invert-char]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/keybindings.el#L79
[key:backward-kill-dwim]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/keybindings.el#L109
[lsp:haskell:type-sig]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/programming.el#L293
[lsp:types-modeline]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/programming.el#L263-L291
[lsp]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/programming.el#L204
[multiple matches]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/keybindings.el#L126-L169
[nlab:hopf-algebra]: https://ncatlab.org/nlab/show/Hopf+algebra
[nlab:hopf-monad]: https://ncatlab.org/nlab/show/Hopf+monad
[nlab:monoidal-category]: https://ncatlab.org/nlab/show/monoidal+category
[org:export-roam]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/org-setup.el#L144
[repeat-map:ex1]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/window-management.el#L43
[repeat-map:ex2]: https://gitlab.com/slotThe/dotfiles/-/blob/77393d030021a3524c03f22bbb4a4ca75965a9fd/emacs/.config/emacs/lisp/keybindings.el#L43
[vc-use-package]: https://github.com/slotThe/vc-use-package
[xmonad]: https://xmonad.org/
