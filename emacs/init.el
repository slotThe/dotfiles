;;; -*- lexical-binding: t; -*-

;;; Commentary

;; Tested and working with Emacs Git:
;;
;; + `system-configuration-options': "--prefix=«some-nix-store-path»
;;   --disable-build-details --with-modules --with-x-toolkit=no --with-xft
;;   --with-cairo --without-toolkit-scroll-bars --with-native-compilation
;;   --with-tree-sitter --with-xinput2 --with-small-ja-dic
;;   --without-compress-install"
;;
;; I'm using my own spin of colemak-ansi-dh (or whatever its name is) as
;; my keyboard layout, so your mileage on the binds may vary, though
;; most of them concentrate on mnemonics anyways.
;;
;; Sources of great inspiration have been:
;;   - https://github.com/hrs/dotfiles
;;   - https://github.com/joedicastro/dotfiles
;;   - https://github.com/munen/emacs.d
;;   - https://github.com/LinFelix/dotEmacs
;;   - https://github.com/belak/dotfiles
;;   - https://zge.us.to/emacs.d.html
;;   - https://leahneukirchen.org/dotfiles/.emacs

;;; Code

;; My elisp files.
;; 24dec2019 ho-ho-ho
(add-to-list 'load-path (concat user-emacs-directory "elpa/org-mode/lisp/"))
(add-to-list 'load-path (concat user-emacs-directory "lisp/"))
(add-to-list 'load-path (concat user-emacs-directory "lisp-pkgs/"))

;; Set custom file :>
(setq custom-file (make-temp-file "emacs-custom.el_"))

(set-language-environment "UTF-8")
(setq default-input-method nil)       ; This gets set by the above—dont'.

(setq         vc-follow-symlinks t)     ; Always follow symlinks without prompting.
(setq-default default-directory "~/")   ; Default directory for current buffer.

;;;; Packages

;; Set up package repositories.
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") 'append)
(setq package-install-upgrade-built-in t
      package-native-compile t
      native-comp-async-report-warnings-errors nil)
;; Initial `use-package' configuration.
(setq use-package-enable-imenu-support t)
(require 'use-package)
(setq use-package-always-ensure    t  ; :ensure t by default
      use-package-always-defer     t  ; :defer  t by default
      use-package-vc-prefer-newest t) ; :rev :newest by default

;;;; Garbage Collector Things

(use-package gcmh                       ; 28mar2022, 13feb2024
  :demand t
  :config
  (defun gcmh-register-idle-gc ()
    "Register a timer to run `gcmh-idle-garbage-collect'.
Cancel the previous one if present."
    (unless (eq this-command 'self-insert-command)
      (let ((idle-t (if (eq gcmh-idle-delay 'auto)
                        (* gcmh-auto-idle-delay-factor gcmh-last-gc-time)
                      gcmh-idle-delay)))
        (if (timerp gcmh-idle-timer)
            (timer-set-time gcmh-idle-timer idle-t)
          (setf gcmh-idle-timer
                (run-with-timer idle-t nil #'gcmh-idle-garbage-collect))))))
  (setq gcmh-idle-delay 'auto
        gcmh-high-cons-threshold (* 32 1024 1024)
        gcmh-verbose nil)
  (gcmh-mode 1))

;;; Include Local Files
;; 06sep2021 delete evil mode Oo let's see how
;;           long it takes until I regret this :]

(use-package dash  :demand)
(use-package s     :demand)
(use-package f     :demand)
(use-package llama :demand)

(require 'hopf-sensible-defaults)

(require 'hopf-startup-screen)
(require 'hopf-theming)
(require 'hopf-keybindings)

(require 'hopf-completion)
(require 'hopf-window-management)
(require 'hopf-dired)
(require 'hopf-global-packages)

(require 'hopf-email)
(require 'hopf-rss)
(require 'hopf-erc)

(require 'hopf-programming)
(require 'hopf-pretty-symbols)
(require 'hopf-eshell)
(require 'hopf-latex-math)
(require 'hopf-latex)
(require 'hopf-org)

;; Private things included specific directory names etc. I don't want to
;; publish (nothing too interesting anyways).
(load (concat user-emacs-directory "private-stuff.el"))
