;;; completion.el --- My completion setup -*- lexical-binding: t; -*-

;;; The cool new completion framework on the block is several individual
;;; packages!  Apparently modularity is nice, who knew.
;; 26oct2021

;;; `vertico'
(use-package vertico
  :demand t
  :config (vertico-mode)
  :preface
  (defun slot/mini-down ()
    "Move to next candidate in minibuffer."
    (interactive)
    (with-selected-window (active-minibuffer-window)
      (execute-kbd-macro [down])))

  (defun slot/mini-up ()
    "Move to previous candidate in minibuffer."
    (interactive)
    (with-selected-window (active-minibuffer-window)
      (execute-kbd-macro [up])))
  :bind (("C-x C-m" . execute-extended-command)
         ("C-x f"   . find-file)
         ("C-c n"   . slot/mini-down)
         ("C-c p"   . slot/mini-up)
         :map vertico-map
         ("C-j"   . vertico-exit-input)
         ("C-M-n" . vertico-next-group)
         ("C-M-p" . vertico-previous-group))
  :config
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
  :custom
  (vertico-resize t)
  (vertico-cycle t)
  (enable-recursive-minibuffers t) ; Minibuffers inside minibuffers!
  ;; Hide M-x commands that do not work in current mode
  (read-extended-command-predicate #'command-completion-default-include-p))

(use-package vertico-directory          ; 26mar2022
  :after vertico
  :load-path "~/.config/emacs/elpa/vertico/extensions/"
  :ensure nil
  :demand t
  :bind (:map vertico-map
              ("DEL" . vertico-directory-delete-char)
              ("C-w" . vertico-directory-delete-word))
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(use-package vertico-reverse
  :after vertico
  :load-path "~/.config/emacs/elpa/vertico/extensions/"
  :ensure nil
  :demand t
  :config (vertico-reverse-mode))

;;; `orderless'
(use-package orderless
  :custom
  (completion-styles '(orderless partial-completion basic))
  (completion-category-defaults nil)
  (orderless-component-separator " +"))

;;; `marginalia'
(use-package marginalia
  :demand t
  :custom (marginalia-margin-threshold 110)
  :config
  (marginalia-mode)
  (add-to-list 'marginalia-annotator-registry '(file builtin none)))

;;; `consult'
(use-package consult
  :preface
  (cl-defmethod register-command-info ((_command (eql consult-register-load)))
    (register-command-info #'jump-to-register))
  :bind (("C-M-e" . consult-buffer)
         ("C-x b" . consult-buffer)
         ("M-s r" . consult-ripgrep)
         ("C-M-s" . consult-line)
         ("M-y"   . consult-yank-pop)
         ("M-'"   . consult-register-store)
         ("M-_"   . consult-register-load)
         ;; going to things
         ("M-g i"   . consult-imenu)
         ("M-g M-g" . consult-goto-line)
         ("M-g o"   . consult-outline)
         ("M-g m"   . consult-mark)
         ("M-g k"   . consult-global-mark))
  :config
  (advice-add 'consult-ripgrep :around
    (lambda (rg &optional dir initial)
      "Disable `vertico-reverse-mode' for ripgrep."
      (interactive "P")
      (vertico-reverse-mode -1)
      (condition-case err
          (apply rg dir initial)
        (quit (vertico-reverse-mode)))
      (vertico-reverse-mode)))
  (use-package wgrep :demand)
  ;; No preview for these commands.
  (consult-customize consult-buffer consult-theme
                     :preview-key "M-.")
  :custom
  (register-use-preview nil)
  (consult-line-start-from-top t)
  (consult-ripgrep-args                 ; initial arguments to `rg'
   (unwords
    "rg --null --line-buffered --color=never --smart-case --no-heading"
    "--follow --line-number --with-filename --hidden --glob !.git"
    "--max-columns=1000 --path-separator /"))
  (completion-in-region-function 'consult-completion-in-region)) ; complete M-:

;;; `embark'
(use-package embark
  :bind (("C-."   . embark-act)
         ("C-c e" . embark-export)
         :map embark-region-map
         ("x"     . ix.io-paste-region)
         :map embark-sort-map
         ("h"     . haskell-sort-imports))
  :custom
  ;; Replace the key help with a completing-read interface.
  (prefix-help-command #'embark-prefix-help-command)
  :config
  ;; Hide the mode line of the Embark live/completions buffers.
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

(use-package embark-consult
  :demand
  :after consult)

;;; `corfu': it's like company, but more indie!
(use-package corfu                      ; 05jun2022, 15sep2023
  :after orderless
  :demand t
  :bind (:map corfu-map
              ("M-SPC" . nil)           ; `hippie-expand' needs this
              ("M-/"   . corfu-insert-separator))
  :preface
  (advice-add #'corfu-insert :after     ; Fix corfu in eshell
    (lambda (&rest _)
      "Send completion candidate when inside eshell."
      (when (and (derived-mode-p 'eshell-mode) (fboundp 'eshell-send-input))
        (eshell-send-input))))
  :hook
  (corfu-mode . corfu-history-mode)
  (corfu-mode . corfu-indexed-mode)
  (corfu-mode . corfu-popupinfo-mode)
  :custom
  (corfu-separator (string-to-char orderless-component-separator))
  (corfu-cycle t)
  (corfu-auto t)
  (global-corfu-modes '((not LaTeX-mode org-mode message-mode markdown-mode) t))
  (completion-cycle-threshold 3)        ; TAB cycle if only a few candidates
  (tab-always-indent 'complete)         ; Indentation + completion using TAB
  (ispell-alternate-dictionary (getenv "WORDLIST"))
  (corfu-popupinfo-delay 0.2)
  :config
  (global-corfu-mode))

;;;; `cape'
(use-package cape                       ; 15sep2023
  :after corfu
  :demand t
  :config
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-abbrev))

(provide 'hopf-completion)
