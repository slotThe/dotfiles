;;; dired-config.el --- Additional configuration for `dired' -*- lexical-binding: t; -*-

;;; `Dired'
(use-package dired   ; 08oct2020 better finding, 07nov2020
  :ensure nil
  :hook
  (dired-mode . diredfl-mode)
  (dired-mode . dired-hide-details-mode)
  (dired-mode . olivetti-mode)
  (dired-mode . dired-async-mode)
  :bind (:map dired-mode-map
              ("C-c C-p" . wdired-change-to-wdired-mode)
              ;; Easier to reach _and_ a straight upgrade
              ("!" . dired-do-async-shell-command)
              ;; Reuse the same buffer.
              ("f" . dired-find-alternate-file)
              ("a" . (lambda () (interactive) (find-alternate-file ".."))))
  :custom
  (async-shell-command-buffer 'new-buffer) ; No confirmation
  (async-shell-command-display-buffer nil)   ; No noise
  (dired-mouse-drag-files t)       ; Copy files on mouse drag
  (dired-dwim-target t)            ; Try to guess target directory
  (dired-listing-switches "-Alhv --group-directories-first")
  (dired-recursive-copies 'always) ; On `C', recursively copy by default
  (dired-omit-files       ; Don't show hidden files in `dired-omit-mode'
   "^\\.?#\\|^\\.$\\|^\\.\\.$\\|^\\..*$")
  (dired-garbage-files-regexp    ; Mark these files for deletion on `%&'
   (rx "." (or "aux" "auxlock" "bbl" "blg" "out" "log" "toc" "fdb_latexmk" "synctex.gz" "fls")
       string-end))
  (dired-auto-revert-buffer t)
  (dired-guess-shell-alist-user
   `((,(rx "." (or "gif" "png" "jpg" "jpeg") string-end) "sxiv -a")
     (,(rx "." (or "pdf" "djvu") string-end) "zathura")
     (,(rx "." (or "mp4" "m4v" "webm" "mkv") string-end) "mpv")))
  :config
  (use-package diredfl)                 ; More colorful output
  (use-package dired-x                  ; Cool Extra functionality
    :demand
    :ensure nil))

(use-package dired-narrow
  :after dired
  :bind (:map dired-mode-map
              ("/" . dired-narrow-fuzzy)))

;;; Deleting Files and Buffers
;; 28aug2020

;; Rename/delete files and the delete associated buffers if needed.
(defun slot/rename-kill-current-file ()
  "Rename the current file, ask to delete the old buffer, and open
the new file."
  (interactive)
  (let ((new-file-name (read-file-name "Enter new file name: ")))
    (progn
      (save-buffer)
      (rename-file buffer-file-name new-file-name)
      (when (y-or-n-p "Delete the old buffer?")
        (kill-current-buffer))
      (find-file new-file-name))))

(defun slot/kill-delete-current-file ()
  "Delete the current file and ask to do the same for the
still-open buffer.  This is the nuclear option."
  (interactive)
  (delete-file buffer-file-name)
  (when (y-or-n-p "Delete the old buffer?")
    (kill-current-buffer)))

(provide 'hopf-dired)
