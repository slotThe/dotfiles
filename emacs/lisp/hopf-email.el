;;; email.el --- Settings for my email client -*- lexical-binding: t; -*-

;; 29jun2023 unify with mailboxes

(defconst email-domain         "mail@tony-zorman.com")
(defconst email-uni            "tony.zorman@tu-dresden.de")
(defconst email-mailbox        "tonyzorman@mailbox.org")
(defconst email-mailbox-slot   "soliditsallgood@mailbox.org")
(defconst slot/email-signature "Tony Zorman | https://tony-zorman.com")

(setq user-full-name    "Tony Zorman"
      user-mail-address email-domain)

;;; `notmuch'
;; 18apr2020, 06jun2020 notmuch, 01oct2020 move smtp settings to `mailboxes.el', 12jan2021
(use-package notmuch
  :load-path (lambda () (getenv "NOTMUCH_EMACS"))
  :commands notmuch
  :preface
  (defun slot/encrypt-message ()
    (interactive)
    (mml-secure-message-sign-encrypt)
    (notmuch-mua-send-and-exit))

  (defun slot/sign-message ()
    (interactive)
    (mml-secure-message-sign)
    (notmuch-mua-send-and-exit))

  (defun slot/mark-as-deleted (beg end)
    "Mark message as deleted.  Note that this will not actually cause
  the deletion of the message; that job will be done by the mail
  fetching script."
    (interactive (notmuch-search-interactive-region))
    (slot/mark-as '("+delete" "-inbox" "-unread") beg end))

  (defun slot/mark-as-read (beg end)
    "Mark message as read."
    (interactive (notmuch-search-interactive-region))
    (slot/mark-as '("-unread") beg end))

  (defun slot/mark-as (lst beg end)
    (notmuch-search-tag lst beg end)
    (notmuch-search-next-thread))

  ;; XXX: Remove when merged into notmuch
  (defun notmuch-mua-subject-check ()
    (or (save-excursion
          (message-goto-subject)
          (message-beginning-of-header t)
          (not (looking-at-p "[[:blank:]]*$")))
        (y-or-n-p "No subject given – still send?")
        (error "No subject")))

  (defun slot/message-reply (hi bye)
    (cl-flet ((lookup (s)
                (if-let (snip (ignore-errors (yas-lookup-snippet s)))
                    (yas-expand-snippet snip)
                  (insert s))))
      (let ((quote? (re-search-forward "On [A-z]+, .*, .* wrote:"
                                       (point-max) 'no-error)))
        (message-goto-body)
        (lookup hi)
        (open-line (if quote? 2 3))
        (let ((pos (progn (next-line (if quote? 3 2)) (pos-bol))))
          (when quote?
            (delete-region (pos-bol) (pos-bol 2)))
          (end-of-buffer)
          (search-backward "--")
          (open-line 3)
          (next-line)
          (lookup bye)
          (goto-char pos)))))

  (defun slot/message-lieber ()
    (interactive)
    (slot/message-reply "Lieber-Header" "Viele Grüße"))

  (defun slot/message-hi ()
    (interactive)
    (slot/message-reply "Hi," "  Tony"))

  (defun slot/notmuch-syntax-highlight ()
    "Syntax highlight `notmuch-show-mode' like markdown.
This means that, in particular, code indented by four spaces and
GFM-style blocks will be fontified as such."
    (require 'markdown-mode)
    ;; Syntax
    (setq-local syntax-propertize-function #'markdown-syntax-propertize)
    (syntax-propertize (point-max))
    ;; Font locking
    (setq font-lock-defaults
          ;; Notmuch indents threads beyond this at some point.
          `(,(cl-delete-if (lambda (el) (equal 'markdown-match-pre-blocks (car el)))
                           markdown-mode-font-lock-keywords)
            nil nil nil nil
            (font-lock-multiline . t)
            (font-lock-syntactic-face-function . markdown-syntactic-face)
            (font-lock-extra-managed-props
             . (composition display invisible rear-nonsticky
                            keymap help-echo mouse-face))))
    (font-lock-update))

  :hook
  (notmuch-mua-send . notmuch-mua-attachment-check)
  (notmuch-mua-send . notmuch-mua-subject-check)
  (notmuch-show     . slot/notmuch-syntax-highlight)

  :bind ( :map message-mode-map
          ("C-a"     . slot/back-to-indentation)
          :map notmuch-show-mode-map
          ("C-c l"   . slot/capture-mail)
          ("o"       . notmuch-show-interactively-view-part)
          ("C-c C-o" . org-open-at-point)
          ("C-C C-d" . epa-mail-decrypt)
          :map notmuch-message-mode-map
          ("C-c C-e" . slot/encrypt-message)
          ("C-c C-s" . slot/sign-message)
          ("C-c C-c" . notmuch-mua-send-and-exit)
          ("C-c C-v" . message-elide-region)
          ("C-c h"   . slot/message-hi)
          ("C-c l"   . slot/message-lieber)
          :map notmuch-hello-mode-map
          ("s"       . consult-notmuch)
          ("S"       . notmuch-search)
          :map notmuch-search-mode-map
          ("s"       . consult-notmuch)
          ("S"       . notmuch-search)
          ("C-c C-r" . slot/mark-as-read)
          ("D"       . slot/mark-as-deleted)
          ("M-r"     . slot/mark-as-read)
          ;; Swap `r' and `R' for replying.
          :map notmuch-show-mode-map
          ("r"       . notmuch-show-reply)
          ("R"       . notmuch-show-reply-sender)
          :map notmuch-tree-mode-map
          ("r"       . notmuch-tree-reply)
          ("R"       . notmuch-tree-reply-sender))

  :custom
  (message-wide-reply-confirm-recipients t)
  (message-kill-buffer-on-exit t)
  (mml-enable-flowed nil)               ; Don't use f=f for now.
  (notmuch-search-oldest-first nil)     ; Newest threads at the top.
  (notmuch-poll-script "fetch-and-sync-mail.sh")
;;;; Sent mail should go into the appropriate directories.
  (notmuch-fcc-dirs
   `((,email-uni          . "uni/Sent"         )
     (,email-mailbox      . "mailbox/Sent"     )
     (,email-mailbox-slot . "mailbox-slot/Sent")
     (,email-domain       . "domain/Sent"      )))
;;;; Cosmetic things.
  (message-elide-ellipsis ">\n> [… %l lines elided …]\n")
  (notmuch-search-result-format
   '(("date"    . "%12s ")
     ("count"   . "%-7s ")
     ("authors" . "%-30s ")
     ("subject" . "%-70s ")
     ("tags"    . "(%s)")))
;;;; What to show on the "frontpage".
  (notmuch-show-logo nil)
  (notmuch-hello-sections '((lambda () (widget-insert "\n"))
                            notmuch-hello-insert-header
                            notmuch-hello-insert-saved-searches
                            notmuch-hello-insert-alltags))
;;;; Things my mail client announces to the world.
  (mail-host-address "hyperspace")
  (notmuch-mua-user-agent-function
   (lambda ()
     (concat "Notmuch/" (notmuch-cli-version) ", Emacs " emacs-version)))
;;;; Properly format citations.
  (message-citation-line-format   "On %a, %b %d %Y %H:%M, %N wrote:\n"     )
  (message-citation-line-function #'message-insert-formatted-citation-line )
  (notmuch-mua-cite-function      #'message-cite-original-without-signature)
;;;; Signature/Encryption settings.
  (notmuch-crypto-process-mime nil)     ; this can terribly hang Emacs
  (mml-secure-openpgp-sign-with-sender t)
  (mml-secure-openpgp-encrypt-to-self  t) ; so that I can read sent encrypted mails
;;;; Attachments
  (notmuch-mua-attachment-regexp        ; never forget them again!
   "\\(attache?ment\\|attached\\|attach\\|im Anhang\\|i\\.A\\.\\|angehängt\\|hänge\\)")

  :config
  (advice-add 'notmuch-mua-send-and-exit :after
    (lambda ()
      (interactive)
      (shell-command
       (format "notmuch tag -unread from:%s from:%s from:%s from:%s tag:unread"
               email-uni email-mailbox email-mailbox-slot email-domain))))
  (use-package consult-notmuch)
  (use-package org-notmuch
    :demand t
    :vc (:url "https://github.com/slotThe/org-notmuch" :branch "main")
    :preface (defun slot/capture-mail ()
               (interactive)
               (require 'org-notmuch)
               (org-capture nil "m"))))

;;; `smtpmail'
(use-package smtpmail
  :custom
  (auth-sources                 '("~/.authinfo.gpg"))
  (smtpmail-default-smtp-server "msx.tu-dresden.de")
  (smtpmail-smtp-server         "msx.tu-dresden.de")
  (send-mail-function           #'smtpmail-send-it)
  (smtpmail-stream-type         'starttls)
  (message-send-mail-function   #'smtpmail-send-it)
  (smtpmail-smtp-service        587))

;;; `gnus-alias'
(use-package gnus-alias
  :preface
  (defun slot/build-sender (mail)
    (format "Tony Zorman <%s>" mail))
  (defun slot/alternative-smtp (smtp mail)
    (cons "X-Message-SMTP-Method" (format "smtp %s 587 %s" smtp mail)))
  (defun slot/mailbox-smtp (mail)
    (slot/alternative-smtp "smtp.mailbox.org" mail))
  (defconst slot/domain-smtp
    (slot/alternative-smtp "mail.your-server.de" "mail@tony-zorman.com"))
  :hook (message-setup . gnus-alias-determine-identity)
  :custom
  ;; Define identities
  (gnus-alias-identity-alist
   `(("uni"
      nil                               ; Refer to other identities
      ,(slot/build-sender email-uni)    ; Sender address
      nil                               ; Organization header
      nil                               ; Extra headers
      nil                               ; Extra body text
      slot/email-signature)             ; Signature
     ("mailbox"
      nil
      ,(slot/build-sender email-mailbox)
      nil
      (,(slot/mailbox-smtp email-mailbox))
      nil
      slot/email-signature)
     ("mailbox-slot"
      nil
      ,(slot/build-sender email-mailbox-slot)
      nil
      (,(slot/mailbox-smtp email-mailbox-slot))
      nil
      slot/email-signature)
     ("domain"
      nil
      ,(slot/build-sender email-domain)
      nil
      (,slot/domain-smtp)
      nil
      slot/email-signature)))

  ;; Match with default SMTP settings above
  (gnus-alias-default-identity "uni")

  ;; Define rules to match other identities
  (gnus-alias-identity-rules
   `(("mailbox"      ("any" ,(regexp-quote email-mailbox)      both) "mailbox"     )
     ("mailbox-slot" ("any" ,(regexp-quote email-mailbox-slot) both) "mailbox-slot")
     ("domain"       ("any" ,(regexp-quote email-domain)       both) "domain"      ))))

(provide 'hopf-email)
