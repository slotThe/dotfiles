;;; eshell-config.el --- My eshell config -*- lexical-binding: t; -*-
;; 20mar2022, 05jun2022, 07aug2023

(use-package eshell
  :preface
  (defconst slot/eshell-term-name "xterm-256color")

  (defun slot/eshell-exit (&optional arg)
    "Exit eshell and kill the current frame."
    (interactive "P")
    (eshell-write-history)
    (save-buffers-kill-terminal))

  (defun slot/eshell-insert-history ()
    "Interactively insert a history item into eshell."
    (interactive)
    (insert
     (completing-read "Eshell history: "
                      (delete-dups (ring-elements eshell-history-ring)))))

  ;; zoxide integration
  (advice-add 'eshell/cd :around
    (lambda (cd &rest args)
      "On directory change, add the path to zoxide's database."
      (let ((old-path (eshell/pwd))
            (_ (apply cd args))
            (new-path (eshell/pwd)))
        (when (and old-path new-path (not (string= old-path new-path)))
          (shell-command-to-string (concat "zoxide add " new-path))))))
  (defun eshell/n (dir)
    "Navigate to a previously visited directory."
    (eshell/cd (string-trim (shell-command-to-string (concat "zoxide query " dir))))
    (eshell/ls))

  :hook
  (eshell-mode . (lambda ()
                   (setq-local read-process-output-max (* 1024 1024))
                   (setq-local corfu-auto nil)
                   (corfu-mode)
                   (setq-local xterm-color-preserve-properties t)
                   (setenv "TERM" slot/eshell-term-name)))

  :bind (:map eshell-mode-map
              ("C-x C-c" . slot/eshell-exit)
              :map eshell-hist-mode-map
              ("M-r" . slot/eshell-insert-history))

  :config
  (require 'tramp)
  (require 'em-tramp)                   ; for `sudo'; see aliases
  (require 'em-hist)
  (require 'em-term)
  (add-to-list 'eshell-visual-commands "nmtui")
  (add-to-list 'eshell-visual-subcommands '("git" "log" "diff" "show"))
  (add-to-list 'eshell-visual-options '("git" "--help" "--paginate"))

  :custom
  (eshell-term-name slot/eshell-term-name)
  (password-cache-expiry (* 5 60))
  (eshell-scroll-to-bottom-on-input 'this)
  (eshell-scroll-to-bottom-on-output 'this)
  (eshell-destroy-buffer-when-process-dies t)
;;;; History
  ;; (eshell-history-file-name "~/.local/share/fish/fish_history")
  (eshell-history-size 100000)
  (eshell-hist-ignoredups t)
;;;; Imitate shells
  (eshell-stringify-t nil)
  (eshell-error-if-no-glob nil)
;;;; Prompt
  (eshell-prompt-regexp "└─[$#] ")      ; bottom of prompt
  (eshell-prompt-function #'slot/default-prompt-function)
;;;; Modules
  (eshell-modules-list
   '( eshell-alias eshell-basic eshell-cmpl eshell-dirs eshell-extpipe eshell-glob
      eshell-hist eshell-ls eshell-pred eshell-prompt eshell-script eshell-term
      eshell-unix)))

(use-package esh-autosuggest            ; fish-like autosuggestions
  ;; 15feb2023 github:dieggsy/esh-autosuggest#15 not merged yet.
  :vc (:url "https://github.com/slotThe/esh-autosuggest")
  :hook (eshell-mode . esh-autosuggest-mode))

(use-package eshell-toggle
  :bind ("M-`" . eshell-toggle)
  :custom
  (eshell-toggle-size-fraction 3)
  (eshell-toggle-init-function (lambda (_dir) (project-eshell))))

;; https://github.com/howardabrams/dot-files/blob/master/emacs-eshell.org
(defun slot/eshell-git-info (pwd)
  (when (and (not (file-remote-p pwd))
             (locate-dominating-file pwd ".git"))
    (let* ((git-url (shell-command-to-string "git config --get remote.origin.url"))
           (invalid-face '(:inherit eshell-prompt :strike-through t))
           (remote (if (s-blank? git-url)
                       (propertize "remote" 'face invalid-face)
                     (file-name-base (s-trim git-url))))
           (git-output (shell-command-to-string "git rev-parse --abbrev-ref HEAD"))
           (branch (if (s-prefix? "HEAD" git-output)
                       (propertize "branch" 'face invalid-face)
                     (s-trim git-output))))
      (concat remote "\xe0a0 " branch))))

;; https://github.com/exot/.emacs.d/blob/5009eb39f98edeb92d6f03089615c314b2043c45/site-lisp/db-eshell.el#L56
(defun slot/default-prompt-function ()
  "A prompt for eshell of the form
   ┌─[$USER@$HOST] [$PWD] [$GIT_INFO]
   └─[#$] "
  (let ((head-face '(:foreground "#315b00"))
        (pwd (abbreviate-file-name (eshell/pwd))))
    (concat (propertize "┌─" 'face head-face)
            (user-login-name) "@" (system-name)
            " "
            (propertize pwd 'face '(:inherit font-lock-function-name-face))
            " "
            (slot/eshell-git-info pwd)
            "\n"
            (propertize "└─" 'face head-face)
            (if (zerop (user-uid)) "# "  "$ "))))

(provide 'hopf-eshell)
;;; eshell-config.el ends here
