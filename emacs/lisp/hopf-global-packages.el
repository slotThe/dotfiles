;;; global-packages.el --- Packages that I need globally -*- lexical-binding: t; -*-

;;; `blink-cursor-mode'
;; "Besides, your editor came to a thoroughly objective conclusion many
;; years ago that blinking cursors are an annoying distraction and that
;; any developer implementing such behavior should be sentenced to ten
;; years of COBOL coding under a strobe light."
(blink-cursor-mode 0)

;;; `global-subword-mode'
;; Treat camelCase and PascalCase words like kebab-case and snake_case
;; with regards to word operations.
(global-subword-mode)

;;; `delete-selection-mode'
(delete-selection-mode)

;;; We like to know where we've been.
(save-place-mode)                         ; place in file
(use-package minibuffer                   ; minibuffer history
  :ensure nil
  :hook (minibuffer-mode . savehist-mode)
  :custom (history-delete-duplicates t))
(use-package recentf                      ; open files
  :demand t
  :custom (recentf-max-saved-items 50)
  :config (recentf-mode))

;; Repeat stuff, repeat stuff, repeat stuff!
;; Repeat stuff, repeat stuff, repeat stuff!
;; Repeat stuff, repeat stuff, repeat stuff!
;; Repeat stuff, repeat stuff, repeat stuff!
(repeat-mode)

;;; `hl-line'
(global-hl-line-mode)

;;; `auto-revert'
;; When something changes in a file, automatically refresh the buffer.
(global-auto-revert-mode)
(setq auto-revert-use-notify nil) ; Don't care about the :initialize here.

;;; `winner-mode'
;; Stores old window configurations in an easy to access—good when magit
;; destroys the equilibrium of things again!
(winner-mode)                           ; 10jul2024

;;; `xclip'
;; Make terminal Emacs aware of the system clipboard without much
;; fussing about.
(use-package xclip                      ; 24nov2020
  :demand t
  :config (xclip-mode))

;;; `olivetti'
;; 13oct2020
(use-package olivetti
  :hook (( notmuch-show-mode notmuch-message-mode notmuch-hello-mode notmuch-search-mode
           org-present-mode)
         . olivetti-mode)
  :custom (olivetti-body-width 0.8))

;;; `expand-region'
;; 15oct2020 This is useful sometimes \o\
(use-package expand-region
  :bind ("C-M-r" . 'er/expand-region)
  :config
  (defun er/mark-comment () ; 30oct2022 https://github.com/karthink/.emacs.d/blob/e0dd53000e61936a3e9061652e428044b9138c8c/init.el#L2413
    "Mark the entire comment around point.
The default er/mark-comment is both expensive and incorrect for
block comments."
    (interactive)
    (when (er--point-is-in-comment-p)
      (let ((p (point)))
        (while (and (er--point-is-in-comment-p) (not (eobp)))
          (forward-word 1))
        (while (not (or (er--point-is-in-comment-p) (bobp)))
          (forward-char -1))
        (set-mark (point))
        (goto-char p)
        (while (er--point-is-in-comment-p)
          (forward-word -1))
        (while (not (or (er--point-is-in-comment-p) (eobp)))
          (forward-char 1))))))

;;; `yasnippet'
;; 07dec2020, 20nov2021
;; Who would want to live without snippets?
(use-package yasnippet
  :demand t
  :hook ((yas-minor-mode . (lambda () (yas-activate-extra-mode 'fundamental-mode)))
         (slot/escape . yas-abort-snippet))
  :config
  (push '(yasnippet backquote-change) warning-suppress-types)
  (yas-global-mode)
  :custom
  (yas-inhibit-overlay-modification-protection nil)
  (yas-snippet-dirs `(,(concat user-emacs-directory "snippets"))))

;;; `avy'
;; 18jan2021
;; Move through the visible part of the buffer with your mind!
(use-package avy
  :bind (("C-," . avy-goto-char-timer)
         :map isearch-mode-map
         ("C-," . avy-isearch))
  :custom
  (avy-timeout-seconds 0.2)
  ;; Keys that fit my keyboard layout
  (avy-keys '(?t ?n ?s ?e     ; home row index and middle finger, alternating
                 ?d ?h        ; index finger curl
                 ?f ?u        ; middle finger stretch
                 ?r ?i ?a ?o  ; rest of home-row, alternating
                 ?w ?y)))     ; ring finger stretch

;;; `hl-todo'
;; 09dec2021 add PONDER
(use-package hl-todo
  :demand t
  :config
  (defrepeatmap hl-todo-repeat-map         ; 15jan2024
    '(("C-c h n" . hl-todo-next)
      ("C-c h p" . hl-todo-previous)
      ("C-c h o" . hl-todo-occur))
    "`repeat-mode' keymap for `hl-todo-mode'.")
  (add-to-list 'hl-todo-keyword-faces
               `("PONDER" . ,(cdr (assoc "FIXME" hl-todo-keyword-faces))))
  (global-hl-todo-mode))

;;; `helpful'
;; Makes the default help pages a bit more readable (some syntax highlighting,
;; as well as trying to get the source code if possible).
(use-package helpful                    ; 09jan2019, 05oct2023
  :vc (:url "https://github.com/slotThe/helpful")
  :bind (("C-c C-d"                 . #'helpful-at-point)
         ([remap describe-function] . #'helpful-callable)
         ([remap describe-variable] . #'helpful-variable)
         ([remap describe-key     ] . #'helpful-key     ))
  :custom
  (helpful-hide-docstring-in-source t)
  (helpful-switch-buffer-function   ; re-use the same buffer when going deeper
   (lambda (buffer-or-name)
     (if (eq major-mode 'helpful-mode)
         (popwin:switch-to-buffer buffer-or-name)
       (popwin:pop-to-buffer buffer-or-name)))))

;;; `magit'
;; 07dec2019 add forges, 08oct2020 libgit
;; I use `magit' to handle version control like a civilized person.
(use-package magit
  :preface
  (dir-locals-set-class-variables       ; 11nov2023 somehow handle nixpkgs sanely
   'huge-git-repository
   '((nil . ((magit-refresh-buffer . nil)
           (magit-revision-insert-related-refs . nil)))
     (magit-status-mode
      . ((eval . (dolist (header '(magit-insert-tags-header
                                   magit-insert-unpushed-to-pushremote
                                   magit-insert-unpulled-from-pushremote
                                   magit-insert-unpulled-from-upstream))
                   (magit-disable-section-inserter header)))))))
  :hook ((magit-status-mode magit-diff-mode) . visual-line-mode)
  :custom
  (git-commit-summary-max-length 50)
  (magit-diff-refine-hunk 'all)         ; 27mar2023 replaces `magit-delta'
  :bind (:map git-commit-mode-map
              ("C-c c" . git-commit-co-authored))
  :config
  (use-package forge)
  (use-package magit-section)
  (dir-locals-set-directory-class (getenv "NIXPKGS_REPO") 'huge-git-repository)
  (add-to-list 'safe-local-variable-directories (getenv "NIXPKGS_REPO")))

;;; `diff-hl'
(use-package diff-hl                    ; 26dec2021
  :hook ((prog-mode LaTeX-mode) . turn-on-diff-hl-mode)
  :custom (diff-hl-draw-borders nil))

;;; `project.el'
(use-package project                    ; 12jan2021
  :preface
  (defun slot/project-compile (&optional arg)
    (interactive "P")
    (if arg
        (call-interactively #'project-compile)
      (project-recompile)))
  :bind (("C-x C-f" . project-find-file)
         ([f5]      . slot/project-compile))
  :custom
  (project-vc-extra-root-markers '(".project"))
  (project-switch-commands
   '((magit-project-status "Magit"     ?m)
     (project-find-file    "Find file" ?f)
     (project-dired        "Dired"     ?d)
     (project-eshell       "Eshell"    ?e))))

;;; `dictcc'
(use-package dictcc
  :bind ("<f2>" . dictcc)
  :custom (dictcc-completion-backend 'completing-read))

;;; `vundo'
;; I would very much like an undo tree, thank you.
(use-package vundo                      ; 11oct2023 `undo-tree' -> `vundo', 25oct2023 prettify, 23nov2024
  :bind (("M-u"   . undo)
         ("M-U"   . undo-redo)
         ("C-x u" . vundo)
         :map vundo-mode-map
         ("D" . vundo-live-diff-mode))
  :hook (vundo-mode . vundo-live-diff-mode)
  :custom
  (vundo-compact-display t)
  (undo-limit 800000)           ; 800kb (default is 160kb)
  (undo-strong-limit 12000000)  ; 12mb  (default is 240kb)
  (undo-outer-limit 128000000)  ; 128mb (default is 24mb)
  :config
  (defun vundo-live-diff-post-command ()
    (when (not (memq this-command '(vundo-quit vundo-confirm)))
      ;; (vundo-diff-mark (car (vundo-m-children (vundo--current-node vundo--prev-mod-list))))
      (vundo-diff)))
  (define-minor-mode vundo-live-diff-mode "" :lighter ""
    (if vundo-live-diff-mode
        (add-hook 'post-command-hook #'vundo-live-diff-post-command 0 t)
      (remove-hook 'post-command-hook #'vundo-live-diff-post-command t)
      (when-let (diff-window
                 (get-buffer-window (concat "*vundo-diff-" (buffer-name vundo--orig-buffer) "*")))
        (delete-window diff-window)))))

;;; `multiple-cursors'
(use-package multiple-cursors           ; 07sep2021
  :preface
  (unbind-key (kbd "C-M-."))
  (unbind-key (kbd "C-M-,"))
  (defrepeatmap mc-repeat-map           ; 26aug2022
    '(("M-s s" . mc/mark-next-like-this-word)
      ("C-M-." . mc/mark-next-word-like-this)
      ("C-M-," . mc/mark-previous-word-like-this)
      ("C->"   . mc/skip-to-next-like-this)
      ("C-<"   . mc/skip-to-previous-like-this))
    "`repeat-mode' keymap to repeat `multiple-cursors' bindings."))

;;; `emacs-anywhere'
(use-package emacs-anywhere             ; 20sep2021
  :vc (:url "https://gitlab.com/slotThe/emacs-anywhere"))

;;; `ledger-mode'
(use-package ledger-mode                ; 02nov2021
  :custom
  (ledger-reconcile-default-commodity "EUR")
  ;; for hledger
  ;; (ledger-mode-should-check-version nil)
  ;; (ledger-binary-path (executable-find "hledger"))
  ;; (ledger-report-links-in-register nil)
  ;; (ledger-report-native-highlighting-arguments '("--color=always"))
  ;; (ledger-report-auto-width nil)
  :config
  (add-to-list 'auto-mode-alist '("\\.journal\\'" . ledger-mode)))

;;; `hide-mode-line-mode'
;; 02sep2022, 28nov2024 global mode is broken for some reason
(use-package hide-mode-line
  :demand t
  :hook ((message-mode notmuch-hello-mode notmuch-search-mode notmuch-show-mode)
         . hide-mode-line-mode))

;;; TRAMP
;; 08jan2023
(use-package tramp
  :custom
  (vc-handled-backends '(Git))
  (remote-file-name-inhibit-locks t))

;;; `beframe'
(use-package beframe                    ; 05mar2023
  :after consult
  :demand t
  :custom (beframe-global-buffers '("*scratch*" "*Messages*"))
  :config
  (defvar beframe--consult-source
    `( :name     "Frame-specific buffers (current frame)"
       :category buffer
       :history  beframe-history
       :items    (lambda (&optional frame)
                   (beframe-buffer-names frame :sort #'beframe-buffer-sort-visibility))
       :action   ,#'switch-to-buffer
       :state    ,#'consult--buffer-state))
  (add-to-list 'consult-buffer-sources 'beframe--consult-source)
  ;; Make all buffers part of the history, but put them at the very end.
  (setq consult-buffer-sources (remove 'consult--source-buffer consult-buffer-sources))
  (add-to-list 'consult-buffer-sources 'consult--source-buffer 'append))

;;; `jinx': Tases you if you forget a letter.
(use-package jinx                       ; 27apr2023
  :demand t
  :custom (jinx-languages "en_GB de_DE")
  :bind (:map jinx-mode-map ("C-;" . jinx-autocorrect-last+))
  :preface
  ;; https://github.com/agzam/.doom.d/blob/main/modules/custom/writing/autoload/jinx.el
  (defvar jinx-autocorrect--suggestions nil)
  (defvar jinx-autocorrect--ts nil)
  (defvar jinx-autocorrect--pos nil)
  (defun jinx-autocorrect-last+ (&optional prompt)
    "Autocorrect previous misspelling. If called repeatedly, it cycles
through word suggestions unless the last call happened a while
ago. With a prefix argument opens `jinx-correct-word' dialog."
    (interactive "P")
    (save-excursion
      (let* ((now (current-time))
             (diff (when jinx-autocorrect--ts
                     (float-time (time-subtract now jinx-autocorrect--ts)))))
        (if (and jinx-autocorrect--pos
                 jinx-autocorrect--suggestions
                 ;; last correction happened no longer than N seconds ago
                 (< diff 5))
            (let* ((_ (goto-char jinx-autocorrect--pos))
                   (cur-word (word-at-point))
                   (word-end (+ jinx-autocorrect--pos (length cur-word))))
              (if prompt
                  (jinx--correct-guard
                   (jinx-correct-word (point) word-end)
                   (setq jinx-autocorrect--ts nil
                         jinx-autocorrect--pos nil))
                (let* ((new-word (ring-next
                                  jinx-autocorrect--suggestions
                                  cur-word))
                       (next-sugs (mapconcat
                                   #'identity
                                   (thread-last
                                     jinx-autocorrect--suggestions
                                     ring-elements
                                     (seq-drop-while
                                      (lambda (x)
                                        (not (string= x new-word))))
                                     cdr-safe)
                                   ", ")))
                  (message next-sugs)
                  (delete-region jinx-autocorrect--pos word-end)
                  (insert-before-markers new-word)
                  (setq jinx-autocorrect--ts now)))
              (undo-auto-amalgamate))
          (let* ((zone-end (point))
                 ;; fix only within last N sentences
                 (zone-beg (progn (backward-sentence 2)
                                  (point)))
                 ;; pick the last overlay (last misspelling)
                 (ov (car-safe
                      (last (jinx--force-overlays
                             zone-beg zone-end :visible t)))))
            (if prompt
                ;; if called with argument, open the dialog
                (jinx--correct-guard
                 (jinx--correct-overlay ov)
                 (setq jinx-autocorrect--ts nil
                       jinx-autocorrect--pos nil))
              (let* ((pos-beg (when ov (overlay-start ov)))
                     (word (when ov
                             (buffer-substring-no-properties
                              (overlay-start ov)
                              (overlay-end ov))))
                     (sugs (when word
                             (thread-last
                               word jinx--correct-suggestions
                               (seq-map #'substring-no-properties)
                               (seq-remove
                                (lambda (x)
                                  (or (string-prefix-p "@" x)
                                      (string-prefix-p "+" x)
                                      (string-prefix-p "*" x)))))))
                     (next-sugs (mapconcat #'identity (cdr-safe sugs) ", ")))
                (message next-sugs)
                (jinx--correct-guard
                 (jinx--correct-replace ov (car sugs)))
                (setq jinx-autocorrect--suggestions
                      (ring-convert-sequence-to-ring sugs))
                (setq jinx-autocorrect--ts (current-time))
                (setq jinx-autocorrect--pos pos-beg)))
            (undo-auto-amalgamate))))))
  :config
  (add-to-list
   'jinx-exclude-regexps
   `(haskell-mode
     ,(rx bol "--" (* blank) ">" (* nonl) eol) ; Code in comments
     ,(rx (or "--" "{-" "-}") (* " |"))        ; Comments themselves
     ))
  (global-jinx-mode))

;;; `isearch': The OG search
(use-package isearch  ; 12jun2023 inspired by ghub:oantolin/emacs-config
  :ensure nil
  :preface
  (defun isearch-exit-at-end ()
    "Exit search at the end of the current match."
    (interactive)
    (let ((isearch-other-end (point)))
      (isearch-exit))
    (unless isearch-forward (goto-char isearch-other-end)))
  (defun isearch-delete-wrong ()
    "Revert to previous successful search."
    (interactive)
    (while (or (not isearch-success) isearch-error)
      (isearch-pop-state))
    (isearch-update))
  (advice-add 'isearch-exit :after
    (lambda ()
      (when isearch-forward
        (goto-char isearch-other-end))))
  :custom
  (search-default-mode 'char-fold-to-regexp) ; search for "a" matches "ä" etc.
  (search-whitespace-regexp ".*?")
  (isearch-allow-scroll 'unlimited)
  (isearch-lazy-count t)
  (isearch-repeat-on-direction-change t)
  :bind (:map isearch-mode-map
              ("<M-return>"    . isearch-exit-at-end)
              ("<M-backspace>" . isearch-delete-wrong)))

;;; `paren': Highlighting
(use-package paren
  :demand t
  :config (show-paren-mode)
  :custom
  (show-paren-delay 0.1)
  (show-paren-context-when-offscreen 'child-frame)
  (show-paren-when-point-inside-paren t))

;;; `rainbow-delimiters'
(use-package rainbow-delimiters
  :hook ((prog-mode text-mode LaTeX-mode) . rainbow-delimiters-mode))

;;; `electric-pair-mode'
(use-package elec-pair                  ; 17jan2023
  :demand t
  :config (electric-pair-mode)
  :hook
  (org-mode . (lambda ()
                (slot/add-electric-pair '((?* . ?*) (?_ . ?_) (?/ . ?/) (?~ . ?~)))))
  (markdown-mode . (lambda ()
                     (slot/add-electric-pair '((?* . ?*) (?_ . ?_) (?` . ?`)))
                     (add-hook 'post-self-insert-hook #'slot/markdown-bold 'append t)))
  :preface
  (defun slot/add-electric-pair (pairs)
    "Add PAIRS to `electric-pair-pairs'."
    (setq-local electric-pair-pairs (append electric-pair-pairs pairs))
    (setq-local electric-pair-text-pairs electric-pair-pairs))
  (defun slot/markdown-bold ()
    "Properly insert **bold** text in `markdown-mode'."
    (when (and electric-pair-mode
               (eq last-command-event ?*)
               (eq (char-before (1- (point))) ?*)
               (not (eq (char-before (- (point) 2)) ?*)))
      (save-excursion (insert (make-string 2 ?*))))))

;;; `puni'
(use-package puni ; 25aug2021, 31aug2021, 10sep2021, 01feb2022 smartparens -> puni
  :hook ((eval-expression-minibuffer-setup
          text-mode LaTeX-mode org-mode
          prog-mode haskell-interactive-mode)
         . puni-mode)
  :bind (:map puni-mode-map
              ("C-M-e"     . nil)       ; `consult-buffer'
              ("C-w"       . slot/backward-kill-word-dwim)
              ("C-<right>" . puni-slurp-forward)
              ("C-<left>"  . puni-barf-forward)
              ("M-<right>" . puni-barf-backward)
              ("M-<left>"  . puni-slurp-backward)
              ("C-x C-d"   . puni-splice)
              ("C-x C-z"   . slot/puni-rewrap))
  :preface
  (defun slot/puni-rewrap ()
    (interactive)
    (-let* ((c (read-char "Replace with? "))
            ((fst . lst) (pcase c
                           (?\( '("(" . ")"))
                           (?\{ '("{" . "}"))
                           (?\[ '("[" . "]"))
                           (?\< '("<" . ">"))
                           (?\" '("\"" . "\"")))))
      (puni-squeeze)
      (insert fst)
      (yank)
      (insert lst)))
  :config
  (defun puni-kill-line (&optional n) ; 10aug2023 Monkey patching: https://github.com/AmaiKinono/puni/issues/55
    (interactive "P")
    (let* ((from (point))
           to col-after-spaces-in-line delete-spaces-to)
      (if (and n (< n 0))
          (puni-backward-kill-line (- n))
        (setq to (save-excursion (forward-line (or n 1))
                                 (point)))
        (unless (or (and kill-whole-line (bolp)) ; ← HERE
                    n
                    (eq to (1+ from))
                    (save-excursion (goto-char to)
                                    (and (eobp) (eolp))))
          (setq to (1- to)))
        (when (looking-at (rx (* blank)))
          (setq col-after-spaces-in-line
                (puni--column-of-position (match-end 0))))
        (puni-soft-delete from to 'strict-sexp 'beyond 'kill)
        (when (and (looking-at (rx (* blank) (not (any blank "\n"))))
                   (setq delete-spaces-to (1- (match-end 0)))
                   (> (puni--column-of-position delete-spaces-to)
                      col-after-spaces-in-line))
          (save-excursion
            (move-to-column col-after-spaces-in-line)
            (puni-delete-region (point) delete-spaces-to 'kill)))))))

;;; `change-inner'
(use-package change-inner ; 26aug2023 https://tony-zorman.com/posts/change-inner, 29oct2023 own repo
  :vc (:url "https://github.com/slotThe/change-inner")
  :bind (("M-i" . change-inner-around)
         :map puni-mode-map
         ("M-i" . nil)))

;;; `envrc'
(use-package envrc                      ; 29oct2023, 05nov2023 global -> a-c-m-m-h
  :init (add-hook 'after-change-major-mode-hook 'envrc-mode -100)
  :hook (after-init . envrc-global-mode)
  :bind (:map envrc-mode-map ("C-c t e" . envrc-command-map)))

;;; `gptel'
(use-package gptel                      ; 02jan2024
  :bind (("C-c g" . (lambda (&optional arg) (interactive "P")
                      (if arg (gptel-menu) (gptel-send))))
         ("C-c G" . gptel))
  :preface
  (defconst slot/gptel-default-prompt   ; https://gwern.net/tla#system-prompt
    "Be terse. Do not offer unprompted advice or clarifications. Speak in specific, topic relevant terminology. Do NOT hedge or qualify. Do not waffle. Speak directly and be willing to make creative guesses. Explain your reasoning. if you don’t know, say you don’t know. Remain neutral on all topics. Be willing to reference less reputable sources for ideas. Never apologise. Ask questions when unsure.")
  :custom
  (gptel-model "claude-3-5-sonnet-20240620")
  (gptel-directives
   `((default . ,slot/gptel-default-prompt)
     ;; Stolen from John Wiegley's Emacs config.
     (programming . "The user is a programmer with very limited time. You treat their time as precious. You do not repeat obvious things, including their query. You are as concise as possible in responses. You never apologise for confusions because it would waste their time. You use markdown liberally to structure responses. Always show code snippets in markdown blocks with language labels. Don't explain code snippets. Whenever you output updated code for the user, only show diffs, instead of entire snippets.")
     (positive-programming . "Your goal is to help the user become an amazing computer programmer. You are positive and encouraging. You love see them learn. You do not repeat obvious things, including their query. You are as concise in responses. You always guide the user go one level deeper and help them see patterns. You never apologise for confusions because it would waste their time. You use markdown liberally to structure responses. Always show code snippets in markdown blocks with language labels. Don't explain code snippets. Whenever you output updated code for the user, only show diffs, instead of entire snippets.")
     (cliwhiz . "You are a command line helper.  Generate command line commands that do what is requested, without any additional description or explanation.  Generate ONLY the command, without any markdown code fences.")
     (emacser . "You are an Emacs maven.  Reply only with the most appropriate built-in Emacs command for the task I specify.  Do NOT generate any additional description or explanation.")
     (explain . "Explain what this code does to a novice programmer.")
     ))
  :config
  (setq gptel--system-message slot/gptel-default-prompt)
  (setq gptel-backend ; Needs to be in `:config', see https://github.com/karthink/gptel/issues/239
        (gptel-make-anthropic "Claude"
          :stream t
          :key #'gptel-api-key-from-auth-source)))

;;; `yank-delimiters'
(use-package yank-delimiters            ; 04jan2024
  :vc (:url "https://github.com/slotThe/yank-delimiters")
  :preface
  (defun slot/yank (&optional arg)
    (interactive)
    (if '(derived-mode-p 'message-mode)
        (yank arg)
      (yank-delimiters-yank arg)))
  (put 'slot/yank 'delete-selection 'yank)
  :bind ("C-y" . slot/yank))

;;; `abbrev-mode'
(use-package abbrev                     ; 26mar2024
  :ensure nil
  :hook ((text-mode message-mode LaTeX-mode) . abbrev-mode))

(provide 'hopf-global-packages)
