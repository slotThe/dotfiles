;;; keybindings.el --- Additional keybindings (that don't fit anywhere else) -*- lexical-binding: t; -*-

;;; `keyboard-quit', stolen from DOOM.
(defvar slot/escape-hook nil) ; To be filled later.
(defun slot/escape (&optional interactive)
  "A sensible `keyboard-quit'."
  (interactive (list 'interactive))
  (let ((inhibit-quit t))
    (cond
     ;; Always quit the minibuffer, even if not open.
     ((> (minibuffer-depth) 0)
      (setq this-command 'abort-recursive-edit)
      (abort-recursive-edit))
     ;; Run all escape hooks. If any returns non-nil, then stop there.
     ((run-hook-with-args-until-success 'slot/escape-hook))
     ;; Don't abort macros.
     ((or defining-kbd-macro executing-kbd-macro) nil)
     ;; Back to the default.
     ((unwind-protect (keyboard-quit)
        (setq this-command 'keyboard-quit))))))
(global-set-key [remap keyboard-quit] #'slot/escape)

;;; Auto completion things.
(use-package hippie-exp
  :bind ("M-/" . hippie-expand)
  :config
  (advice-add 'he-substitute-string :after
    (lambda (str &optional _)
      "Remove extra paren when expanding line."
      (when (equal (substring str -1) ")")
        (backward-delete-char 1)
        (forward-char))))
  :custom
  (hippie-expand-try-functions-list
   '(try-complete-file-name
     try-complete-file-name-partially
     try-expand-dabbrev-visible
     try-expand-dabbrev
     try-expand-dabbrev-all-buffers
     try-expand-dabbrev-from-kill
     try-expand-list
     try-expand-line
     try-complete-lisp-symbol
     try-complete-lisp-symbol-partially
     try-expand-all-abbrevs)))

;;; Util
(use-package emacs                    ; 08aug2021 clean up bindings, 10mar2023
  :preface
  (defun slot/paste-primary-selection ()
    "Paste the primary selection after the point."
    (interactive)
    (insert (gui-get-primary-selection)))

  (defun slot/insert-time ()
    (interactive)
    (let* ((formats '((?i "ISO 8601"  "%Y-%m-%d")
                      (?l "DDmmmYYYY" "%d%b%Y")
                      (?t "Time"      "%H:%M")))
           (key (read-key
                 (cl-loop for (key label _) in formats
                          concat (format "[%s] %s "
                                         (propertize (single-key-description key) 'face 'bold)
                                         label)))))
      (->> (alist-get key formats)
           cl-second
           format-time-string
           downcase                     ; Jan -> jan
           insert)))
  :bind (("M-o"   . other-window)       ; Faster switching between windows
         ("C-x k" . kill-current-buffer)
         ("M-:"   . pp-eval-expression)
         ("<f12>" . slot/insert-time)
         ;; Interacting with X11
         ("C-c i" . slot/paste-primary-selection)))

(bind-key "C-c t t" 'consult-theme)
(bind-key "C-c t d" 'toggle-debug-on-error)
(bind-key "C-c w" 'dictionary-lookup-definition)
(bind-key "C-c f" 'fontaine-set-preset)

;;; Toggling things
;; 20may2022
(defrepeatmap toggle-repeat-map
  '(("C-c t o" . olivetti-mode)
    ("C-c t v" . variable-pitch-mode)
    ("C-c t l" . display-line-numbers-mode)
    ("C-c t h" . hide-mode-line-mode))
  "`repeat-mode' keymap to repeat toggle key sequences.")

;;; Text
(use-package mwim                       ; 30oct2022 mwim
  :bind (("C-a" . slot/back-to-indentation)
         ("C-e" . mwim-end))
  :preface
  (defun slot/back-to-indentation ()
    "Move point to the first non-whitespace character on this line.
If the point is already there, move to the beginning of the line
instead."
    (interactive)
    (let ((bti (save-excursion (back-to-indentation) (point))))
      (cond ((eq major-mode 'haskell-interactive-mode)
             (haskell-interactive-mode-bol))
            ((eq major-mode 'notmuch-message-mode)
             (let ((mp (save-excursion (message-beginning-of-line) (point))))
               (if (= (point) mp)
                   (goto-char bti)
                 (goto-char mp))))
            (t (mwim-beginning))))))

(use-package query-replace-many         ; 21feb2023
  :vc (:url "https://github.com/slotThe/query-replace-many")
  :bind ("M-#" . query-replace-many))

;; 26jan2021, 02feb2021, 24may2021, 08aug2021, 18aug2021, 22aug2021
(use-package whole-line-or-region
  :demand t
  :config (whole-line-or-region-global-mode)
  :bind (("M-z"   . zap-up-to-char               )
         ("C-M-Z" . zap-to-char                  )
         ("C-w"   . slot/backward-kill-word-dwim )
         ("C-M-w" . kill-whole-line              )
         ("M-c"   . slot/invert-char             )
         ("M-;"   . slot/comment-dwim            )
         ("C-c a" . align-regexp                 )
         ("C-x C-SPC" . rectangle-mark-mode      )
         ([remap upcase-region  ] . upcase-dwim  )
         ([remap downcase-region] . downcase-dwim)))

(defun slot/comment-dwim (arg)
  (interactive "*P")
  (if (use-region-p)
      (comment-dwim arg)
    (comment-line arg)))

(defun slot/invert-char (&optional arg)
  "Invert char at point and move forward.
If a \\[universal-argument] is given, go backwards instead."
  (interactive "P")
  (if arg
      (backward-word)
    (when (cl-find (char-after) "-_()[]{}/.:,\n\t ")
      (forward-to-word 1)))
  (let ((char (char-after))
        (point (point)))
    (if (s-lowercase? (char-to-string char))
        (upcase-char 1)
      (downcase-region point (1+ point))))
  (forward-word))

(defun slot/backward-kill-word-dwim (&optional arg)
  "Kill words or selected text.
If a \\[universal-argument] is given, call `join-line'.  If a
region is selected, kill that region.  Otherwise, forward to
mode-specific `backward-kill-word' forwarders instead."
  (interactive "P")
  (let ((in-rect (bound-and-true-p rectangle-mark-mode))
        (in-lisp (bound-and-true-p paredit-mode))
        (in-puni (bound-and-true-p puni-mode)))
    (cond (arg            (apply 'join-line
                                 (if (equal arg '(4)) nil arg)
                                 (when (use-region-p)
                                   (list (region-beginning) (region-end)))))
          (in-rect        (kill-rectangle (region-beginning) (region-end)))
          ((use-region-p) (kill-region    (region-beginning) (region-end)))
          (in-lisp        (paredit-backward-kill-word))
          (in-puni        (puni-backward-kill-word))
          (t              (backward-kill-word 1)))))

(use-package delete-indentation-comments
  :defer nil
  :vc (:url "https://github.com/slotThe/delete-indentation-comments"))

(provide 'hopf-keybindings)
