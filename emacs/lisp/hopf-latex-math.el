;;; latex-math.el --- LaTeX math: used by both `LaTeX-mode' and `org-mode' ' -*- lexical-binding: t; -*-

;;; `math-delimiters'
(use-package math-delimiters            ; 05sep2021
  :vc (:url "https://github.com/oantolin/math-delimiters")
  :commands math-delimiters-insert
  :config
  (defun math-delimiters-no-dollars (&optional beg end)
    "Convert math formulas in buffer from dollars to \\(\\) and
\\=\\[\\]."
    (interactive
     (when (region-active-p)
       (list (region-beginning) (region-end))))
    (message "%s %s" beg end)
    (cl-flet ((replace-all (a b &optional c)
                (goto-char (or beg (point-min)))
                (while (search-forward a end t)
                  (replace-match b t t)
                  (when c
                    (search-forward a)
                    (replace-match c t t)))))
      (save-excursion
        (replace-all "\\$" "\\dollar ")
        (replace-all "$$" "\\[" "\\]")
        (replace-all "$" "\\(" "\\)")
        (replace-all "\\dollar " "\\$"))))
  :custom (math-delimiters-compressed-display-math nil))

;;; `latex-change-env'
(use-package latex-change-env           ; 21sep2021
  :after latex
  :commands latex-change-env
  :bind (:map LaTeX-mode-map
              ("C-c r" . latex-change-env))
  :custom
  (latex-change-env-math-display '("\\[" . "\\]"))
  (latex-change-env-math-inline  '("\\(" . "\\)"))
  (latex-change-env-edit-labels-in-project t))

;;; =CDLaTeX=
;; 08dec2019, 21dec2019, 08oct2020, 13nov2020, 28jan2021

;; This is like the best thing ever.  It defines all kinds of
;; abbreviations, key shortcuts etc.
(use-package cdlatex
  :bind (:map cdlatex-mode-map ("$" . nil)) ; use `latex-change-env'
  :hook
  (LaTeX-mode  . turn-on-cdlatex    ) ; with `LaTeX-mode'
  (org-mode    . turn-on-org-cdlatex) ; use a subset for `org-mode'
  (cdlatex-tab . yas-expand         )
  :custom
  (cdlatex-use-dollar-to-ensure-math nil)
  (cdlatex-sub-super-scripts-outside-math-mode nil)
  (cdlatex-paired-parens "$([{")        ; pair (almost) all the things
  (cdlatex-math-modify-alist            ; math modify; accessed with '
   '(;;  MATHCMD      TEXTCMD    ARG RMDOT IT
     (?k "\\mathfrak" nil        t   nil   nil)
     (?B "\\mathbb"   "\\textbf" t   nil   nil)
     (?b "\\mathbf"   "\\textbf" t   nil   nil)
     (?f "\\mathsf"   "\\textsf" t   nil   nil)
     (?l "\\ld"       "\\textsl" t   nil   nil)
     (?u "\\lld"      ""         t   nil   nil)
     ))
  ;; Custom bindings for the symbol list, accessed with `.  You may add
  ;; additional layers here.
  (cdlatex-math-symbol-alist
   '(;;   LAYER 1        LAYER 2      LAYER
     (?c ("\\circ"       ""           ""        ))
     (?C ("\\coprod"     ""           ""        ))
     (?e ("\\varepsilon" "\\epsilon"  "\\exp"   ))
     (?f ("\\varphi"     "\\phi"      ""        ))
     (?F ("\\Phi"        ""           ""        ))
     (?R ("\\real"       "\\Re"       ""        ))
     (?N ("\\nat"        "\\nabla"    "\\exp"   ))
     (?Z ("\\integer"    ""           ""        ))
     (?Q ("\\Theta"      "\\rat"      ""        ))
     (?0 ("\\varnothing" ""           ""        ))
     (?{ ("\\subseteq"   "\\subset"   ""        ))
     (?} ("\\supseteq"   "\\supset"   ""        ))
     (?. ("\\cdot"       "\\dots"     ""        ))
     (?I ("\\iota"       "\\Im"       ""        ))
     (?$ ("\\lad"        "\\vdash"    ""        ))
     (?6 ("\\otimes"     ""           ""        ))
     (?7 ("\\kotimes"    ""           ""        ))
     (?5 ("\\bullet"     ""           ""        ))
     (?4 ("\\ract"       ""           ""        ))
     (?4 ("\\oplus"      ""           ""        ))
     (?2 ("\\cotens"     ""           ""        ))
     (?1 ("\\lact"       ""           ""        ))
     )))

;;; `aas': sane auto-expanding snippets
(use-package aas                        ; 20nov2021
  :hook ((LaTeX-mode org-mode) . aas-activate-for-major-mode)
  :preface
  (defun slot/try-add-math (base-snippet)
    (-let (((beg . end) (if (texmathp) (cons "" "") math-delimiters-inline)))
      (yas-expand-snippet
       (concat (regexp-quote beg) base-snippet (regexp-quote end)))))
  :config
  (--each '(LaTeX-mode org-mode)
    (aas-set-snippets it
      "cec"   (lambda () (interactive) (slot/try-add-math "\\cat{C}"))
      "cnc"   (lambda () (interactive) (slot/try-add-math "\\cat{N}"))
      "cd"    (lambda () (interactive) (slot/try-add-math "\\cat{D}"))
      "cm"    (lambda () (interactive) (slot/try-add-math "\\cat{M}"))
      "cv"    (lambda () (interactive) (slot/try-add-math "\\cat{V}")))
    (aas-set-snippets it
      :cond #'texmathp
      ":"     "\\from "
      "**"    "\\star"
      "mm"    "\\mid "
      "->"    "\\to "
      "mt"    "\\mapsto "
      "=>"    "\\implies "
      "..."   "\\dots"
      "bbln"  "\\bblank"
      "bln"   "\\blank"
      "bbbln" "\\bbblank"
      "defn"  "\\defeq"
      "~~"    "\\simeq"
      "=="    "\\cong"
      "=def"  "\\eqdef"
      "blact" "\\blact"
      "lact"  "\\lact"
      "bract" "\\bract"
      "ract"  "\\ract"
      "kk"    "\\Bbbk"
      "bf"    (lambda () (interactive)
                (puni-backward-sexp) (puni-forward-kill-word) (just-one-space)
                (insert "\\mathbf{" (current-kill 0) "}"))
      "mrm"    (lambda () (interactive)
                 (puni-backward-sexp) (puni-forward-kill-word) (just-one-space)
                 (insert "\\mathrm{" (current-kill 0) "}"))
      "sf"    (lambda () (interactive)
                (puni-backward-sexp) (puni-forward-kill-word) (just-one-space)
                (insert "\\mathsf{" (current-kill 0) "}"))
      "frk"   (lambda () (interactive)
                (puni-backward-sexp) (puni-forward-kill-word) (just-one-space)
                (insert "\\mathfrak{" (current-kill 0) "}"))
      "mbb"   (lambda () (interactive)
                (puni-backward-sexp) (puni-forward-kill-word) (just-one-space)
                (insert "\\mathbb{" (current-kill 0) "}"))
      "cc"    (lambda () (interactive)
                (slot/try-add-math "\\cat{C}"))
      "bc"    (lambda () (interactive)
                (slot/try-add-math "\\BiCat{$0}"))
      "emc"   (lambda () (interactive)
                (slot/try-add-math "\\cat{${1:C}}^{${2:T}}$0"))
      "cat"   (lambda () (interactive)
                (yas-expand-snippet "\\cat{$1}$0"))
      "copp"  (lambda () (interactive)
                (slot/try-add-math "\\cat{${1:C}}^{\\op}"))
      "opp"   (lambda () (interactive)
                (yas-expand-snippet "^{\\op}"))
      ">"     (lambda () (interactive)
                (yas-expand-snippet "\\langle $0 \\rangle"))
      "adj"   (lambda () (interactive)
                (yas-expand-snippet "\\adj{${1:F}}{${2:U}}{${3:\\cc}}{${4:\\mm}} $0"))
      "coend" (lambda () (interactive)
                (yas-expand-snippet "\\int^{${2:c \\in \\cat{C\\}}}"))
      "end"   (lambda () (interactive)
                (yas-expand-snippet "\\int_{${2:c \\in \\cat{C\\}}}"))
      "lr"    (lambda () (interactive)
                (yas-expand-snippet "\\left($0\\right)"))
      "sett"  (lambda () (interactive)
                (yas-expand-snippet "\\\\{\\\\, $0 \\\\,\\\\}")))
    (aas-set-snippets it
      :cond (lambda () (not (texmathp)))
      "iff " "if and only if ")))

(provide 'hopf-latex-math)
;;; latex-math.el ends here
