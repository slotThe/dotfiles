;;; latex-config.el --- Base LaTeX configuration -*- lexical-binding: t; -*-

;;; AUCTeX
(use-package latex
  :ensure auctex
  :preface
  ;; 13mar2023, 28jul2023 Make LaTeX math less visible.
  (defvar slot/latex-math-face (make-face 'slot/latex-math-face))
  (set-face-attribute 'slot/latex-math-face
                      nil
                      :inherit 'font-lock-comment-face :weight 'extra-light :slant 'normal)
  (font-lock-add-keywords
   'LaTeX-mode '(("\\\\[]()[]\\|\\$" 0 slot/latex-math-face t)))

  (advice-add 'TeX-fold-buffer :after
    (lambda ()
      "Fold quiver comments."
      (save-excursion
        (goto-char (point-min))
        (while (re-search-forward "^\s*%+.+q\.?uiver\\(\.app\\)?.+"
                                  (point-max) 'no-error)
          (let ((beg (progn (back-to-indentation) (point))))
            (while (TeX-comment-forward))
            (end-of-line 0)
            (let ((ov (TeX-fold-make-overlay beg (point) 'comment "% ≫quiver URL≪")))
              (TeX-fold-hide-item ov)
              (overlay-put ov 'face 'font-lock-comment-face)))))))
  :bind (:map LaTeX-mode-map
              ("$"       . latex-change-env-cycle)
              ("<SPC>"   . slot/LaTeX-space)
              (","       . slot/LaTeX-self-insert)
              ("."       . slot/LaTeX-self-insert)
              ("-"       . slot/LaTeX-self-insert)
              ("C-c C-n" . LaTeX-find-matching-end)
              ("C-c C-p" . LaTeX-find-matching-begin))
  :hook
  (LaTeX-mode . visual-line-mode)          ; only visually wrap lines
  (LaTeX-mode . TeX-source-correlate-mode) ; jump between editor and pdf viewer
  (LaTeX-mode . slot/LaTeX-fold-annoyances)
  (LaTeX-mode . (lambda () (setq-local local-abbrev-table LaTeX-mode-abbrev-table)))
  :custom
  (TeX-command-extra-options "-shell-escape -synctex=1")
  (LaTeX-babel-hyphen nil)       ; Disable language-specific hyphen insertion.
  (LaTeX-item-indent  0  )       ; Indent items by 2 spaces (really!)
  (TeX-auto-save      t  )       ; Enable parse on save
  (TeX-parse-self     t  )       ; Enable parse on load
  (TeX-master         nil)       ; Ask for master file
  (TeX-view-program-selection    ; How to view documents.
   '((output-pdf "Zathura")
     (output-html "firefox")))
  (flycheck-chktex-extra-flags
   '(;; The checking here seems to be very naïve, suggesting a dash instead of
     ;; an en dash for Person1–Person2. No.
     "-n8"
     ;; Tikzpictures force one to use "…" instead of ``…'', so this warning is
     ;; spurious almost every time. Other cases are handled by Emacs.
     "-n18"
     ;; Chktex seems to have problems with optional arguments to \label; i.e.,
     ;; something like \label[opt]{l}, which I use with cleveref.
     "-n10"
     ;; Personally, I think that (co)algebra is a perfectly fine thing to
     ;; write, thank you very much.
     "-n36"
     )))

(defvar slot/LaTeX-space-alist
  '((?+           . "\\dots + ")
    (?-           . "\\dots - ")
    ("\\lact"     . "\\dots \\lact ")
    ("\\ract"     . "\\dots \\ract ")
    ("\\blact"    . "\\dots \\blact ")
    ("\\bract"    . "\\dots \\bract ")
    ("\\cdot"     . "\\dots \\cdot ")
    ("\\quad"     . "\\quad")
    ("\\qquad"    . "\\qquad")
    ("\\subseteq" . "\\dots \\subseteq ")
    ("\\subset"   . "\\dots \\subset ")
    (?,           . "\\dots, "))
  "Mappings from a symbol to a substituting expression.
This is used in `slot/LaTeX-space' to decide—upon pressing space—whether
to `self-insert-command', or to instead insert the CDR of the respective
element in the alist.")

(defun slot/LaTeX-space (&optional arg)
  "Insert a space; or not.
See `slot/LaTeX-space-alist' for a list of replacement pairs. If we find
ourselves in a situation like `CAR ', then pressing space will transform
this to `CAR CDR ', with CAR and CDR being the respective elements of
the above alist.  Otherwise, insert \\blank instead."
  (interactive "P")
  (cl-flet ((aget (key)
              (alist-get key slot/LaTeX-space-alist nil nil #'equal)))
    (let ((char-before-before (char-before (1- (point)))))
      (if (and (= ?\s (char-before))              ; space before
               (not (equal ?\s char-before-before)) ; not just indenting
               (texmathp))                        ; in math-mode
          (let ((word-before (save-excursion
                               (backward-word)
                               (when (>= (point) (cdr texmathp-why))
                                 (thing-at-point 'sexp t)))))
            (if-let ((ins (or (aget char-before-before) (aget word-before))))
                (insert ins)
              (insert "\\blank ")))
        (slot/LaTeX-self-insert arg ?\s)))))

(defun slot/LaTeX-fold-annoyances ()
  "Fold annoyances.
Everything that makes the LaTeX file worse to look at—things like
commutative diagrams, as well as comments."
  (interactive)
  (TeX-fold-mode)
  (setq-local TeX-fold-env-spec-list-internal
              '(("≫commutative diagram≪" ("tikzcd" "mytikzcd"))
                ("≫tikz picture≪" ("tikzpicture"))))
  (TeX-fold-buffer))

(defun slot/LaTeX-self-insert (&optional arg char)
  "`self-insert-command' for LaTeX mode.
If the previous word is just a single character, surround it with
dollar signs.  If already in math mode, do nothing.  If the
character is a single `a', do nothing.

If called with a single \\[universal-argument], just call
`self-insert-command'."
  (interactive "P")
  (pcase arg
    ('(4) (self-insert-command 1))
    (_ (let ((ppoint (save-excursion (backward-word)       (point)))
             (ipoint (save-excursion (back-to-indentation) (point)))
             (word   (word-at-point)))
         (unless (or (length> word 1)   ; longer than a single character
                     (not word)
                     (= ipoint ppoint)  ; the first thing on a new line
                     (equal "a" word)
                     (number-at-point)
                     (texmathp))
           (-let (((open . close) math-delimiters-inline))
             (backward-char)
             (insert open)
             (forward-char 1)
             (insert close)))
         (self-insert-command 1 char)))))

;;;; Bibliography management with `reftex'.
(use-package reftex                     ; 31aug2021, 29jan2025
  :preface
  (defun slot/fix-reftex (&optional _)
    "I use a custom include macro sometimes, so I had to slog through… that
code to find out the magic variables to change. Fun times."
    (setq reftex-everything-regexp
          "\\(?:\\\\label{\\(?1:[^}]*\\)}\\|\\\\\\(?:begin[[:space:]]*{\\(?:d\\(?:array\\|group\\|math\\|series\\)\\|frame\\|lstlisting\\)}[[:space:]]*\\|\\(?:ctable\\)\\)\\[[^][]*\\(?:{[^}{]*\\(?:{[^}{]*\\(?:{[^}{]*}[^}{]*\\)*}[^}{]*\\)*}[^][]*\\)*\\<label[[:space:]]*=[[:space:]]*{?\\(?1:[^] ,}\n	%]+\\)[^]]*\\]\\)\\|\\(^\\)[ 	]*\\\\\\(part\\|chapter\\|section\\|subsection\\|subsubsection\\|paragraph\\|subparagraph\\|addchap\\|addsec\\)\\*?\\(\\[[^]]*\\]\\)?[[{ 	\n\\]\\|\\(^\\)[ 	]*\\\\\\(include\\|input\\|includeOrInput\\)[{ 	]+\\([^} 	\n]+\\)\\|\\(^\\)[ 	]*\\(\\\\appendix\\)\\|\\(\\\\glossary\\|\\\\index\\)[[{]"
          reftex-everything-regexp-no-index
          "\\(?:\\\\label{\\(?1:[^}]*\\)}\\|\\\\\\(?:begin[[:space:]]*{\\(?:d\\(?:array\\|group\\|math\\|series\\)\\|frame\\|lstlisting\\)}[[:space:]]*\\|\\(?:ctable\\)\\)\\[[^][]*\\(?:{[^}{]*\\(?:{[^}{]*\\(?:{[^}{]*}[^}{]*\\)*}[^}{]*\\)*}[^][]*\\)*\\<label[[:space:]]*=[[:space:]]*{?\\(?1:[^] ,}\n %]+\\)[^]]*\\]\\)\\|\\(^\\)[ 	]*\\\\\\(part\\|chapter\\|section\\|subsection\\|subsubsection\\|paragraph\\|subparagraph\\|addchap\\|addsec\\)\\*?\\(\\[[^]]*\\]\\)?[[{ 	\n \\]\\|\\(^\\)[ 	]*\\\\\\(include\\|input\\|includeOrInput\\)[{ 	]+\\([^}\n ]+\\)\\|\\(^\\)[ 	]*\\(\\\\appendix\\)\\|\\(\\\\6\\\\3\\\\1\\)"
          reftex-section-or-include-regexp
          "\\(^\\)[ 	]*\\\\\\(part\\|chapter\\|section\\|subsection\\|subsubsection\\|paragraph\\|subparagraph\\|addchap\\|addsec\\)\\*?\\(\\[[^]]*\\]\\)?[[{ 	\n\\]\\|\\(^\\)[ 	]*\\\\\\(include\\|input\\|includeOrInput\\)[{ 	]+\\([^} 	\n]+\\)"))
  (dolist (mode '(reftex-mode reftex-toc reftex-reference reftex-select-label-mode))
    (advice-add mode :before #'slot/fix-reftex))
  :hook
  (LaTeX-mode . turn-on-reftex)
  :config
  (setf (cdaadr (alist-get 'biblatex reftex-cite-format-builtin))
        reftex-cite-format)
  :bind (:map reftex-mode-map
              ("C-c [" . (lambda () (interactive)
                           (reftex-citation nil ?))))
  :custom
  (reftex-cite-format "~\\cite[][]{%l}")
  (reftex-plug-into-AUCTeX t)               ; reftex plays nicely with auctex
  (reftex-toc-split-windows-horizontally t) ; show reftex TOC on the left
  (reftex-ref-macro-prompt nil)             ; no unnecessary prompts
  (reftex-label-alist                   ; tell reftex about some environments
   '(("section"     ?s "sec:"  "\\cref{%s}" t (regexp "[Ss]ection\\(s\\)?"       ))
     ("definition"  ?d "def:"  "\\cref{%s}" t (regexp "[Dd]efinition\\(s\\)?"    ))
     ("example"     ?x "ex:"   "\\cref{%s}" t (regexp "[Ee]xample\\(s\\)?"       ))
     ("lemma"       ?l "lem:"  "\\cref{%s}" t (regexp "[Ll]emma\\(s\\|ta\\)?"    ))
     ("proposition" ?p "prop:" "\\cref{%s}" t (regexp "[Pp]roposition\\(s\\)?"   ))
     ("theorem"     ?t "thm:"  "\\cref{%s}" t (regexp "[Tt]heorem\\(s\\)?"       ))
     ("remark"      ?r "rem:"  "\\cref{%s}" t (regexp "[Rr]emark\\(s\\)?"        ))
     ("corollary"   ?c "cor:"  "\\cref{%s}" t (regexp "[Cc]orollar\\(y\\|ies\\)?"))
     ("equation"    ?e "eq:"   "\\cref{%s}" t (regexp "[Ee]quation\\(s\\)?"      )))))

(provide 'hopf-latex)
