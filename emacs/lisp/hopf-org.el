;;; org-setup.el --- My `org-mode' configuration -*- lexical-binding: t; -*-

;;; Basic Org Config

(use-package org
  :vc                                   ; 25mar2023 new LaTeX preview
  (org-mode :url "https://git.tecosaur.net/tec/org-mode.git" :branch "dev")
  :preface
  (defun slot/in-org-dir (file)
    (concat (file-name-as-directory org-directory) file))

  (defun slot/insert-image (&optional dir)
    "Select and insert an image at point."
    (interactive)
    (let* ((file-name (format "%s-%s.png"
                              (file-name-sans-extension (buffer-name))
                              (cl-random (expt 2 31))))
           (dir (or dir
                    (expand-file-name
                     (read-file-name "Directory to store image: "))))
           (file-and-dir (format "%s%s" dir file-name)))
      ;; The mouse movement via xdotool is needed because otherwise, if
      ;; unclutter is active, the pointer will remain hidden.  Uff.
      (call-process "xdotool" nil 0 nil "mousemove_relative" "--" "-1" "0")
      (let ((scrot-exit (call-process "scrot" nil nil nil "-z" "-f" "-s" "--file" file-and-dir)))
        (when (= scrot-exit 0)
          (insert (format "[[%s]]" file-and-dir))))))
  :hook
  ;; Don't highlight my Haskell arrows!
  (org-mode . (lambda ()
                (modify-syntax-entry ?< "." org-mode-syntax-table)
                (modify-syntax-entry ?> "." org-mode-syntax-table)))
  (org-mode . (lambda () ; Org's default advice quite significantly breaks writing LaTeX for me.
                (advice-remove 'texmathp 'org--math-p)
                (advice-add 'texmathp :around
                  (lambda (old-fun)
                    (if (not (derived-mode-p 'org-mode))
                        (funcall old-fun)
                      (let ((ret (funcall old-fun)))
                        (if (or (string= "$"  (car texmathp-why)) ; Just ignore dollars
                                (string= "$$" (car texmathp-why)))
                            (setq texmathp-why nil)
                          ret)))))))
  :bind (([f6] . org-capture)
         ( :map org-mode-map
           ("C-c C-x C-s" . org-archive-subtree)
           ("C-c c"       . org-capture)
           ("$"           . latex-change-env-cycle))
         ( :repeat-map org-repeat-map    ; 07jul2022
           ("p"   . org-previous-visible-heading)
           ("n"   . org-next-visible-heading)
           ("TAB" . org-cycle)))
  :custom
  (org-directory "~/repos/org")
  (org-archive-location (concat (slot/in-org-dir "archive.org") "::* From %s"))
  (org-hide-leading-stars t)        ; Hide leading stars in subheadings.
  (org-log-done 'time)              ; Record time when archiving.
  (org-log-repeat nil)              ; Record nothing when repeating
  (org-refile-targets '((org-agenda-files :todo . "PROJECT")))
  (org-todo-keywords
   '((sequence "TODO(t)" "NEXT(n)" "WAITING(w)" "CURRENT(C)" "|" "DONE(d)"    )
     (sequence "BUG(b)" "HOLD(h)" "SOMEDAY(s)" "IDEA(i)"     "|"              )
     (sequence "PROJECT(p)"                                  "|" "CANCELED(c)")))
  (org-capture-templates
   `(("t" "Todo" entry (file ,(slot/in-org-dir "todos.org")) "* TODO %?\n %i\n %a")
     ("m" "Mail Todo" entry (file ,(slot/in-org-dir "todos.org")) "* TODO %A %?")))
  (org-modules '(org-habit))
  (org-babel-load-languages
   '((emacs-lisp . t)
     (shell . t)
     (python . t)))
  (org-startup-indented t)
  (org-insert-heading-respect-content t)
  (org-special-ctrl-a/e t)
  (org-highlight-latex-and-related '(native script entities latex))
  (org-export-with-broken-links t)
  (org-latex-hyperref-template "\\hypersetup{\n pdfauthor={%a},\n pdftitle={%t},\n pdfkeywords={%k},pdfsubject={%d},\n pdfcreator={%c},\n pdflang={%L},\n colorlinks,\n linktoc=page,\n linkcolor={red},\n citecolor={red},\n urlcolor={red}}\n")
  :config
  (require 'org-protocol)
  (add-to-list 'org-export-backends 'md) ; Export to more formats
  ;; If `org-open-at-point' wants to open a file, open it in this frame.
  (add-to-list 'org-link-frame-setup '(file . find-file))
  (--each '("amsmath" "mathtools" "amsthm" "amssymb" "mathrsfs")
    (push `("" ,it t) org-latex-packages-alist))
  ;; Ellipsis styling
  (setq org-ellipsis "…")
  (set-face-attribute 'org-ellipsis nil :inherit 'default :box nil))

(use-package org-modern                 ; 15dec2023
  :custom (org-modern-table nil)
  :hook
  (org-mode . org-modern-mode)
  (org-agenda-finalize . org-modern-agenda))

(use-package org-modern-indent
  :vc (:url "https://github.com/jdtsmith/org-modern-indent")
  :hook (org-mode . org-modern-indent-mode)) ; late hook

(use-package org-sticky-header          ; 15dec2023
  :hook (org-mode . org-sticky-header-mode))

(use-package org-latex-preview          ; 28mar2023
  :after org
  :ensure nil
  :preface
  (defun slot/disable-preview-around (fun &optional arg)
    (org-latex-preview-auto-mode -1)
    (prog1 (funcall fun arg)
      (org-latex-preview-auto-mode)))
  :hook (org-mode . org-latex-preview-auto-mode)
  :config
  ;; Prevent slowness of M-<↑↓> in large files, especially numbering.
  (advice-add 'org-metaup   :around #'slot/disable-preview-around)
  (advice-add 'org-metadown :around #'slot/disable-preview-around)
  :custom
  (org-latex-preview-process-precompiled nil) ; XXX: currently precompilation seems to happen on every fragment all the time?
  (org-latex-preview-width 1.0)
  (org-latex-preview-default-process 'dvisvgm)
  (org-latex-preview-numbered nil)
  (org-latex-preview-auto-ignored-commands
   '(next-line previous-line scroll-up-command scroll-down-command scroll-other-window scroll-other-window-down org-forward-paragraph org-backward-paragraph)))

;;; Agenda

;;;; `org-agenda'
(use-package org-agenda
  :ensure nil
  :hook
  (org-agenda-mode . olivetti-mode)
  (org-agenda-finalize . slot/org-agenda-repeated-names)
  :bind (:map org-agenda-mode-map
              ("a"   . org-agenda-archive   )
              ("d"   . (lambda () (interactive) (org-agenda-todo 'done)))
              ("c"   . (lambda () (interactive) (org-agenda-todo "CANCELED")))
              ("C"   . org-agenda-goto-calendar)
              ("M-f" . org-agenda-date-later)
              ("M"   . org-agenda-month-view))
  :preface
  (defun slot/org-agenda-skip-tag (tag)
    "Context: point is currently on a headline."
    (when (member tag (org-get-tags (point)))
      (outline-next-heading)))
  (defun slot/org-agenda-unless-tags (tags)
    "Context: point is currently on a headline."
    (let ((tags-at-point (org-get-tags (point))))
      (unless (--any? (member it tags-at-point) tags)
        (outline-next-heading))))

  ;; Show "Rep: 4d" for repeatable tasks in the agenda, instead of just
  ;; "scheduled". Source: https://whhone.com/posts/org-agenda-repeated-tasks/
  (defun slot/org-agenda-repeated-names ()
    (save-excursion
      (goto-char (point-min))
      (while (text-property-search-forward 'type "scheduled" t)
        (save-excursion
          (when-let ((prefix (car org-agenda-scheduled-leaders))
                     (org-repeat (when-let ((marker (org-get-at-bol 'org-marker))
                                            (buffer (marker-buffer marker))
                                            (pos (marker-position marker)))
                                   (with-current-buffer buffer
                                     (goto-char pos)
                                     (org-get-repeat)))))
            (when (search-backward prefix (pos-bol) t)
              (delete-forward-char (length prefix))
              (insert-and-inherit (format "Rep %4s: " org-repeat))))))))
  :custom
  (org-agenda-tags-column 77)           ; Show tags in colum 77.
  (org-agenda-prefix-format '((agenda . "  %?-12t% s"))) ; " In x d.:  "
  ;; Holidays in agenda
  (holiday-bahai-holidays nil)
  (holiday-hebrew-holidays nil)
  (holiday-islamic-holidays nil)
  (holiday-oriental-holidays nil)
  (holidays-general-holidays
   '((holiday-fixed 1 1 "New Year's Day")
     (holiday-fixed 5 1 "Tag der Arbeit")
     (holiday-fixed 10 3 "Tag der Deutschen Einheit")
     (holiday-fixed 1 6 "Heilige Drei Könige")
     (holiday-easter-etc 60 "Fronleichnam")
     (holiday-fixed 8 15 "Mariä Himmelfahrt")
     (holiday-fixed 10 31 "Reformationstag")
     (holiday-fixed 11 1 "Allerheiligen")
     (holiday-float 11 3 -1 "Buß- und Bettag" 22)
     (holiday-fixed 4 1 "April Fools' Day")
     (holiday-float 5 0 2 "Mother's Day")
     (holiday-easter-etc 39 "Father's Day")
     (holiday-fixed 10 31 "Halloween")
     (holiday-float 11 4 4 "Thanksgiving")))
  (holiday-other-holidays
   '((holiday-fixed 6 8 "Yoneda Appreciation Day")
     (holiday-fixed 2 14 "I Love Free Software Day")
     (holiday-fixed 7 21 "Moonlanding (1969)")
     (holiday-fixed 1 11 "Aaron Swartz Rememberance Day")))
  (org-agenda-include-diary t)
  ;; Super agenda
  (org-agenda-custom-commands
   `(("n" "Block Agenda"
      ((agenda "Current day"
               ((org-agenda-block-separator nil)
                (org-agenda-span 'day)
                (org-agenda-compact-blocks t)
                (org-agenda-skip-function
                 '(org-agenda-skip-entry-if 'todo '("IDEA")))
                (org-super-agenda-groups
                 '(( :name "Overdue"
                     :scheduled past
                     :deadline past)
                   ( :name "Today"
                     :time-grid t
                     :and (:scheduled today :not (:tag "habit"))
                     :and (:deadline today))
                   ( :name "Take these"
                     :and (:scheduled today :tag "dont_die"))
                   ( :name "Due soon"
                     :deadline future)
                   ( :name "Habits"
                     :and (:tag "habit" :scheduled today))
                   ( :name "Themes"
                     :scheduled nil)))))
       (alltodo "Ideas"
                ((org-agenda-block-separator nil)
                 (org-agenda-overriding-header "\nIdeas")
                 (org-agenda-skip-function
                  ;; Show a random selection of ideas
                  '(or (org-agenda-skip-entry-if 'nottodo '("IDEA"))
                       (when (/= 0 (mod (random) 15))
                         (org-entry-end-position))))))
       (agenda "14 day preview"
               ((org-agenda-block-separator nil)
                (org-agenda-span 13)
                (org-agenda-start-day "+1d")
                (org-agenda-time-grid nil)
                (org-agenda-skip-function
                 '(or (org-agenda-skip-entry-if 'regexp ".*NEHMEN:.*")
                      (org-agenda-skip-entry-if 'todo   '("IDEA"))
                      (slot/org-agenda-skip-tag "dont_die")))
                (org-agenda-overriding-header "\n14 day preview\n")
                (org-super-agenda-groups nil))))))))

(use-package org-super-agenda           ; 11feb2024
  :after org-agenda
  :demand t
  :config (org-super-agenda-mode))

;;; `org-roam'
(use-package org-roam                   ; 12oct2020, 22nov2020, 21jul2021 v2
  :after org
  :preface
  (defconst slot/org-roam-header
    "#+title: ${title}
#+startup: inlineimages
#+latex_header: \\usepackage[type=org,math=fancy]{$HOME/.tex/styles/style}\n\n")

  (defun slot/org-roam-insert-image ()
    "Select and insert an image at point."
    (interactive)
    (slot/insert-image (format "%s/images" org-roam-directory)))

  (defun slot/org-roam-process-before-exporting (_)
    "Properly process an `org-roam' file before exporting.
In particular, kill all the cross-references and just replace
them with their respective descriptions."
    (when (bound-and-true-p org-roam-mode)
      (replace-regexp (rx "[[" (one-or-more (not ?\]))         ; id
                          "][" (group (one-or-more (not ?\]))) ; description
                          "]]")
                      "\\1")))

  (defun slot/org-roam-capture-new-node ()
    (org-roam-tag-add '("draft"))
    (when (s-prefix? (concat org-roam-directory "/novel") (buffer-file-name))
      (org-roam-tag-add '("novel"))))

  (defun slot/org-roam-anki-node (&optional arg)
    (interactive "P")
    (slot/org-new-anki-node
     (read-string "Name (optional): " nil nil (int-to-string (cl-random (1- (expt 2 32)))))
     :use-deck t
     :type (if arg "Cloze" "Basic")))
  :hook
  (org-roam-capture-new-node . slot/org-roam-capture-new-node)
  (org-export-before-processing . slot/org-roam-process-before-exporting)
  (org-roam-find-file . anki-whitespace-mode)
  (org-roam-find-file . (lambda () (ignore-errors (org-latex-preview 'buffer))))
  :bind (:map org-mode-map
              ("C-c n i" . org-roam-node-insert)
              ("C-c n l" . org-roam-buffer-toggle)
              ("C-c n f" . org-roam-node-find)
              ("C-c n g" . org-roam-graph)
              ("C-c n c" . org-roam-capture)
              ("C-c r"   . slot/org-roam-anki-node)
              ("C-c n p" . anki-editor-push-note-at-point)
              ("C-c n d" . anki-editor-delete-note-at-point)
              ("C-c M-i" . slot/org-roam-insert-image)
              ("C-c RET" . org-id-get-create))
  :custom
  (org-roam-directory (file-truename "~/repos/org/org-roam"))
  (org-roam-node-display-template
   (concat "${type:10} ${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-capture-templates
   `(("n" "novel" plain
      "%?"
      :if-new (file+head "novel/${slug}.org" ,slot/org-roam-header)
      :immediate-finish t
      :unnarrowed t)
     ("r" "reference" plain
      "%?"
      :if-new (file+head "reference/${slug}.org" ,slot/org-roam-header)
      :immediate-finish t
      :unnarrowed t)))
  :config
  (org-roam-setup)
  (require 'org-roam-protocol)
  (cl-defmethod org-roam-node-type ((node org-roam-node))
    (condition-case nil
        (file-name-nondirectory
         (directory-file-name
          (file-name-directory
           (file-relative-name (org-roam-node-file node) org-roam-directory))))
      (error ""))))

;;; `anki-editor'
(use-package anki-editor                ; 15feb2023, 26mar2024
  :vc (:url "https://github.com/anki-editor/anki-editor")
  :commands anki-editor-mode
  :custom
  (anki-editor-create-decks t)
  (anki-editor-break-consecutive-braces-in-latex t)
  :bind ( :map anki-editor-mode-map
          ("C-c N" . slot/anki-node))
  :config
  (use-package anki-editor-ui
    :ensure nil
    :bind (:map anki-editor-mode-map
	        ("C-c C-a" . anki-editor-ui)))

  (defun anki-editor-cloze-dwim ()
    "Cloze current active region or a word the under the cursor.
Decide the current cloze level to be one higher than the highest
surrounding one (in the same note).  Monkey-patched version of the
original `anki-editor-cloze-dwim' so that I don't have to rewrite
`anki-editor-ui'."
    (interactive)
    ;; Decide on a cloze level
    (-let* (((beg end) (if (bound-and-true-p anki-whitespace-mode)
                           (-cons-to-list (anki-whitespace--get-whitespace-note))
                         (-drop-last 1 (org-list-context))))
            (lvl (let ((lvl 0))
                   (save-mark-and-excursion
                     (goto-char beg)
                     (while (re-search-forward "{{c\\([[:digit:]+]\\)::" end t)
                       (setq lvl (max lvl (read (match-string-no-properties 1)))))
                     (cl-incf lvl)))))
      (cond
       ((region-active-p)
        (anki-editor-cloze (region-beginning) (region-end) lvl ""))
       ((thing-at-point 'word)
        (let ((bounds (bounds-of-thing-at-point 'word)))
          (anki-editor-cloze (car bounds) (cdr bounds) lvl "")))
       (t (user-error "Nothing to create cloze from")))))

  (cl-defun slot/org-new-anki-node (name &key type use-deck level)
    "Create a new anki node in any Org file.
If NAME is not specified, use a random 32-bit unsigned int for the
string.  If TYPE is \"Cloze\", insert a cloze deletion; if it is nil,
insert a \"Basic\" card template.  If USE-DECK is a string, use that
string as the deck name; otherwise, query the user.  LEVEL is the level
the headline should be set at; if this is nil, make a guess based on
'org-current-level'."
    (interactive
     (list (read-string "Name (optional): " nil nil (int-to-string (cl-random (1- (expt 2 32)))))))
    (let ((lvl (or level
                   (max 2 (or (save-excursion
                                (anki-editor--goto-nearest-note-type)
                                (org-current-level))
                              0))))
          (type (or type "Basic"))
          (use-deck (when use-deck
                      (if (stringp use-deck)
                          use-deck
                        (completing-read "Deck: " (anki-editor-api-call-result 'deckNames))))))
      (insert (concat (make-string lvl ?*) " " name))
      (insert (format "\n:PROPERTIES:%s\n:ANKI_NOTE_TYPE: %s\n:END:"
                      (if use-deck (format "\n:ANKI_DECK: %s" use-deck) "")
                      type))
      (newline)))

  (defun slot/anki-node (&optional arg)
    (interactive "P")
    (slot/org-new-anki-node
     (read-string "Name (optional): " nil nil (int-to-string (cl-random (1- (expt 2 32)))))
     :type (if arg "Cloze" "Basic"))))

(use-package anki-whitespace
  :vc (:url "https://github.com/anki-editor/anki-whitespace")
  :commands anki-whitespace-mode
  :custom
  (anki-whitespace-prefix "»")
  (anki-whitespace-suffix "«")
  :config (require 'anki-editor-ui)
  :bind ( :map anki-whitespace-mode-map
          ("C-c C-a" . anki-editor-ui)
          ("C-c N"   . anki-whitespace-new-note)))

;;; `org-ref'
(use-package parsebib
  :vc (:url "https://github.com/joostkremers/parsebib"))
(use-package org-ref                    ; 28feb2024
  :vc (:url "https://github.com/jkitchin/org-ref")
  :custom
  (bibtex-completion-bibliography '("~/.tex/bibliography.bib"))
  (bibtex-completion-pdf-open-function arxiv-citation-open-pdf-function))

(provide 'hopf-org)
