;;; hopf-pretty-symbols.el --- More `prettify-symbols-mode' symbols -*- lexical-binding: t; -*-

;;; More Pretty Symbols
;; 24dec2019 ho-ho-ho, 30may2021, 29dec2023

;;; General

(defvar fundamental-prettify-symbols-alist
  '(("lambda" . ?λ)
    ("!="     . ?≠)
    ("/="     . ?≠)
    (">="     . ?≥)
    ("<="     . ?≤)
    ("||"     . ?⋁)
    ("&&"     . ?⋀)))

(defun slot/enable-pretty-symbols-for (mode)
  (let ((pretty-alist (intern (concat (symbol-name mode) "-prettify-symbols-alist"))))
    (setq-local prettify-symbols-alist (symbol-value pretty-alist))
    (prettify-symbols-mode 1)))

(use-package emacs
  :custom (prettify-symbols-unprettify-at-point 'right-edge))

;;; Haskell

(use-package emacs
  :hook (haskell-mode . (lambda () (slot/enable-pretty-symbols-for 'haskell)))
  :preface
  (defvar haskell-prettify-symbols-alist
    `(("::"                 . ?∷)
      ("forall"             . ?∀)
      ("exists"             . ?∃)
      ("->"                 . ?→)
      ("<-"                 . ?←)
      ("=>"                 . ?⇒)
      ("~>"                 . ?⇝)
      ("<~"                 . ?⇜)
      ("<>"                 . ?⨂)
      ("msum"               . ?⨁)
      ("\\"                 . ?λ)
      ("<<<"                . ?⋘)
      (">>>"                . ?⋙)
      ("`elem`"             . ?∈)
      ("`notElem`"          . ?∉)
      ("`member`"           . ?∈)
      ("`notMember`"        . ?∉)
      ("`union`"            . ?⋃)
      ("`intersection`"     . ?⋂)
      ("`isSubsetOf`"       . ?⊆)
      ("`isNotSubsetOf`"    . ?⊄)
      ("`isSubsequenceOf`"  . ?⊆)
      ("`isProperSubsetOf`" . ?⊂)
      ("undefined"          . ?⊥)
      ,@fundamental-prettify-symbols-alist)))

;;; Lisp

(use-package emacs
  :hook (emacs-lisp-mode . (lambda () (slot/enable-pretty-symbols-for 'elisp)))
  :preface
  (defvar elisp-prettify-symbols-alist
    `(("not" ?¬)
      ("nil" ?⊥)
      ("t"   ?⊤)
      ,@fundamental-prettify-symbols-alist)))

;;; Org

(use-package emacs
  :preface
  (require 'tex-mode)
  (defvar org-prettify-symbols-alist tex--prettify-symbols-alist)
  :hook
  (org-mode . (lambda ()
                (slot/enable-pretty-symbols-for 'org)
                (slot/more-latex-symbols)
                (prettify-symbols-mode))))

;;; LaTeX: A beast of its own

(use-package emacs
  :hook
  (LaTeX-mode . prettify-symbols-mode  )
  (LaTeX-mode . slot/more-latex-symbols))

;; Use the Agda input method to insert symbols. Alternatively, this site seems
;; to be good for symbols (use dec): https://unicodelookup.com
(defun slot/more-latex-symbols ()
  (let ((op '(?ᵒ (tr tl -25 0) ?ᵖ)))
    (--map (push it prettify-symbols-alist)
           `(;; Numbers
             ("\\nat"         . ?ℕ)
             ("\\integer"     . ?ℤ)
             ("\\real"        . ?ℝ)
             ("\\complex"     . ?ℂ)
             ;; Functions
             ("\\colon"       . ?:)
             ("\\from"        . ?:)
             ("\\trightarrow" . ?→)
             ("\\xrightarrow" . ?→)
             ("\\emb"         . ?↪)
             ("\\sur"         . ?↠)
             ("\\iso"         . ?⥲)
             ("\\implies"     . ?⇒)
             ("\\nt"          . ?⇒)
             ("\\longmapsfrom". ?↤)
             ("\\mapsfrom"    . ?↤)
             ("\\xslashedrightarrow{}" . ?⇸)
             ("\\slashedrightarrow"    . ?⇸)
             ;; Binary operations
             ("\\otimes"      . ?⨂)
             ("\\ox"          . ?⨂)
             ("\\odot"        . ?⨀)
             ("\\bigoplus"    . ?⨁)
             ("\\star"        . ?★)
             ("\\ostar"       . ?⍟)
             ("\\cotens"      . ?□)
             ("\\land"        . ?∧)
             ("\\lor"         . ?∨)
             ("\\tak"         . (215 (br . cl) ?A))                 ; ×_A
             ("^{\\tensorop}" . (?​ (tr bl 0 80) ?⊗ (tr . tl) ,@op)) ; ⊗^op
             ("\\kotimes"     . (?⨂ (br . cl) ?ₖ))                  ; ⨂ₖ
             ;; Categories
             ("\\cc"          . ?𝓒)
             ("\\dd"          . ?𝓓)
             ("\\ee"          . ?𝓔)
             ("\\mm"          . ?𝓜)
             ("\\nn"          . ?𝓝)
             ("\\cent"        . ?𝓩)
             ("\\ZCat"        . ?𝓩)
             ;; Sub- and superscript
             ("_0"            . ?₀)
             ("_1"            . ?₁)
             ("_2"            . ?₂)
             ("_r"            . ?ᵣ)
             ("_l"            . ?ₗ)
             ("_x"            . ?ₓ)
             ("^2"            . ?²)
             ("^l"            . ?ˡ)
             ("^r"            . ?ʳ)
             ("^T"            . ?ᵀ)
             ("_m"            . ?ₘ)
             ("_x"            . ?ₓ)
             ("^{\\op}"       . (?ᵒ (tr tl -25 0) ?ᵖ))                   ; ^op
             ("^{-1}"         . (?​ (tr bl 0 100) ?- (cr . bl) ?¹))       ; ⁻¹
             ("^{(x)}"        . (?⁽ (cr cl -25 -7) ?ˣ (cr cl -25 0) ?⁾)) ; ⁽ˣ⁾
             ("^{(r)}"        . (?⁽ (cr cl -25 -7) ?ʳ (cr cl -25 0) ?⁾)) ; ⁽ʳ⁾
             ;; Meta
             ("\\footnote"    . ?⸸)
             ("\\label"       . ?‡)
             ;; Missing Greek letters
             ("\\vartheta"    . ?ϑ)
             ("\\varpi"       . ?ϖ)
             ;; Triangles
             ("\\lact"        . ?▹)
             ("\\ract"        . ?◃)
             ("\\bract"       . ?◂)
             ("\\blact"       . ?▸)
             ("\\vartriangle" . ?△)
             ;; Assignments
             ("\\eqdef"       . ?≝)
             ("\\defeq"       . (?: (cr cl -20 -8) ?=)) ; a better ≔
             ("\\bnf"         . (?∷ (cr . cl) ?=))
             ;; Brackets
             ("\\llbracket"   . ?\⟦)
             ("\\rrbracket"   . ?\⟧)
             ;; Hip category theory
             ("\\lad"         . ?⊣)
             ("\\adjoint"     . ?⊣)
             ("\\yo"          . ?よ)
             ("\\ta"          . ?た)
             ("\\k"           . ?𝕜)
             ("\\Bbbk"        . ?𝕜)
             ("\\blackdiamond". ?♦)
             ;; Misc
             ("\\dots"        . ?…)
             ("\\blank"       . ?—)
             ("\\bblank"      . ?=)
             ("\\bbblank"     . ?≡)
             ))))

;;;; (Ab)use AUCTeX's code fold for personal gain
;;
;; 28apr2023: https://tecosaur.github.io/emacs-config/config.html#editor-visuals

(defconst slot/string-offset-exceptions
  '(;; lowercase serif
    (119892 . ?ℎ)
    ;; lowercase caligraphic
    (119994 . ?ℯ)
    (119996 . ?ℊ)
    (120004 . ?ℴ)
    ;; caligraphic
    (119965 . ?ℬ)
    (119968 . ?ℰ)
    (119969 . ?ℱ)
    (119971 . ?ℋ)
    (119972 . ?ℐ)
    (119975 . ?ℒ)
    (119976 . ?ℳ)
    (119981 . ?ℛ)
    ;; fraktur
    (120070 . ?ℭ)
    (120075 . ?ℌ)
    (120076 . ?ℑ)
    (120085 . ?ℜ)
    (120092 . ?ℨ)
    ;; blackboard
    (120122 . ?ℂ)
    (120127 . ?ℍ)
    (120133 . ?ℕ)
    (120135 . ?ℙ)
    (120136 . ?ℚ)
    (120137 . ?ℝ)
    (120145 . ?ℤ))
  "An alist of deceptive codepoints, and where the glyph
actually resides.")

(defun slot/shift-string (offset word)
  "Shift the codepoint of each character in WORD by OFFSET.
Add an extra -6 shift if the letter is lowercase."
  (apply #'string
         (--map (let ((char (pcase it
                              (?, ?,)   ; Allow things like \Hom(\mathbb{A,B})
                              (_ (+ (if (>= it ?a) (- it 6) it) offset)))))
                  (or (cdr (assoc char slot/string-offset-exceptions))
                      char))
                word)))

(defun slot/TeX-word? (teststring)
  "Return t if TESTSTRING is a single token, nil otherwise."
  (if (string-match-p "^\\\\?\\w+$" teststring) t nil))

(cl-flet ((start-at (pt)
            (- pt ?A)))
  (setq TeX-fold-math-spec-list
        `((,(lambda (num den)
              (if (and (slot/TeX-word? num) (slot/TeX-word? den))
                  (concat num "／" den)
                (concat "❪" num "／" den "❫")))
           ("frac"))
          ("‘{1}’"               ("text"))
          ("{1}ᵛ"                ("rd"))
          ("{1}ᵛᵛ"               ("rrd"))
          ("ᵛ{1}"                ("ld" "lDual"))
          ("ᵛᵛ{1}"               ("lld" "lbiDual"))
          ("⦅{1}⦆"               ("eqref"))
          ("⟦{1}⟧"               ("ref"))
          ("([1] of {1})||({1})" ("cite"))
          (,(##slot/shift-string (start-at ?𝐀) %) ("mathbf"))
          (,(##slot/shift-string (start-at ?𝖠) %) ("mathsf"))
          (,(##slot/shift-string (start-at ?𝖠) %) ("mathrm"))
          (,(##slot/shift-string (start-at ?𝙰) %) ("mathtt"))
          (,(##slot/shift-string (start-at ?𝔄) %) ("mathfrak"))
          (,(##slot/shift-string (start-at ?𝔸) %) ("mathbb"))
          (,(##slot/shift-string (start-at ?𝓐) %) ("mathcal" "cat" "euler")))))

(setq LaTeX-fold-math-spec-list TeX-fold-math-spec-list)

(setq TeX-fold-macro-spec-list
      '(;; tweaked defaults
        ("[1]:||►" ("item"))
        ("❡❡ {1}" ("part" "part*"))
        ("❡ {1}" ("chapter" "chapter*"))
        ("§ {1}" ("section" "section*"))
        ("§§ {1}" ("subsection" "subsection*"))
        ("§§§ {1}" ("subsubsection" "subsubsection*"))
        ("¶ {1}" ("paragraph" "paragraph*"))
        ("¶¶ {1}" ("subparagraph" "subparagraph*"))
        ;; extra
        ("{1}" ("emph"))
        ("□" ("begin{proof}"))
        ("■" ("end{proof}"))))

(provide 'hopf-pretty-symbols)
