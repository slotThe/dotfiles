;;; programming.el --- Small programming configurations -*- lexical-binding: t; -*-

;;; General Programming
(use-package prog-mode
  :ensure nil
  :config
  (global-font-lock-mode)
  (setq-default   ; Always indent with spaces, never use tabs; #FuckTabs
   indent-tabs-mode  nil
   default-tab-width 2)
  (setq standard-indent 2))

(use-package compile ; 22feb2023 http://endlessparentheses.com/ansi-colors-in-the-compilation-buffer-output.html
  :hook (compilation-filter . ansi-color-compilation-filter)
  :bind (:map compilation-mode-map
              ("z" . delete-window))
  :custom
  (compilation-scroll-output 'first-error) ; Scroll to first error
  (compilation-auto-jump-to-first-error t)
  ;; Just do it.
  (compilation-always-kill t)
  (compilation-ask-about-save nil))

;;; `apheleia': Universal code formatting
(use-package apheleia                   ; 06jan2024
  :hook ((python-mode          ; ruff
          emacs-lisp-mode      ; `lisp-indent-function'
          clojure-mode         ; cljfmt
          rustic-mode          ; rustfmt
          tuareg-mode          ; ocamlfmt
          ) . apheleia-mode)
  :config
  (setf (alist-get 'python-mode apheleia-mode-alist) 'ruff)
  (setf (alist-get 'haskell-mode apheleia-mode-alist) 'fourmolu)
  (setf (alist-get 'fourmolu apheleia-formatters)
        '("fourmolu"
          "--function-arrows" "leading"
          "--indentation" "2"
          "--column-limit" "90"
          "--stdin-input-file" filepath)))

;;; `flycheck'
;; Most IDE-like packages automatically set hooks for `flycheck', so
;; this configuration is rather minimal.
(use-package flycheck
  :hook ((markdown-mode message-mode LaTeX-mode) . flycheck-mode) ; `proselint'
  :bind (("M-n" . flycheck-next-error)
         ("M-p" . flycheck-previous-error)))

;;; Haskell \o/
;; 23mar2020, 08oct2020 nav imports bidirectionally, 22sep2021
(use-package haskell-mode
  :preface
  (defun slot/haskell-load-and-bring ()
    "Sane behaviour when loading the current file into ghci."
    (interactive)
    (save-buffer)
    (haskell-process-load-file)
    (haskell-interactive-bring))

  (defun slot/haskell-add-pragma ()
    "Add a pragma to the function above point."
    (interactive)
    (let* ((fun-name (save-excursion
                       (re-search-backward "^[\s]*[[:ascii:]]* ::"
                                           (save-excursion (previous-line 50) (point)))
                       (progn (back-to-indentation)
                              (buffer-substring (point)
                                                (1- (search-forward " " (point-at-eol)))))))
           (choice (completing-read (concat "Add to `" fun-name "': ")
                                    '("INLINE" "INLINABLE" "SPECIALISE"))))
      (insert "{-# " choice " " fun-name " #-}")))

  :bind (:map haskell-mode-map
              ("C-c M-." . hoogle                         )
              ("C-c C-p" . slot/haskell-add-pragma        )
              ([f5]      . haskell-compile                )
              ;; Jump to the import blocks and back in current file.
              ([f12]     . haskell-navigate-imports       )
              ([f11]     . haskell-navigate-imports-return)
              ;; Interactive stuff
              ("C-c C-c" . slot/haskell-load-and-bring    )
              ("C-c C-z" . haskell-interactive-switch     )
              ;; For the times when the LSP stuff fails
              ("C-c ."   . haskell-process-do-type        )
              ("C-c ,"   . haskell-process-do-info        )
              ("C-M-;"   . haskell-mode-jump-to-def-or-tag)
              :map puni-mode-map
              ("C-d"     . delete-char                    )
              ("DEL"     . delete-backward-char           ))
  :custom
  (haskell-interactive-popup-errors nil) ; Don't pop up errors in a separate buffer.
  (haskell-indentation-where-pre-offset  1)
  (haskell-indentation-where-post-offset 1)
  (haskell-process-auto-import-loaded-modules t))

(use-package hindent
  :hook ((haskell-mode lsp-mode) . hindent-mode)
  :custom
  (hindent-process-path "fourmolu")
  (hindent-extra-args '("-o" "-XBangPatterns"
                        "-o" "-XTypeApplications"
                        "--indentation" "2")))

;; Yes, this is a mode for a config file I will probably edit for a
;; grand total of 5 minutes ever.  Fight me.
(use-package ghci-conf-mode             ; 28nov2020
  :ensure nil)

;;; Shell
(use-package emacs
  :hook
  (sh-mode . flycheck-mode)             ; Uses shell-check
  ;; When saving a file that starts with "#!", make it executable.
  (after-save . executable-make-buffer-file-executable-if-script-p))

;;; Agda --- `agda2-mode' is a masterpiece.
;; 20sep2020, 17sep2021
(advice-add 'toggle-input-method :before
  (lambda ()
    (interactive)
    (add-to-list 'load-path (f-dirname (shell-command-to-string "agda-mode locate")))
    (require 'agda2-mode)))

;;; Rust
;; Rust is like Haskell but without the cool functional-ness!
(use-package rustic ; 05feb2020, 12feb2022, 28aug2022 rust-mode -> rustic-mode
  :vc (:url "https://github.com/emacs-rustic/rustic")
  :bind (:map rustic-mode-map
              ("C-c m" . lsp-rust-analyzer-expand-macro)
              ([f5]    . rustic-cargo-build))
  :config (put 'rustic-indent-offset 'safe-local-variable #'numberp)
  :custom (rust-mode-treesitter-derive nil))

;;; Clojure
;; Clojure is like Haskell but without the cool type system!  If you say
;; homoiconic in front of your bathroom mirror three times with the
;; lights out, the ghost of John McCarthy appears and hands you a
;; parenthesis.
(use-package cider
  :hook (((cider-mode cider-repl-mode) . cider-company-enable-fuzzy-completion)
         (cider-mode . (lambda ()
                         (require 'flycheck-clj-kondo)
                         (flycheck-mode))))
  :bind (:map cider-mode-map
              ("C-c C-v C-e" . cider-eval-buffer)
              ("C-c C-v M-;" . cider-pprint-eval-last-sexp-to-comment))
  :custom (cider-enrich-classpath t)
  :config (use-package flycheck-clj-kondo :defer nil))

;;; Emacs Lisp
(use-package emacs                      ; 15jan2023, 12mar2023
  :bind (:map emacs-lisp-mode-map
              ("C-c <return>"   . pp-macroexpand-last-sexp)
              ("C-c C-<return>" . emacs-lisp-macroexpand))
  :hook (emacs-lisp-mode . (lambda () (setq-local sentence-end-double-space t))))

;; Always ensure that everything is indented properly.
(use-package aggressive-indent
  :hook ((emacs-lisp-mode clojure-mode) . aggressive-indent-mode))

;;; Nixlang
(use-package nix-mode
  :vc ( :url "https://github.com/slotThe/nix-mode"
        :branch "nix-indent-line")
  :custom (nix-indent-function #'nix-indent-line))

;;; OCaml, 23apr2023 31oct2023
(use-package tuareg
  :mode (("\\.ocamlinit\\'" . tuareg-mode))
  :hook (tuareg-mode . (lambda ()
                         (setq-local compile-command "dune build ")))
  :custom (tuareg-prettify-symbols-full t))

(use-package dune
  :after tuareg)

;;; Python, 29aug2023
(use-package python
  :ensure nil
  :custom (python-indent-guess-indent-offset-verbose nil))

;;; Markdown
(use-package markdown-mode
  ;; Associate .md files with github-flavoured markdown (this is
  ;; _really_ sad, but I guess that's what we have to deal with when it
  ;; comes to monopolies).
  :mode (("\\.md$" . gfm-mode))
  :commands gfm-mode
  :bind (:map markdown-mode-map ("C-c l" . slot/often-used-links))
  :custom (markdown-command "pandoc --standalone --mathjax --from=markdown"))

(defun slot/often-used-links (&optional arg)
  "Choose a link and insert it into the buffer in .md format.
This is quite useful, since many people happen to have very
similar problems when, for example, first starting out with
xmonad."
  (interactive "P")
  (cl-flet
      ((get-xmonad-modules ()
         "Get all XMonad modules in the form (NAME . DOC-URL)."
         (let* ((xmonad-cabal "~/repos/xmonad/xmonad-contrib/xmonad-contrib.cabal")
                (modules (shell-command-to-string
                          (format "tail -n +50 %s | grep -E \" XMonad\\.*\""
                                  xmonad-cabal))))
           (->> (s-lines modules)
                (-drop-last 1)          ; empty line
                (--map (s-trim (s-replace "exposed-modules:" "" it)))
                (--map (cons it
                             (format "https://hackage.haskell.org/package/xmonad-contrib/docs/%s.html"
                                     (s-replace "." "-" it)))))))
       (get-posts ()
         "Get all of my blog posts in the form (NAME . URL)."
         (let* ((website "https://tony-zorman.com/")
                (base-path "~/repos/slotThe.github.io/")
                (posts (directory-files-recursively (concat base-path "posts/") ".md$")))
           (--map (with-temp-buffer
                    (insert-file-contents-literally it)
                    (search-forward "title: ")
                    (cons               ; Name . URL
                     (string-replace "\"" "" (buffer-substring (point) (point-at-eol)))
                     (concat website (string-trim it base-path ".md") ".html")))
                  posts))))
    (-let* ((links
             (-concat '(("tutorial" . "https://xmonad.org/TUTORIAL.html")
                        ("install"  . "https://xmonad.org/INSTALL.html")
                        ("xmonad.hs". "https://gitlab.com/slotThe/dotfiles/-/blob/master/xmonad/src/xmonad.hs"))
                      (get-xmonad-modules)
                      (get-posts)))
            (choice (completing-read "Link: " (mapcar #'car links)))
            ((name . link) (assoc choice links)))
      (insert "[" name "]")
      (if arg
          (insert "(" link ")")
        (save-excursion (insert "\n\n[" name "]: " link))))))

;;; `kbd-mode'
(use-package kbd-mode ; 24sep2020 https://github.com/kmonad/kbd-mode
  :vc (:url "https://github.com/kmonad/kbd-mode")
  :mode "\\.kbd\\'"
  :hook (kbd-mode . (lambda ()
                      (aggressive-indent-mode -1)
                      (electric-indent-mode -1)
                      (apheleia-mode -1)))
  :commands kbd-mode
  :custom
  (kbd-mode-kill-kmonad "pkill -9 kmonad")
  (kbd-mode-start-kmonad "kmonad ~/.config/kmonad/config.kbd"))

;;; `lsp-mode'
(use-package lsp-mode ; 05feb2020, 23mar2020, 28oct2021, 12dec2021
  :preface
  (defun slot/syntax-highlight-string (str mode)
    "Syntax highlight STR in MODE."
    (with-temp-buffer
      (insert str)
      ;; We definitely don't want to call certain modes, so delay the mode's
      ;; hooks until we have removed them.
      (delay-mode-hooks (funcall mode))
      (-map #'funcall
            (--remove (-contains? '(lsp-mode lsp-deferred flymake-mode-off) it)
                      (-mapcat #'symbol-value delayed-mode-hooks)))
      ;; Now we can propertise the string.
      (font-lock-ensure)
      (buffer-string)))

  (defun slot/lsp-get-type-signature (lang str)
    "Get LANGs type signature in STR.
Original implementation from https://github.com/emacs-lsp/lsp-mode/pull/1740."
    (let* ((start (concat "```" lang))
           (groups (--filter (s-equals? start (car it))
                             (-partition-by #'s-blank? (s-lines (s-trim str)))))
           (name-at-point (symbol-name (symbol-at-point)))
           (type-sig-group (car
                            (--filter (--any? (s-contains? name-at-point it) it)
                                      groups))))
      (->> (or type-sig-group (car groups))
           (-drop 1)                    ; ``` LANG
           (-drop-last 1)               ; ```
           (-map #'s-trim)
           (--filter (not (s-prefix? comment-start it))) ; e.g. rust-analyzer puts size hints here
           (s-join " "))))

  (defun slot/lsp-get-type-signature-at-point (&optional lang)
    "Get LANGs type signature at point.
If LANG is not given, get it from `lsp--buffer-language'."
    (interactive)
    (-some->> (lsp--text-document-position-params)
      (lsp--make-request "textDocument/hover")
      lsp--send-request
      lsp:hover-contents
      (funcall (-flip #'plist-get) :value)
      (slot/lsp-get-type-signature (or lang lsp--buffer-language))))

  (defun slot/lsp-show-type-signature ()
    "Show the type signature for the thing at point.
This is essentially what `lsp-clients-extract-signature-on-hover'
does, just as an extra function."
    (interactive)
    (message (slot/syntax-highlight-string
              (slot/lsp-get-type-signature-at-point)
              major-mode)))

  (defun slot/lsp-shutdown-last-workspaces ()
    "Shut down the `lsp--last-active-workspaces'."
    (interactive)
    (mapc (lambda (ws)
            (lsp--warn "Stopping %s" (lsp--workspace-print ws))
            (with-lsp-workspace ws (lsp--shutdown-workspace)))
          lsp--last-active-workspaces))

  (defun slot/orderless-dispatch-flex-first (_pattern index _total)
    (and (eq index 0) 'orderless-flex))

  (defun slot/lsp-mode-setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
          '(orderless))
    (add-hook 'orderless-style-dispatchers #'slot/orderless-dispatch-flex-first nil 'local)
    (setq-local completion-at-point-functions (list (cape-capf-buster #'lsp-completion-at-point))))

  ;; `emacs-lsp-booster' integration
  (define-advice json-parse-buffer
      (:around (old-fn &rest args) lsp-booster-parse-bytecode)
    "Try to parse bytecode instead of json."
    (or (when (equal (following-char) ?#)
          (let ((bytecode (read (current-buffer))))
            (when (byte-code-function-p bytecode)
              (funcall bytecode))))
        (apply old-fn args)))
  (define-advice lsp-resolve-final-command
      (:around (old-fn cmd &optional test?) add-lsp-server-booster)
    "Prepend emacs-lsp-booster command to lsp CMD."
    (let ((orig-result (funcall old-fn cmd test?)))
      (if (and (not test?)                             ; lsp-server-present?
               (not (file-remote-p default-directory)) ; see lsp-resolve-final-command, it would add extra shell wrapper
               lsp-use-plists
               (not (functionp 'json-rpc-connection))  ; native json-rpc
               (executable-find "emacs-lsp-booster"))
          (progn
            (message "Using emacs-lsp-booster for %s!" orig-result)
            (cons "emacs-lsp-booster" orig-result))
        orig-result)))

  ;; Interaction with `slot/escape'.
  (add-hook 'slot/escape-hook
            (lambda () (when (bound-and-true-p lsp-signature-mode)
                    (lsp-signature-stop) t)))

  :commands (lsp-deferred lsp)

  :hook
  ((haskell-mode tuareg-mode python-mode) . lsp-deferred) ; Rust is taken care of by `rustic-mode'
  (lsp-mode . (lambda ()
                (setq-local read-process-output-max (* 1024 1024))))
  (lsp-completion-mode  . slot/lsp-mode-setup-completion) ; For `corfu' compatibility
  :bind (:map lsp-mode-map
              ("C-c C-s"   . slot/lsp-show-type-signature)
              ("C-c C-r"   . lsp-execute-code-action)
              ("C-c C-d"   . lsp-ui-doc-show)
              ("C-c l i"   . lsp-ui-imenu)
              ("C-c l h i" . lsp-inlay-hints-mode)
              ("C-c l s"   . (lambda ()
                               (interactive)
                               (lsp-ui-sideline-enable (not lsp-ui-sideline-mode))))
              ("C-c d"     . (lambda ()
                               (interactive)
                               (if lsp-lens-mode
                                   (progn (lsp-lens-mode -1)
                                          (lsp-lens-hide))
                                 (lsp-lens-mode 1)
                                 (lsp-lens-show)))))

  :custom
  (lsp-keep-workspace-alive nil)
  (lsp-use-plists t)
  (lsp-completion-provider :none)       ; Corfu
  (lsp-keymap-prefix "C-c l")
  (lsp-idle-delay 0.6)
  ;; Disable things I don't care about
  (lsp-headerline-breadcrumb-enable nil)
  (lsp-modeline-code-actions-enable nil)
  (lsp-modeline-diagnostics-enable nil)
  (lsp-enable-symbol-highlighting nil)
  (lsp-enable-text-document-color nil)
  (lsp-enable-folding nil)
  (lsp-enable-on-type-formatting nil))

(use-package lsp-ui
  :after lsp-mode
  :commands lsp-ui-mode
  :config
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references]  #'lsp-ui-peek-find-references)
  :custom
  (lsp-ui-doc-max-width 90)
  (lsp-ui-doc-show-with-mouse nil)
  (lsp-ui-doc-position 'at-point)
  (lsp-ui-peek-always-show t)
  ;; Sideline config
  (lsp-ui-sideline-ignore-duplicate t)
  (lsp-ui-sideline-diagnostic-max-lines 3))

;;;; Language specific LSP things

;;;;; Haskell
(use-package lsp-haskell
  :after lsp-mode
  :preface
  (defun slot/lsp-haskell-type-signature ()
    "Add a type signature for the thing at point.
This is very convenient, for example, when dealing with local
functions, since those—as opposed to top-level expressions—don't
have a code lens for \"add type signature here\" associated with
them."
    (interactive)
    (let* ((value (slot/lsp-get-type-signature-at-point "haskell")))
      (slot/back-to-indentation)
      (insert value)
      (haskell-indentation-newline-and-indent)))
  :hook
  (lsp-lsp-haskell-after-open
   . (lambda ()
       ;; Fixes https://github.com/emacs-lsp/lsp-haskell/issues/151
       (cl-defmethod lsp-clients-extract-signature-on-hover (contents (_server-id (eql lsp-haskell)))
         "Display the type signature of the function under point."
         (slot/syntax-highlight-string
          (slot/lsp-get-type-signature "haskell" (plist-get contents :value))
          'haskell-mode))))
  :bind (:map lsp-mode-map
              ("C-c C-t" . slot/lsp-haskell-type-signature))
  :custom
  (lsp-haskell-plugin-stan-global-on nil)
  (lsp-haskell-plugin-import-lens-code-lens-on nil)
  (lsp-haskell-plugin-import-lens-code-actions-on nil))

;;;;; Rust
(use-package emacs
  :after (rust-mode lsp-mode)
  :hook (lsp-rust-analyzer-after-open
         . (lambda ()
             (cl-defmethod lsp-clients-extract-signature-on-hover (contents (_server-id (eql rust-analyzer)))
               "Display the type signature of the function under point."
               (slot/syntax-highlight-string
                (slot/lsp-get-type-signature "rust" (plist-get contents :value))
                'rustic-mode))
             ;; See https://github.com/emacs-lsp/lsp-mode/pull/1740#issuecomment-1319485376
             (advice-add #'lsp-eldoc-function :after
               (lambda (&rest _) (setq-local lsp--hover-saved-bounds nil)))
             ;; Rust sends _way_ too much here.
             (setq-local lsp-signature-render-documentation nil)))
  :custom
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial")
  (lsp-rust-analyzer-display-chaining-hints t)
  (lsp-rust-analyzer-display-closure-return-type-hints t)
  (lsp-rust-analyzer-display-parameter-hints t)
  (lsp-rust-analyzer-display-lifetime-elision-hints-use-parameter-names t)
  (lsp-rust-analyzer-exclude-dirs [".direnv"]))

;;;;; Python
(use-package lsp-pyright
  :hook (python-mode . (lambda () (require 'lsp-pyright)))
  :custom (lsp-pyright-langserver-command "basedpyright"))

;;; `paredit'
(use-package paredit                    ; 04sep2022 lispy -> paredit
  :hook ((emacs-lisp-mode clojure-mode) . enable-paredit-mode)
  :bind (:map paredit-mode-map
              ("M-s"       . nil)
              ("C-x C-d"   . paredit-splice-sexp)
              ("M-<right>" . paredit-backward-barf-sexp)
              ("M-<left>"  . paredit-backward-slurp-sexp)))

;;; `hs-mode'
(use-package hideshow                   ; 26jan2024
  :ensure nil
  :hook (rustic-mode . hs-minor-mode)
  :preface
  ;; https://karthinks.com/software/simple-folding-with-hideshow/
  (defun hs-cycle (&optional level)
    (interactive "p")
    (let (message-log-max
          (inhibit-message t))
      (if (= level 1)
          (pcase last-command
            ('hs-cycle
             (hs-hide-level 1)
             (setq this-command 'hs-cycle-children))
            ('hs-cycle-children
             (save-excursion (hs-show-block))
             (hs-show-block)
             (setq this-command 'hs-cycle-subtree))
            ('hs-cycle-subtree
             (hs-hide-block))
            (_
             (if (not (hs-already-hidden-p))
                 (hs-hide-block)
               (hs-hide-level 1)
               (setq this-command 'hs-cycle-children))))
        (hs-hide-level level)
        (setq this-command 'hs-hide-level))))
  (defun hs-global-cycle ()
    (interactive)
    (pcase last-command
      ('hs-global-cycle
       (save-excursion (hs-show-all))
       (setq this-command 'hs-global-show))
      (_ (hs-hide-all))))
  :bind ( :map hs-minor-mode-map
          ("S-<tab>"         . hs-cycle)
          ("<backtab>"       . hs-cycle) ; My laptop keyboards sends this keycode on S-<tab>.
          ("M-<iso-lefttab>" . hs-global-cycle)) ; M-S-<tab>
  :custom
  (hs-set-up-overlay
   (lambda (ov)
     (when (eq 'code (overlay-get ov 'hs))
       (overlay-put ov 'display
                    (propertize
                     (format " … (%d lines) "
                             (count-lines (overlay-start ov) (overlay-end ov)))
                     'face
                     '((t (:inherit font-lock-comment-face :weight light)))))))))

;;; `gnu-apl-mode'
(use-package gnu-apl-mode
  :commands gnu-apl gnu-apl-mode
  :custom (gnu-apl-show-keymap-on-startup nil)
  :preface
  ;; `kbd' does not like sequences of the form `s-..' an I don't use these
  ;; anyways, instead preferring APLs input method in all situations. Still
  ;; want some keybindings, though.
  (advice-add 'gnu-apl--make-base-mode-map :override
    (lambda (prefix)
      (let ((map (make-sparse-keymap)))
        (define-key map (kbd (concat prefix "SPC")) 'gnu-apl-insert-spc)
        (define-key map (kbd "C-c C-k") 'gnu-apl-show-keyboard)
        (define-key map (kbd "C-c C-h") 'gnu-apl-show-help-for-symbol)
        (define-key map (kbd "C-c C-a") 'gnu-apl-apropos-symbol)
        (define-key map (kbd "M-.") 'gnu-apl-find-function-at-point)
        (define-key map (kbd "C-c C-.") 'gnu-apl-trace)
        (define-key map (kbd "C-c C-i") 'gnu-apl-finnapl-list)
        map)))
  :init
  (advice-add 'toggle-input-method :before (lambda () (require 'gnu-apl-mode)))
  ;; Re-create the Dyalog keyboard as best as possible.
  (setq gnu-apl--symbols
        '(("diamond" "◊" "`") ("diaeresis" "¨" "1") ("i-beam" "⌶" "!") ("macron" "¯" "2") ("del-tilde" "⍫" "@") ("less-than" "<" "3") ("del-stile" "⍒" "#") ("less-than-or-equal-to" "≤" "4") ("delta-stile" "⍋" "$") ("equals" "=" "5") ("circle-stile" "⌽" "%") ("greater-than-or-equal-to" "≥" "6") ("circle-backslash" "⍉" "^") ("greater-than" ">" "7") ("circled-minus" "⊖" "&") ("not-equal-to" "≠" "8") ("circle-star" "⍟" "*") ("logical-or" "∨" "9") ("down-caret-tilde" "⍱" "(") ("logical-and" "∧" "0") ("up-caret-tilde" "⍲" ")") ("multiplication-sign" "×" "-") ("exclamation-mark" "!" "_") ("division-sign" "÷" "=") ("quad-divide" "⌹" "+") ("question-mark" "?" "q") ("omega" "⍵" "w") ("omega-underbar" "⍹" "W") ("epsilon" "∊" "e") ("epsilon-underbar" "⍷" "E") ("rho" "⍴" "r") ("tilde" "∼" "t") ("tilde-diaeresis" "⍨" "T") ("uparrow" "↑" "y") ("yen-sign" "¥" "Y") ("downarrow" "↓" "u") ("iota" "⍳" "i") ("iota-underbar" "⍸" "I") ("circle" "○" "o") ("circle-diaeresis" "⍥" "O") ("star-operator" "⋆" "p") ("star-diaeresis" "⍣" "P") ("leftarrow" "←" "[") ("quote-quad" "⍞" "{") ("rightarrow" "→" "]") ("zilde" "⍬" "}") ("right-tack" "⊢" "\\") ("left-tack" "⊣" "|") ("alpha" "⍺" "a") ("alpha-underbar" "⍶" "A") ("left-ceiling" "⌈" "s") ("left-floor" "⌊" "d") ("underscore" "_" "f") ("del-tilde" "⍫" "F") ("nabla" "∇" "g") ("increment" "∆" "h") ("delta-underbar" "⍙" "H") ("ring-operator" "∘" "j") ("jot-diaeresis" "⍤" "J") ("apostrophe" "'" "k") ("quad-equals" "⌸" "K") ("quad" "⎕" "l") ("squish-quad" "⌷" "L") ("down-tack-jot" "⍎" ";") ("identical-to" "≡" ":") ("up-tack-jot" "⍕" "'") ("not-identical-to" "≢" "\"") ("subset-of" "⊂" "z") ("subseteq" "⊆" "Z") ("superset-of" "⊃" "x") ("greek-letter-chi" "χ" "X") ("intersection" "∩" "c") ("left-shoe-stile" "⍧" "C") ("union" "∪" "v") ("up-tack" "⊥" "b") ("pound-sign" "£" "B") ("down-tack" "⊤" "n") ("divides" "|" "m") ("shoe-jot" "⍝" ",") ("comma-bar" "⍪" "<") ("backslash-bar" "⍀" "..") ("slash-bar" "⌿" "/") ("quad-colon" "⍠" "?") ("pi" "e") ("root" "√") ("inverted-exclamation-mark" "¡") ("quad-backslash" "⍂") ("inverted-question-mark" "¿"))))

(use-package bqn-mode
  :hook ((bqn-mode bqn-comint-mode) . (lambda () (corfu-mode -1)))
  :custom
  (bqn-glyph-prefix ?.)
  (bqn-comint-use-overlay t)
  :config
  (face-spec-set 'bqn-default '((t (:family "Hopf Mono"))))
  (face-spec-set 'bqn-primitive-function '((t (:inherit bqn-default))))
  (face-spec-set 'bqn-function '((t (:inherit bqn-default))))
  (face-spec-set 'bqn-arrow '((t (:inherit bqn-primitive-two-modifier))))
  :preface
  (defun slot/bqn-standalone ()
    (interactive)
    (let ((bqn-comint--process-name "BQN-standalone"))
      (switch-to-buffer (bqn-comint-buffer))))
  :bind ( :map bqn-mode-map
          ("C-c C-e" . bqn-comint-eval-dwim)
          ("C-c C-c" . bqn-comint-eval-dwim)
          ("C-c M-c" . bqn-comint-send-dwim)
          ("C-c C-h" . bqn-help-symbol-info-at-point)
          ("C-c C-b" . bqn-comint-eval-buffer)
          ("C-c C-k" . bqn-keymap-mode-show-keyboard)
          ("C-c M-e" . bqn-glyph-mode-show-glyphs)))

(provide 'hopf-programming)
