;;; rss.el --- `elfeed' setup -*- lexical-binding: t; -*-

;;; `elfeed'
(use-package elfeed                     ; 07oct2021
  :hook (elfeed-show-mode . olivetti-mode)
  :preface
  (defun slot/arXiv ()
    (interactive)
    (elfeed-search-set-filter "+unread +arXiv"))

  (defun slot/elfeed-print-entry (entry)
    (if (string-match-p "+arXiv" elfeed-search-filter)
        (slot/arXiv-print-entry entry)
      (elfeed-search-print-entry--default entry)))

  (defun slot/elfeed-browser (&optional arg)
    (interactive "P")
    (let* ((entry (if (eq major-mode 'elfeed-show-mode)
                      elfeed-show-entry
                    (elfeed-search-selected :ignore-region)))
           (link (elfeed-entry-link entry)))
      (if arg
          (call-process "firefox" nil 0 nil link)
        (when (eq major-mode 'elfeed-search-mode)
          (elfeed-search-show-entry entry))
        (eww link)
        (add-hook 'eww-after-render-hook 'eww-readable nil t))))

  :bind (:map elfeed-search-mode-map
              ("o" . slot/elfeed-browser)
              ("a" . slot/arXiv)
              :map elfeed-show-mode-map
              ("o"     . slot/elfeed-browser)
              ("C-c C-x C-l" . org-latex-preview)
              ("C-c d" . arxiv-citation-elfeed))
  :config
  (defun elfeed-show-entry (entry)
    "Display ENTRY in the current buffer.
Monkey patching: https://github.com/skeeto/elfeed/pull/521"
    (let ((buff (get-buffer-create (elfeed-show--buffer-name entry))))
      (with-current-buffer buff
        (elfeed-show-mode)
        (setq elfeed-show-entry entry))
      (funcall elfeed-show-entry-switch buff)
      (elfeed-show-refresh)))
  ;; XXX: does not work when under `:custom'
  (setq elfeed-search-print-entry-function #'slot/elfeed-print-entry)
  :custom
  (elfeed-db-directory (concat user-emacs-directory "elfeed/"))
  (elfeed-search-filter "@2-weeks-ago -arXiv +unread")
  (elfeed-show-entry-switch 'pop-to-buffer)
  (elfeed-show-entry-delete 'delete-window))

;;;; arXiv integration
;; 02jan2022, 23feb2022 elfeed-score, 14apr2022 download

(use-package arxiv-citation
  :commands (arxiv-citation-elfeed arxiv-citation arxiv-citation-gui)
  :custom
  (arxiv-citation-library "~/uni/books")
  (arxiv-citation-open-pdf-function
   (lambda (url)
     (call-process "decide-link.sh" nil 0 nil url))))

;; From: https://cundy.me/post/elfeed/
(defun slot/arXiv-print-entry (entry)
  "Pretty print arXiv entries.
Implementation mostly stolen from elfeed's default printing
function; i.e., `elfeed-search-print-entry--default'."
  (let* ((date (elfeed-search-format-date (elfeed-entry-date entry)))

         (title (or (elfeed-meta entry :title)
                    (elfeed-entry-title entry) ""))
         (title-faces (elfeed-search--faces (elfeed-entry-tags entry)))
         (title-column (elfeed-format-column title 80 :left))

         (entry-authors (s-join ", "
                                (--map (plist-get it :name)
                                       (elfeed-meta entry :authors))))
         (authors-column (elfeed-format-column entry-authors 52 :left))

         (feed (elfeed-entry-feed entry))
         (feed-column (let ((ft (or (elfeed-meta feed :title)
                                    (elfeed-feed-title feed))))
                        (elfeed-format-column
                         (cond ((s-matches? "math.AT" ft) "Algebraic Topology")
                               ((s-matches? "math.CT" ft) "Category Theory")
                               ((s-matches? "math.KT" ft) "Homological Algebra")
                               ((s-matches? "math.QA" ft) "Quantum Algebra"))
                         (length "Homological Algebra") :left))))
    (insert (propertize date 'face 'elfeed-search-date-face) " ")
    (insert (propertize title-column 'face title-faces 'kbd-help title) " ")
    (insert (propertize feed-column 'face 'elfeed-search-feed-face) " ")
    (insert (propertize authors-column 'kbd-help entry-authors) " ")))

(provide 'hopf-rss)
