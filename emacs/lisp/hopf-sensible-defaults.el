;;; sensible-defaults.el --- Finally, sensible defaults for Emacs! -*- lexical-binding: t; -*-

(setq-default
 cursor-type 'hbar
 cursor-in-non-selected-windows nil) ; Hide cursor when window is unfocused

;; Using setopt is much slower here, even though none of the variables has an
;; extra :initialize keyword. Curious.
(setq
 kill-do-not-save-duplicates t       ; Don't save duplicates to kill ring
 mouse-yank-at-point t               ; Yank text to point, not to mouse cursor
 sentence-end-double-space nil       ; 30mar2023
 kill-whole-line t           ; `kill-line' with no arg at start of line kills the whole line
 idle-update-delay 1.0       ; Wait this long before updating things on screen
 epg-pinentry-mode 'loopback ; Query passphrases through the minibuffer
;;;; Backups
 backup-directory-alist               ; Where to store backup files
 `(("." . ,(expand-file-name "backups/" user-emacs-directory)))
 backup-by-copying   t                ; Copy the whole file when backing it up
 version-control     t                ; Verison numbers for backup files
 delete-old-versions t                ; Delete excess backup version silently
 kept-old-versions   5
 kept-new-versions   10
 words-include-escapes t                 ; Treat escape chars as part of words
 truncate-string-ellipsis "…"           ; Unicode for string truncation
 x-stretch-cursor t                      ; Stretch cursor to glyph width
 redisplay-skip-fontification-on-input t ; Skip fontification when there is input pending
 fast-but-imprecise-scrolling t          ; Don't fontify too much when scrolling fast
 read-process-output-max (* 64 1024)
;;;; Emacs prompting me
 use-short-answers t          ; Why would you ever want to type a full yes?
 y-or-n-p-use-read-key t      ; read-key instead of minibuffer (more flexible)
;;;;  Emacs trying to "help" me.
 visible-bell t                          ; Visual bell instead of audio bell
 disabled-command-function nil           ; Allow all the things
 )

;;; `ultra-scroll': Like `pixel-scroll-precision-mode' but with sensible
;;; behaviour for large images!
(use-package ultra-scroll
  :vc (:url "https://github.com/jdtsmith/ultra-scroll")
  :hook (server-after-make-frame . ultra-scroll-mode)
  :custom
  (auto-window-vscroll nil)            ; Don't adjust window-vscroll for tall lines
  (next-screen-context-lines 4)        ; Preserve this many lines when jumping a screenful
  (scroll-margin 0)                    ; Scroll when cursor is this many lines to screen edge
  (scroll-conservatively 101)          ; Only 'jump' when moving this far off the screen
  (scroll-preserve-screen-position t)) ; Preserve line/column (nicer M-v, C-v, etc.)

;; Startup screen shenanigans
(setq inhibit-startup-screen t)
(fset 'display-startup-echo-area-message #'ignore)
(fset 'display-startup-screen #'ignore)

;;; https://trojansource.codes/trojan-source.pdf
(use-package emacs
  :hook (prog-mode . glyphless-display-mode)
  :config
  (setq bidi-inhibit-bpa t)
  (setq-default bidi-paragraph-direction 'left-to-right))

;;; Whitespace
(use-package emacs
  :hook
  (before-save . delete-trailing-whitespace)
  (prog-mode . (lambda ()
                 (setq-local fill-column 78)))
  :config
  (setq require-final-newline t)      ; Require a final newline—POSIX BOIS
  (setq-default fill-column 72))      ; Default column line length.

;;; `browse-url'
(use-package browse-url                 ; 14oct2022
  :custom
  (browse-url-generic-program "decide-link.sh")
  (browse-url-browser-function #'browse-url-generic))

;;; Pop mark
(use-package emacs ; 14aug2023 https://endlessparentheses.com/faster-pop-to-mark-command.html
  :custom (set-mark-command-repeat-pop t)
  :config (advice-add 'pop-to-mark-command :around ; Pop until the cursor actually moves
            (lambda (pop-to-mark &rest args)
              (let ((p (point)))
                (dotimes (_ 5)
                  (when (= p (point))
                    (apply pop-to-mark args)))))))

;;; Line and Column Numbers

;; 12oct2020, 17jan2022 remove keybinding, 12feb2022 remove fn
(use-package emacs
  :custom (column-number-mode t)        ; for the modeline
  :config (advice-add 'display-line-numbers-mode :after
            (lambda (_)
              (when display-line-numbers-mode
                (setq display-line-numbers 'visual
                      display-line-numbers-current-absolute t)))))

;;; Utility

;; 27oct2021
(defun unwords (&rest xs)
  (mapconcat #'identity xs " "))

;; 21sep2021, 02sep2022
(defmacro defrepeatmap (symbol pairs &optional docstring)
  "A macro for defining `repeat-map's.
Defines a new repeat-map called SYMBOL with the given DOCSTRING.
The keys are derived via the list PAIRS, whose elements are cons
cells of the form (KEY . DEF), where KEY and DEF must fulfill the
same requirements as if given to `keymap-set'.

If the key only consists of a single character; i.e., is already
bound and a repeat map is created afterwards, simply add it to
the repeat-map SYMBOL.  If not, globally bind KEY to DEF and only
insert the last character of DEF into the repeat map SYMBOL."
  (declare (indent 1) (debug t))
  `(progn
     (defvar ,symbol
       (let ((map (make-sparse-keymap)))
         (--each ,pairs
           (-let (((key . fun) it))
             (if (length= key 1)
                 (keymap-set map key fun)
               (bind-key (kbd key) fun)
               (keymap-set map (s-right 1 key) fun))))
         map)
       ,docstring)
     ;; Tell the keys they are in a repeat map.
     (--each (mapcar 'cdr (cdr ,symbol))
       (put it 'repeat-map ',symbol))))

;; 26aug2023
(put 'advice-add 'lisp-indent-function 'defun)

(provide 'hopf-sensible-defaults)
