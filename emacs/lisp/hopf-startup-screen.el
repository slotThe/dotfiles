;;; startup-screen.el --- A minimal startup screen -*- lexical-binding: t; -*-

;;; Code:

(defun slot/startup-screen ()
  "Customize the startup screen."
  (setq initial-major-mode 'fundamental-mode
        initial-scratch-message (unwords ";;" (slot/random-startup-msg))))

(defun slot/random-startup-msg ()
  "Get a randomized startup message—we like to have fun in life."
  (let ((msgs '("Welcome to Emacs, the thermonuclear editor."
                "Vi Vi Vi, the editor of the beast."
                "Welcome to the church of Emacs."
                "While any text editor can save your files, only Emacs can save your soul."
                "You'd be better off selling T-shirts that say \"WHAT PART OF\" (and then the Hindley–Milner prinicipal-type algorithm) \"DON'T YOU UNDERSTAND?\"."
                "A comathematician is someone who turns cotheorems into ffee."
                "Carpe diem. Seize the day, boys. Make your lives extraordinary."
                "Mama always said life was like a box of chocolates. You never know what you're gonna get."
                "I am big! It's the pictures that got small."
                "I love the smell of napalm in the morning."
                "A coconut is really just a nut."
                "I showed you my source code, pls respond."
                "I’m using Linux, a library that Emacs uses to communicate with Intel hardware."
                "Homs sind a priori Vektorräume."
                "Representation theory is the glue that holds mathematics together."
                "Ich war letztens in der Zug Mensa."
                "I'm so wobbely!"
                "Y ≔ λf.(λx.f(x x))(λx.f(x x))"
                "I almost wish I hadn’t gone down that rabbit-hole—and yet—and yet—it’s rather curious, you know, this sort of life!"
                "[D]ie Monade, das unvollkommenste aller Wesen […]"
                "The struggle itself toward the heights is enough to fill a man's heart. One must imagine Sisyphus happy."
                "A monad is just a monoid in the category of endofunctors, what’s the problem?"
                )))
    (nth (random (length msgs)) msgs)))

(add-hook 'after-init-hook 'slot/startup-screen)

(provide 'hopf-startup-screen)
;;; startup-screen.el ends here
