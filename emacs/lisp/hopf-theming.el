;;; theming.el --- Themes and fonts -*- lexical-binding: t; -*-

;;; Mode Line
;; 05aug2020, 20sep2020 `battery-mode', 30may2021 rm `battery-mode', 16nov2023 size

(use-package emacs                      ; 01jan2023 fresh start
  :preface
  ;; Input method
  (defvar-local slot/mode-line-input-method ; 30jul2023
      '("" (:propertize ("" current-input-method-title)
                        face fixed-pitch)))
  (put 'slot/mode-line-input-method 'risky-local-variable t)
  ;; Buffer identification
  (defvar-local slot/buffer-identification nil) ; 06jan2023 this is all I need
  (put 'slot/buffer-identification 'risky-local-variable t)
  (defun slot/generate-buffer-identification ()
    (cl-flet* ((stitch-together (xs)
                 (mapconcat #'identity xs "/"))
               (shorten-directory (rest max-length)
                 (stitch-together
                  (cons "…"
                        (cdr (--reduce-from
                              (if (< (car acc) max-length)
                                  (cons (+ (car acc) (string-width it))
                                        (cons it (cdr acc)))
                                acc)
                              (cons 0 '())
                              (nreverse rest))))))
               (special-buffer (name)
                 (unless (or (not name) (s-contains? "*message*" name))
                   name)))
      (setq slot/buffer-identification
            (-if-let* ((buf-name (buffer-name))
                       (buf-fn (special-buffer (buffer-file-name)))
                       (path (abbreviate-file-name (file-name-directory buf-fn)))
                       ((prefix . rest) (f-split path))
                       (max-size (max 0 (- 35 (string-width prefix) (string-width buf-name)))))
                (concat (if (length< path max-size)
                            path
                          (concat prefix "/" (shorten-directory rest max-size) "/"))
                        (propertize buf-name 'face '(:inherit font-lock-builtin-face)))
              (propertize buf-name 'face '(:inherit font-lock-builtin-face))))))
  :custom
  (mode-line-position-column-line-format '("%l,%c"))
  (mode-line-buffer-identification
   '("" (slot/buffer-identification
         slot/buffer-identification
         (:eval (slot/generate-buffer-identification)))))
  (mode-line-format
   '("%e" mode-line-front-space
     (:eval (if buffer-read-only "  " ""))
     "   " mode-line-buffer-identification "   "
     mode-line-position (vc-mode vc-mode) "  " mode-line-modes
     mode-line-misc-info
     "   " slot/mode-line-input-method
     mode-line-end-spaces))
  :init
  (when-let ((mma (cl-member-if
                   (lambda (x) (and (listp x)
                               (equal (car x) :propertize)
                               (equal (cadr x) '("" minor-mode-alist))))
                   mode-line-modes)))
    ;; No minor-mode list and no parens around the major mode.
    (setcar mma nil)
    (--each '("(" ")") (delete it mode-line-modes))
    ;; Keep the modeline at a reasonable size.
    (custom-set-faces '(mode-line          ((t :height 90))))
    (custom-set-faces '(mode-line-inactive ((t :height 90))))))

;;; Default Font
;; 09sep2020, 26dec2020, 18jan2022 stop ruining my fonts!, 15feb2023 use fontaine instead

(use-package fontaine                   ; 12feb2023
  :demand t
  :config (fontaine-set-preset 'default)
  :custom
  (fontaine-presets
   `((default :default-height 105)
     (_90     :default-height  90)
     (_120    :default-height 120)
     (_150    :default-height 150)
     (_200    :default-height 200)
     (cozette
      :default-family "Cozette"
      :fixed-pitch-family "Cozette"
      :variable-pitch-height 0.85
      :default-weight 'HiDpi
      :antialias 'none
      :default-height 260)
     (cozette-smol
      :default-family "Cozette"
      :fixed-pitch-family "Cozette"
      :variable-pitch-height 0.85
      :default-weight 'regular
      :antialias 'none
      :default-height 130)
     (t
      :default-family "Hopf Mono"
      :fixed-pitch-family "Hopf Mono"
      :variable-pitch-family "Alegreya"
      :line-spacing nil
      :variable-pitch-height 1.18))))

(use-package emacs
  :hook ((org-mode notmuch-show-mode) . variable-pitch-mode))

;;; Themes

(use-package modus-themes ; 22mar2020 modus operandi, 29dec2022 churn…, 10aug2023 term colours
  :demand t
  :config
  ;; Palette overrides. Using setq is fine here, as :initialize is set to its
  ;; default value and no theme is loaded yet, so :set does not need to
  ;; reload anything.
  (setq modus-themes-common-palette-overrides
        `((bg-paren-match bg-magenta-intense)     ; paren-match: intense
          (bg-region bg-blue-intense)             ; region: accented
          (bg-hl-line bg-blue-nuanced)            ; hl-line: accented
          (border-mode-line-inactive unspecified) ; no borders!
          (border-mode-line-active unspecified)   ; no borders!
          ,@modus-themes-preset-overrides-faint)) ; let everything be faint!
  ;; Terminal backgrounds… see https://lists.sr.ht/~protesilaos/modus-themes/%3C87fs4wforf.fsf%40hyperspace%3E
  (dolist (colour '("red" "green" "yellow" "blue" "magenta" "cyan"))
    (cl-flet ((intercat (&optional pref suf)
                (intern (concat "bg"   (if pref (concat "-" pref "-") "-")
                                colour (if suf  (concat "-" suf)      "")))))
      (dolist (override `((,(intercat "term") ,(intercat nil "subtle"))
                          (,(intercat "term" "bright") ,(intercat nil "intense"))))
        (add-to-list 'modus-themes-common-palette-overrides override))))
  (modus-themes-select 'modus-operandi)
  :custom
  (modus-themes-bold-constructs t)
  (modus-themes-italic-constructs t)
  (modus-themes-variable-pitch-ui t)    ; variable pitch mode line etc.
  (modus-themes-mixed-fonts t)
  (modus-themes-headings
   '((0 . (light 1.6))
     (1 . (light 1.4))
     (2 . (light 1.2))
     (3 . (light 1.0))
     (agenda-structure . (variable-pitch light 1)))))

(use-package stimmung-themes        ; 25mar2022, 04may2022
  :custom
  (stimmung-themes-comment 'foreground)
  (stimmung-themes-string  'foreground))

;;; `rainbow-mode'
;; Highlight hex colours with the colour they represent!
(use-package rainbow-mode               ; 16feb2021
  :hook ((text-mode prog-mode) . rainbow-mode))

(provide 'hopf-theming)
