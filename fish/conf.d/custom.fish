function __autols --on-variable PWD
    if status --is-interactive
        eza --group-directories-first
    end
end

# fh: repeat history ([f]zf [h]istory)
# Adapted from https://github.com/junegunn/fzf/wiki/examples
function fh
    history | awk '{ print }'                 \
            | awk '!($0 in a){ a[$0];print }' \
            | fzf --tac                       \
            | sed -r 's/ *[0-9]*\*? *//'      \
            | sed -r 's/\\/\\\\/g/'           \
            | read -l result
    [ "$result" ]; and commandline -j -- $result
    commandline -f repaint
end

# This can sometimes be useful when `decide-link' would open it in a
# browser etc.
function em
    emacsclient -a '' -c $argv & disown
end

# Create a directory and then cd into it. Source: grb dotfiles.
function mkcd
    mkdir -p $argv && cd $argv
end

abbr --add fw sudo systemctl restart NetworkManager
abbr --add d decide-link.sh
abbr --add s sxiv -a
abbr --add ssh TERM=xterm-256color ssh
abbr --add top htop
abbr --add v nvim
abbr --add vim nvim
abbr --add mpno mpv --video=no
abbr --add mpl mpv --ytdl-raw-options=yes-playlist=
abbr --add mpln mpv --video=no --ytdl-raw-options=yes-playlist=
abbr --add z zathura
abbr --add m "magit.sh -x --config ~/.config/emacs/ --eval \"(menu-bar-mode -1) (scroll-bar-mode -1) (tool-bar-mode -1) (visual-line-mode) (load-theme 'modus-operandi)\""
abbr --add restartkbd "pkill -9 kmonad ; kmonad -linfo ~/.config/kmonad/config.kbd &";
abbr --add remdev "rm -fr ./mnt; docker build -t remarkable . && docker run -ti --net=host --ipc=host -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --env='QT_X11_NO_MITSHM=1' remarkable"
abbr --add rm rm -i
abbr --add cp cp -i
abbr --add ls eza --group-directories-first
abbr --add e $(echo $EDITOR)
abbr --add n z
