{
  description = "Tony's flake—the mothership";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    systems.url = "github:nix-systems/default";
    nur = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.systems.follows = "systems";
    };
    git-ignore-nix = {
      url = "github:hercules-ci/gitignore.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.92.0.tar.gz";
      inputs = {
        flake-utils.follows = "flake-utils";
        nixpkgs.follows     = "nixpkgs";
      };
    };

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    xmonad = {
      url = "github:xmonad/xmonad";
      inputs = {
        flake-utils.follows    = "flake-utils";
        nixpkgs.follows        = "nixpkgs";
        git-ignore-nix.follows = "git-ignore-nix";
      };
    };
    xmonad-contrib = {
      url = "github:xmonad/xmonad-contrib";
      inputs = {
        flake-utils.follows    = "flake-utils";
        nixpkgs.follows        = "nixpkgs";
        git-ignore-nix.follows = "git-ignore-nix";
        xmonad.follows         = "xmonad";
      };
    };

    emacs-overlay = {
      url = "github:nix-community/emacs-overlay";
      inputs.nixpkgs.follows        = "nixpkgs";
      inputs.nixpkgs-stable.follows = "nixpkgs";
    };
    emacs-mps = {
      url = "github:emacs-mirror/emacs/feature/igc";
      flake = false;
    };

    kmonad = {
      url = "github:kmonad/kmonad?dir=nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-index-database = {
      url = "github:nix-community/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    firefox-csshacks = {
      url = "github:MrOtherGuy/firefox-csshacks";
      flake = false;
    };
  };

  outputs = {
    nixpkgs, lix-module,
    emacs-overlay, emacs-mps, kmonad, xmonad-contrib, nix-index-database, nur,
    home-manager, ...
  }@inputs:
    let
      system  = "x86_64-linux";
      my-overlays = {
        nixpkgs.overlays = [
          emacs-overlay.overlays.default
          nur.overlays.default
          (import ./nix/overlays { inherit inputs; })
        ];
      };

    in {
      nixosConfigurations.comonad = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [
          ./nix/hosts/comonad/hardware-configuration.nix
          ./nix/hosts/comonad/tuxedo.nix
          ./nix/configuration.nix
          (import ./nix/nix.nix { inherit inputs; })
          lix-module.nixosModules.default
          my-overlays
          kmonad.nixosModules.default
          nix-index-database.nixosModules.nix-index {
            programs.command-not-found.enable = false;
            programs.nix-index-database.comma.enable = true;
          }
          home-manager.nixosModules.home-manager {
            home-manager.useGlobalPkgs   = true;
            home-manager.useUserPackages = true;
            home-manager.users.slot      = import ./nix/home.nix;
            home-manager.extraSpecialArgs= inputs;
          }
        ]
        ++ xmonad-contrib.nixosModules;
      };
    };

}
