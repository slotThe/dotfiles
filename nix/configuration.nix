{ pkgs, lib, ... }:

{
  # Bootloader.
  boot = {
    loader.systemd-boot.enable      = true;
    loader.efi.canTouchEfiVariables = true;
  };

  # Supposedly better for the SSD.
  fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];

  # Keep a maximum of 15 old configurations.
  boot.loader.systemd-boot.configurationLimit = 15;

  # Initial network configuration.
  networking.networkmanager = {
    enable  = true;
    plugins = lib.mkForce [];
  };

  # Time zone and internationalisation properties.
  time.timeZone = "Europe/Berlin";
  i18n = {
    defaultLocale       = "en_GB.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS        = "de_DE.UTF-8";
      LC_IDENTIFICATION = "de_DE.UTF-8";
      LC_MEASUREMENT    = "de_DE.UTF-8";
      LC_MONETARY       = "de_DE.UTF-8";
      LC_NAME           = "de_DE.UTF-8";
      LC_NUMERIC        = "de_DE.UTF-8";
      LC_PAPER          = "de_DE.UTF-8";
      LC_TELEPHONE      = "de_DE.UTF-8";
      LC_TIME           = "en_GB.UTF-8";
      LC_ALL            = "en_GB.UTF-8";
    };
  };

  # My user account.
  users.users.slot = {
    isNormalUser = true;
    description  = "it's me!";
    shell        = pkgs.bash;
    extraGroups  = [
      "networkmanager"   # WARNING: Device may contain internet.
      "wheel"            # sudo
      "audio" "pipewire" # PipeWire
      "input" "uinput"   # KMonad
    ];
  };
  # Fish is the default, but not the login shell—it's not POSIX compliant.
  programs.fish.enable = true;

  virtualisation.podman = {
    enable = true;
    # Create a `docker` alias for podman, to use it as a drop-in replacement.
    dockerCompat = true;
    # Required for containers under podman-compose to talk to each other.
    defaultNetwork.settings.dns_enabled = true;
  };

  # XMonad
  services.xserver = {
    enable = true;
    displayManager.startx.enable = true;
    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
      flake = {
        enable   = true;
        compiler = "ghc966";
      };
      config = builtins.readFile ../xmonad/xmonad.hs;
      enableConfiguredRecompile = true;
    };
  };
  services.xscreensaver.enable = true;

  fonts.packages = with pkgs; [
    alegreya          # Main variable pitch font
    cozette
    fixeder-sys       # https://tom7.org/fixedersys
    font-awesome_4
    hopf-mono         # Main fixed pitch font
    league-spartan
    raleway
    scientifica       # Bitmap font for xmobar
  ];

  # KMonad—a sane keyboard manager
  services.kmonad.enable = true;
  services.xserver.xkb = {
    layout  = "us";
    options = "compose:menu";
  };

  # Sound
  security.rtkit.enable = true;
  services.pipewire = {
    enable             = true;
    audio.enable       = true;
    systemWide         = true;
    alsa.enable        = true;
    pulse.enable       = true;
    jack.enable        = true;
    wireplumber.enable = true;
  };

  # Initial package list.
  environment.systemPackages = with pkgs; [
    dosfstools
    lm_sensors   # core temps
    mtools
    openvpn
    qjackctl
    xscreensaver # the most fun way to lock your screen!
  ];

  # Offline dictionaries
  environment.wordlist.enable = true; # Plain-text word list.
  services.dictd = {
    enable = true;
    DBs    = with pkgs.dictdDBs; [
      wordnet
      eng2deu
      wiktionary
    ];
  };

  # Create a link to /etc/openvpn/update-resolv-conf, to prevent openvpn
  # scripts from breaking.
  environment.etc.openvpn.source = "${pkgs.update-resolv-conf}/libexec/openvpn";

  # Sigh.
  systemd.extraConfig = "DefaultTimeoutStopSec=10s";

  # Sigh²
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "dyalog"
  ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}
