{ config, pkgs, ... }:

with config.lib.my;
with builtins;
{
  # Import everything in ./modules. The ./. + "blah" coerces the relative path
  # "blah" into an absolute one; see
  #
  # https://nixos.wiki/wiki/Nix_Language:_Tips_%26_Tricks#Coercing_a_relative_path_with_interpolated_variables_to_an_absolute_path_.28for_imports.29
  imports = map (mod: ./. + "/modules/${mod}.nix") [
    "X11"
    "clojure"
    "direnv"
    "emacs"
    "email"
    "firefox"
    "fish"
    "git"
    "gpg-ssh"
    "gtk"
    "haskell"
    "home-lib"
    "kitty"
    "pass"
    "python"
    "rust"
    "zoxide"
  ];

  # Housekeeping
  home = {
    username      = "slot";
    homeDirectory = "/home/slot";
    stateVersion  = "23.05";
  };
  programs.home-manager.enable = true;

  # Set XDG variables to sensible values.
  xdg.enable = true;

  fonts.fontconfig = {
    enable = true;                       # Refresh font cache, please
    defaultFonts = {
      serif = [ "Alegreya" ];
      sansSerif = [ "League Spartan" ];
      monospace = [ "Hopf Mono" ];
      emoji = [ "Noto Color Emoji" ];
    };
  };

  home.packages = with pkgs; let h = haskellPackages; in [
    # DEVELOPERS, DEVELOPERS, DEVELOPERS!
    ## (La)TeX
    texlive.combined.scheme-full  # Abstract nonsense—ALL OF IT
    tikzit                        # Graphical abstract nonsense!
    ## Nix
    nix-output-monitor            # look ma, pretty graphics!
    ## OCaml
    dune_3                        # Haskell 30 years ago
    opam
    ocaml
    ocamlPackages.findlib
    ocamlPackages.utop
    ## APL
    (dyalog.override {            # Sigh. But dfns, dude.
      acceptLicense = true;
      dotnetSupport = true;       # LINK
    })
    gnuapl                        # {⍺←⊃⍵◊↑⍺⍺⍨/(⌽1↓⍵),⊂⍺} and I'm not even joking
    # ride                          # Get me off it XXX: https://github.com/NixOS/nixpkgs/pull/369163
    ## …the rest
    (hiPrio gcc)                  # Win when a snake fight with clang breaks out
    agda                          # Haskell 30 years from now
    autoconf
    bash
    cbqn
    cmake
    evtest                        # Debugging KMonad
    gforth
    git-crypt                     # Secretly store secrets in a secret way
    gnumake
    zlib                          # Needed by xmobar

    # CLI tools
    _7zz
    atool           # Universal unpacker so I don't have to think
    bc              # Can you imagine a calculator that supports maths with floats?
    curl            # Yes we curl!
    eza             # A better `ls'
    fd              # A better `find'
    ffmpeg          # The `imagemagick' of video editing programs
    file            # Determine the type of files
    fzf             # Fuzzy find literally anything
    gifsicle        # Creating GIFs like it's 1999
    graphviz        # Pretty pictures!
    h.duden
    h.matrix-maker
    hledger         # Finances, but in Haskell!
    htop            # A better `top'
    hyperfine       # Benchmark command line programs: my favourite procrastination tool
    imagemagick     # The `pdftk' of image manipulation programs
    ledger          # Finances
    (linkchecker.overrideAttrs (_: { # Snuff out broken links
      doCheck        = false;
      doInstallCheck = false;
    }))
    macchanger      # Please don't change to mac
    neovim          # Vi Vi Vi, the editor of the beast
    openssl
    pandoc          # The truly universal document converter
    pciutils        # lspci
    pdftk           # The `ffmpeg' of pdf editing programs
    pinentry        # Ask for secrets like a cool hacker
    poppler_utils   # Imagine using AI for pdftotext
    qrencode        # QR Code generation
    redshift        # Keep your eyes from burning
    ripgrep         # A better `grep'
    ripgrep-all     # An even better `grep': search in pdf, jpg, mkv, mp4, etc.
    rlwrap          # Sane REPL behaviour for your broken programs!
    tealdeer        # Who has time to read through `man' pages anymore?
    tmux            # Basically only for SSHing into servers
    tokei           # Measure the most important metric of a project… or something
    unzip           # Can you imagine academic journals not accepting tar's? I don't have to.
    wezterm         # Configured in an actual programming language!
    wget
    yt-dlp

    # GUI applications
    anki-bin             # Pretending that I don't have a fish brain
    chromium             # Sigh
    dmenu                # Disimproved hmenu
    feh                  # That one image viewer that can also change the desktop bg
    gimp
    h.greenclip          # Clipboard management in Haskell!
    h.hmenu              # Improved dmenu
    h.vmensa             # Don't have to open a browser for this \o/
    h.xmobarrc
    inkscape
    mpv
    mupdf                # Okular keeps not wanting to let me sign PDFs :]
    okular               # zathura used to segfault for fun—backup for when that comes back :]
    scrot
    simplescreenrecorder # A screen recorder that—surprisingly—does not suck
    sioyek               # Many rough edges, but also generates TOCs for you so…
    sxiv                 # That *other* image viewer; can play GIFs
    xournalpp            # Draw abstract nonsense
    zathura              # View abstract nonsense

    # IM
    element-desktop
    signal-desktop
    tdesktop
  ];

  # Move some files.
  xdg.configFile."zathura/zathurarc".source       = mkSymlink "zathura/zathurarc";
  xdg.configFile."xmonad".source                  = mkSymlink "xmonad";
  xdg.configFile."profile/profile".source         = mkSymlink "profile/.profile";
  home.file.".scripts".source                     = mkSymlink "scripts";
  xdg.configFile."kmonad/config.kbd".source       = mkSymlink "kmonad/config.kbd";

  home.sessionVariables = {
    # The thing itself
    DOTFILES     = "${config.home.homeDirectory}/.dotfiles";
    # My local copy of nixpkgs
    NIXPKGS_REPO = "${config.home.homeDirectory}/repos/nixpkgs";
  };
}
