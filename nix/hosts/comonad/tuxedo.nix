{ pkgs, ... }:

{
  hardware = {
    tuxedo-drivers.enable = true;
    tuxedo-rs = {
      enable = true;
      tailor-gui.enable = true;
    };
  };

  networking.hostName = "comonad";

  # Bluetooth.
  hardware.bluetooth.enable = true;
  services.blueman.enable   = true;

  # Power saving shenanigans
  powerManagement.enable = true;
  services.tlp.enable    = true;

  services.logind.extraConfig = ''
    # Don’t shutdown when the power button is pressed
    HandlePowerKey=ignore
    # Systemd messes up and thinks that my laptop is docked when it's connected
    # to external displays because I turn off the internal laptop display.
    # Since I don't have a dock, this does the job.
    HandleLidSwitchDocked=suspend
  '';

  # Special ALSA settings.
  environment.etc."asound.conf".text = ''
    defaults.pcm.!card "Device"
    defaults.ctl.!card "Device"
  '';

  environment.systemPackages = with pkgs; [
    acpi
    alsa-utils
    brightnessctl
    tlp
  ];
}
