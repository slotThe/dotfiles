{ config, pkgs, ... }:

with config.lib.my; {
  home.packages = with pkgs; [
    autorandr        # Remember screen configurations
    mons             # Manage two monitors; mons -m is super great for quick mirroring
    unclutter-xfixes # Begone!
    xclip
    xdotool          # Can your Wayland do _this_‽
    xorg.xev
    xorg.xkill
    xorg.xmessage
  ];

  xdg.configFile."X11/xinitrc".source = mkSymlink "xinitrc/.xinitrc";
  home.file.".xscreensaver".source = mkSymlink "xscreensaver/.xscreensaver";
}
