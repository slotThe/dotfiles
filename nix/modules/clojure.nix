{ config, pkgs, ... }: {
  home = {
    packages = with pkgs; [
      babashka  # Scripts with parens!
      clojure   # (my (favourite (version (of (java [with brackets!])))))
      jdk       # I only use Clojure I swear!
      leiningen # lein help takes 2 seconds to load—and yet.
    ];
    sessionVariables = {
      LEIN_HOME = "${config.xdg.dataHome}/lein";
    };
  };
}
