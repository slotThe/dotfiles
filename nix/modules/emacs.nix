{ emacs-mps, config, pkgs, ... }:

{
  xdg.configFile."emacs".source = config.lib.my.mkSymlink "emacs";

  home.packages = with pkgs; [
    emacs-lsp-booster        # Convert JSON to bytecode—with Rust!
    ghostscript              # AUCTeX needs pdf2dsc
    gnutls
    hunspell                 # ispell, jinx
    hunspellDicts.de_DE
    hunspellDicts.en_GB-ise  # The correct ending
    noto-fonts
    noto-fonts-emoji         # Yup :]
    sqlite                   # org-roam
  ];

  programs.emacs = {
    enable  = true;
    package = pkgs.emacs-git.overrideAttrs(old: {
      src = emacs-mps;
      buildInputs = old.buildInputs ++ [ pkgs.mps ];
      configureFlags = old.configureFlags ++ [
        "--with-mps=yes"
        "--with-small-ja-dic"
        "--without-compress-install"
        "--without-toolkit-scroll-bars"
      ];
    });
    extraPackages = import ./emacs/packages.nix;
  };

  home.sessionVariables = {
    # Magit uses $SSH_ASKPASS to try and pop up a weird graphical
    # interface—don't.
    SSH_ASKPASS    = "";
    # Lol
    LSP_USE_PLISTS = "true";
  };

}
