# Note that these are _not_ all of the packages that I use; some are installed
# directly from their respective upstream repository (via use-package's :vc
# keyword) and are thus not featured here. This is a bit awkward, I admit, but
# I haven't found a better solution either.
epkgs: with epkgs; [
  # early-init.el
  auto-compile

  # init.el
  gcmh
  dash
  s
  f

  # hopf-theming.el
  fontaine
  modus-themes
  stimmung-themes
  rainbow-mode

  # hopf-keybindings.el
  mwim
  whole-line-or-region

  # hopf-completion.el
  vertico
  orderless
  marginalia
  consult
  wgrep
  embark
  embark-consult
  corfu
  cape

  # hopf-window-management
  popwin

  # hopf-dired.el
  diredfl
  dired-narrow

  # hopf-global-packages.el
  xclip
  olivetti
  expand-region
  yasnippet
  avy
  hl-todo
  magit
  forge
  magit-section
  magit-todos
  diff-hl
  project
  dictcc
  vundo
  multiple-cursors
  ledger-mode
  hide-mode-line
  beframe
  jinx
  rainbow-delimiters
  puni
  envrc
  gptel

  # hopf-email.el
  consult-notmuch
  gnus-alias

  # hopf-rss.el
  elfeed
  arxiv-citation

  # hopf-erc.el
  erc-hl-nicks

  # hopf-programming.el
  apheleia
  flycheck
  haskell-mode
  hindent
  rust-mode # rustic # Using psibi's fork for now
  cider
  flycheck-clj-kondo
  aggressive-indent
  tuareg
  dune
  markdown-mode
  (lsp-mode.overrideAttrs (_: { LSP_USE_PLISTS = true; }))
  # lsp-ui # FIXME: somehow this does not work with plists, even when the env var is present at compile and runtime?
  lsp-haskell
  lsp-pyright
  lsp-jedi
  paredit
  gnu-apl-mode
  bqn-mode

  # hopf-eshell.el
  eshell-toggle

  # hopf-latex-math.el
  latex-change-env
  cdlatex
  aas

  # hopf-latex.el
  auctex

  # hopf-org.el
  org-appear
  org-modern
  org-sticky-header
  # org-roam    # FIXME: With me using dev versions of org, I sometimes need to manually recompile org-roam.
  # org-roam-ui
  # anki-editor # FIXME: With me using dev versions of org, I sometimes need to manually recompile anki-editor.
  org-ref
  org-super-agenda
]
