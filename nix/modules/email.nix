{ config, pkgs, ... }:

with config.lib.my; {
  home.packages = with pkgs; [
    offlineimap      # Syncing
    notmuch          # Tagging
    notmuch.emacs    # Viewing
  ];

  # Make Emacs find the correct path, to ensure that notmuch.el's and
  # notmuch's version are synced.
  home.sessionVariables = {
    NOTMUCH_EMACS  = "${pkgs.notmuch.emacs}/share/emacs/site-lisp/";
    NOTMUCH_CONFIG = "${config.xdg.configHome}/notmuch-config";
  };

  xdg.configFile."offlineimap".source    = mkSymlink "offlineimap/";
  xdg.configFile."notmuch-config".source = mkSymlink "notmuch/notmuch-config";
  home.file.".mailcap".text = ''
    text/html;            firefox %s;
    application/pdf;      zathura %s;
    application/vnd.fdf;  zathura %s;
    image/*;              sxiv -a %s;
  '';

  # Fetch mails every 15 minutes, using a systemd timer instead of cron—if the
  # bloat is already there, why not use it to turn it into a feature!
  systemd.user = {
    services.fetch-and-sync-mail = {
      Unit.Description = "Fetch and sync mail";
      Service = {
        Type      = "oneshot";
        ExecStart = toString(pkgs.writeShellScript "fasm" ''
          set -eou pipefail
          export NOTMUCH_CONFIG="${config.xdg.configHome}/notmuch-config"
          export PASSWORD_STORE_DIR="${config.xdg.configHome}/password-store"
          PATH=$PATH:/run/current-system/sw/bin:/home/slot/.scripts:/etc/profiles/per-user/slot/bin
          ${pkgs.bash}/bin/bash -c ${config.home.homeDirectory}/.scripts/fetch-and-sync-mail.sh
        '');
      };
    };
    timers = {
      fetch-and-sync-mail = {
        Timer = {
          OnBootSec       = "1h";  # Take your time upon boot.
          OnUnitActiveSec = "15m";
        };
        Install.WantedBy = [ "timers.target" ];
      };
    };
  };

}
