# Trying to tame the beast… at least a little bit.
#
# Lots of inspiration from https://github.com/pjones/tilde
{ pkgs, ... }:

with pkgs.nur.repos.rycee.firefox-addons;
let
  extensions = [
    canvasblocker
    clearurls
    decentraleyes
    multi-account-containers
    old-reddit-redirect
    ublock-origin
    vimium
    # FIXME: tree-style-tab does not seem to work?
  ];

  settings = {
    # UI:
    "accessibility.typeaheadfind.autostart"          = false;
    "accessibility.typeaheadfind.flashBar"           = 0;
    "browser.bookmarks.showMobileBookmarks"          = false;
    "browser.contentblocking.category"               = "strict";
    "browser.display.use_system_colors"              = true;
    "browser.formfill.enable"                        = false;
    "browser.newtabpage.enabled"                     = false;
    "browser.startup.homepage"                       = "about:blank";
    "browser.sessionstore.collect_zoom"              = false;
    "browser.sessionstore.resume_from_crash"         = false;
    "browser.sessionstore.resume_session_once"       = false;
    "browser.sessionstore.resuming_after_os_restart" = false;
    "browser.sessionstore.interval"                  = "1800000";
    "browser.tabs.closeWindowWithLastTab"            = false;
    "browser.tabs.inTitlebar"                        = 0;
    "browser.urlbar.trimURLs"                        = false;
    "dom.forms.autocomplete.formautofill"            = false;
    "extensions.formautofill.addresses.enabled"      = false;
    "extensions.formautofill.creditCards.enabled"    = false;
    "extensions.pocket.enabled"                      = false;
    "media.gmp-widevinecdm.visible"                  = false;
    "signon.rememberSignons"                         = false;  # Don't use the built-in password manager.
    "widget.gtk.overlay-scrollbars.enabled"          = false;
    # Privacy:
    "privacy.donottrackheader.enabled"                       = true;
    "privacy.trackingprotection.enabled"                     = true;
    "privacy.trackingprotection.socialtracking.enabled"      = true;
    "network.trr.confirmation_telemetry_enabled"             = false;
    "browser.newtabpage.activity-stream.feeds.telemetry"     = false;
    "browser.newtabpage.activity-stream.telemetry"           = false;
    "browser.newtabpage.activity-stream.telemetry.ut.events" = false;
    "browser.ping-centre.telemetry"                          = false;
    "browser.urlbar.eventTelemetry.enabled"                  = false;
    "browser.safebrowsing.downloads.enabled"                 = false;
    "browser.safebrowsing.downloads.remote.url"              = "127.0.0.1";
    "browser.safebrowsing.malware.enabled"                   = false;
    "datareporting.healthreport.uploadEnabled"               = false;
    "app.shield.optoutstudies.enabled"                       = false;
    "dom.private-attribution.submission.enabled"             = false;
    "dom.security.https_only_mode"                           = true;
    "dom.security.https_only_mode_ever_enabled"              = true;
    # Force FF to use the user chrome CSS file:
    "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
    # Stop all of these nonsense suggestions in the urlbar.
    "browser.urlbar.suggest.searches"           = false;
    "browser.urlbar.shortcuts.bookmarks"        = false;
    "browser.urlbar.shortcuts.history"          = false;
    "browser.urlbar.shortcuts.tabs"             = false;
    "browser.urlbar.showSearchSuggestionsFirst" = false;
    "browser.urlbar.speculativeConnect.enabled" = false;
  };

  # See https://mrotherguy.github.io/firefox-csshacks/ for all of them.
  userChrome = ''
    @import url(${pkgs.firefox-csshacks}/chrome/window_control_placeholder_support.css);
    @import url(${pkgs.firefox-csshacks}/chrome/loading_indicator_bouncing_line.css);
    @import url(${pkgs.firefox-csshacks}/chrome/compact_proton.css);
    @import url(${pkgs.firefox-csshacks}/chrome/hide_tabs_toolbar.css);
    @import url(${pkgs.firefox-csshacks}/chrome/autohide_bookmarks_and_main_toolbars.css);
  '';

  # I mostly use X.A.Search, but sometimes these are convenient as well.
  search = {
    force = true;
    engines = {
      "Startpage" = {
        urls = [{ template = "https://www.startpage.com/sp/search?query={searchTerms}"; }];
        definedAliases = [ "s" ];
      };
      "DuckDuckGo" = {
        urls = [{ template = "https://duckduckgo.com/?hps=1&q={searchTerms}"; }];
        definedAliases = [ "d" ];
      };
      "YouTube" = {
        urls = [{ template = "https://www.youtube.com/results?search_type=search_videos&search_query={searchTerms}"; }];
        definedAliases = [ "y" ];
      };
    };
  };

in {
  programs.firefox = {
    enable = true;

    profiles.default = {
      inherit userChrome search;
      name = "default";
      id = 0;
      extensions.packages = [ umatrix ] ++ extensions;
      settings = settings // {
        "browser.privacy.trackingprotection.menu"          = "private";
        "pref.privacy.disable_button.cookie_exceptions"    = false;
        "browser.toolbars.bookmarks.showInPrivateBrowsing" = true;
        "browser.privatebrowsing.autostart"                = true;
        "privacy.clearOnShutdown.cache"                    = true;
        "privacy.clearOnShutdown.cookies"                  = true;
        "privacy.clearOnShutdown.downloads"                = true;
        "privacy.clearOnShutdown.formdata"                 = true;
        "privacy.clearOnShutdown.history"                  = true;
        "privacy.clearOnShutdown.offlineApps"              = true;
        "privacy.clearOnShutdown.sessions"                 = true;
        "privacy.clearOnShutdown.site Settings"             = true;
      };
    };

    profiles.persistent = {
      inherit settings userChrome search;
      name = "persistent";
      id = 1;
      extensions.packages = extensions;
      containersForce = true;
      containers = {
        "solid" = {
          id    = 1;
          color = "purple";
          icon  = "chill";
        };
        "personal" = {
          id    = 2;
          color = "yellow";
          icon  = "fingerprint";
        };
        "L" = {
          id    = 3;
          color = "orange";
          icon  = "fence";
        };
        "db" = {
          id    = 4;
          color = "red";
          icon  = "fence";
        };
        "ol" = {
          id    = 5;
          color = "green";
          icon  = "briefcase";
        };
        "ds" = {
          id    = 6;
          color = "blue";
          icon  = "dollar";
        };
      };
    };

    profiles.vanilla = {
      inherit settings;
      name = "vanilla";
      id = 2;
    };
  };
}
