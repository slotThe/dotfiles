{ config, pkgs, ... }:

with config.lib.my; {
  programs.fish = {
    enable = true;
    interactiveShellInit = ''
      set fish_greeting
      bind \ct transpose-chars # NixOS's fzf integration binds this without asking me.
    '';
  };
  xdg.configFile."fish/conf.d/custom.fish".source         = mkSymlink "fish/conf.d/custom.fish";
  xdg.configFile."fish/conf.d/private.fish".source        = mkSymlink "secrets/private.fish";
  xdg.configFile."fish/functions/fish_prompt.fish".source = mkSymlink "fish/functions/fish_prompt.fish";

  programs.bash = {
    enable = true;
    # One-off nix-shells still use bash; make the history large enough so as
    # to not delete anything on accident.
    historySize     = 100000;
    historyFileSize = 100000;
    # Bash is still my login shell: automatically start X when in tty 1.
    profileExtra = ''
      . "$XDG_CONFIG_HOME/profile/profile"
      if [[ !"$DISPLAY" && "$XDG_VTNR" = "1" ]]; then
        exec xinit "$XINITRC" -- "$(which X)" :0 vt1 -keeptty >& ~/.config/xsession.errors
      fi
    '';
    # Start fish if not already inside of fish.
    initExtra = ''
      if [[ $(${pkgs.procps}/bin/ps --no-header --pid=$PPID --format=comm) != "fish" && -z ''${BASH_EXECUTION_STRING} ]]
      then
        shopt -q login_shell && LOGIN_OPTION='--login' || LOGIN_OPTION=""
        exec ${pkgs.fish}/bin/fish $LOGIN_OPTION
      fi
    '';
  };
  home.sessionVariables = {
    HISTFILE="${config.xdg.stateHome}/bash/history";
  };
}
