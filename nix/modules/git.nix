{ pkgs, config, ... }:
{
  programs.git = {
    enable    = true;
    userName  = "Tony Zorman";
    userEmail = "mail@tony-zorman.com";
    extraConfig = {
      pull.rebase = true;
      rebase.autosquash = true;
      init.defaultBranch = "main";
      rerere.enabled = true;
      merge.conflictstyle = "zdiff3";
      diff.algorithm = "histogram";
      branch.sort = "-committerdate";
      credential.helper = "${pkgs.pass-git-helper}/bin/pass-git-helper";
      # scalar clone --full-clone --no-src
      scalar.repo = "${config.home.sessionVariables.NIXPKGS_REPO}";
      maintenance.repo = "${config.home.sessionVariables.NIXPKGS_REPO}";
      # Yup
      github.user = "slotThe";
      gitlab.user = "slotThe";
      url."git@github.com:".insteadOf = "https://github.com/";
      url."git@gitlab.com:".insteadOf = "https://gitlab.com/";
    };
    aliases = {
      authors = "!git log --pretty=format:%aN | sort | uniq -c | sort -n";
      lsi     = "ls-files --exclude-standard --ignored --others";
      su      = "submodule update --init --recursive";
      spull   = "!git stash && git pull && git stash pop";
    };
  };

  home.packages = with pkgs; [ pass-git-helper ];
  xdg.configFile."pass-git-helper/git-pass-mapping.ini".text = ''
    [git.overleaf.com*]
    target=overleaf/auth-token
  '';
}
