{ pkgs, ...}:
{
  programs.gpg.enable = true;
  services.gpg-agent  = {
    enable          = true;
    pinentryPackage = pkgs.pinentry-curses;
    defaultCacheTtl = 12 * 60 * 60;  # In seconds
    maxCacheTtl     = 24 * 60 * 60;
  };
  services.ssh-agent.enable = true;
}
