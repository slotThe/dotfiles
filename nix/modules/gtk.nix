{ config, pkgs, ... }:

{
  gtk = {
    enable = true;
    font = {
      name = "Alegreya";
      size = 13;
    };
    theme.name     = "Arc-Lighter";
    iconTheme.name = "Adwaita";
    gtk2.configLocation = "${config.xdg.configHome}/gtk-2.0/gtkrc";
  };

  home.packages = with pkgs; [
    dconf
    arc-theme
    adwaita-icon-theme
    lxappearance
  ];

}
