{ config, pkgs, ... }:

# tooMany *** operators ^? _impossible ^..! []
{
  home = {
    packages = with pkgs; [
      cabal-install
      # Version of GHC must fit with that of haskell-language-server below.
      (haskell.packages.ghc96.ghcWithPackages (p: with p; [
        # Make e.g. `ghci' come loaded up with some nice packages.
        lens
        unordered-containers
        comonad
        vector
        alex
      ]))
      haskell-language-server
      haskellPackages.fourmolu
    ];
    sessionVariables = {
      STACK_XDG = "1";
    };
  };

  xdg.configFile."ghc/ghci.conf".source = config.lib.my.mkSymlink "ghc/ghci.conf";
}
