{ config, ... }:

{
  config.lib.my = {
    # Create an out-of-store symlink to the given file or directory.
    #
    # mkSymlink : String → IO String (?)
    mkSymlink = path:
      config.lib.file.mkOutOfStoreSymlink
        ("${config.home.sessionVariables.DOTFILES}/" + path);
  };
}
