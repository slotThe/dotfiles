{
  programs.kitty = {
    enable = true;
    themeFile = "Catppuccin-Frappe";
    shellIntegration.enableFishIntegration = true;
    settings = {
      # Font settings
      font_family = "Hopf Mono";
      font_size   = 11;
      force_ltr   = true; # Force left-to-right: no BIDI
      # Cursor
      cursor_blink_interval = 0;
      # Mouse
      open_url_with         = "decide-link.sh";
      strip_trailing_spaces = "smart";
      # Reduce input lag
      repaint_delay = 8;
      input_delay = 0;
      sync_to_monitor = "no";
    };
  };
}
