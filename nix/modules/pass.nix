{ config, pkgs, ... }:

{
  # Pass—the only relevant password manager.
  programs.password-store = {
    enable   = true;
    package  = pkgs.pass.withExtensions (exts: [ exts.pass-otp ]);
    settings = {
      PASSWORD_STORE_DIR = "${config.xdg.configHome}/password-store";
    };
  };
}
