{ config, pkgs, ... }: {
  # Snek
  home = {
    packages = with pkgs; let p = python312Packages; in [
      (python312.withPackages (p: with p; [ numpy scipy matplotlib ]))
      p.ipython               # Interactive snek
      poetry                  # Project management with snek (and poetry2nix!)
      basedpyright            # Yuck!
      ruff                    # A linter/formatter written in a sane language
      ruff-lsp
    ];
    sessionVariables = {
      IPYTHONDIR = "${config.xdg.configHome}/ipython";
      JUPYTER_CONFIG_DIR = "${config.xdg.configHome}/jupyter";
      PYTHON_HISTORY="${config.xdg.configHome}/python/history";
    };
  };
}
