{ config, pkgs, ... }: {
  home = {
    packages = with pkgs; [
      cargo     # f::<<<T>>>().unwrap().unwrap().unwrap().unwrap()
      clang     # *Cries in GPL*
      mold      # A fast linker for my slow binaries
    ];

    sessionVariables = {
      CARGO_HOME = "${config.xdg.configHome}/cargo";
      RUFF_CACHE_DIR = "${config.xdg.configHome}/ruff";
    };
  };

  xdg.configFile."cargo/config.toml".text = ''
    [alias]
    rr = "run --release"

    [target.x86_64-unknown-linux-gnu]
    linker = "${pkgs.clang}/bin/clang"
    rustflags = ["-C", "link-arg=-fuse-ld=${pkgs.mold}/bin/ld.mold"]
  '';
}
