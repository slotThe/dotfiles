{ inputs, ... }:  # nix/nix.nix oh yeah!
{
  nix = {
    settings = {
      use-xdg-base-directories = true;
      experimental-features = [ "nix-command" "flakes" ]; # Use flakes
      flake-registry = "";                                # Disable global flake registry
      warn-dirty = false;                                 # Please don't spam me
      substituters = [ "https://nix-community.cachix.org" ];
      trusted-public-keys = [ "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=" ];
    };

    # Put the changed nixpkgs into the NIX_PATH, so that shells can
    # find and use them instead of the vanilla ones.
    nixPath = [ "nixpkgs=${inputs.nixpkgs.outPath}" ];

    # Add each flake input as a registry, to make nix3 commands
    # consistent with the flake.
    registry = inputs.nixpkgs.lib.mapAttrs (_: value: { flake = value; }) inputs;
  };

  # https://github.com/NixOS/nixpkgs/pull/308801
  system.switch = {
    enable   = false;
    enableNg = true;
  };
}
