{ inputs }:
self: super:

let overlays = [
      (import ./haskell.nix)
      (final: prev: {
        firefox-csshacks = prev.callPackage ../packages/firefox-csshacks.nix { inherit inputs; };
        hopf-mono        = prev.callPackage ../packages/hopf-mono.nix { };
        fixeder-sys      = prev.callPackage ../packages/fixeder-sys.nix { };

        # Newer version for BQN and APL support, as well as a functional JSON
        # output. Can't just use overrideAttrs because of
        # https://github.com/NixOS/nixpkgs/issues/107070.
        tokei = prev.rustPlatform.buildRustPackage rec {
          pname = "tokei";
          version = "13.0alpha8";
          useFetchCargoVendor = true;
          cargoHash = "sha256-LzlyrKaRjUo6JnVLQnHidtI4OWa+GrhAc4D8RkL+nmQ=";
          src = prev.fetchFromGitHub {
            owner = "XAMPPRocky";
            repo = "tokei";
            rev = "7f258f473ced89855e92fb6eecb2e03c7d4b6896";
            hash = "sha256-f2YVaXjo11ghVNckJAk3QmU7Nvp4KkB2cSzYboQXBt4=";
          };
          buildFeatures = [ "all" ];
        };
      })
    ];
in super.lib.composeManyExtensions overlays self super
