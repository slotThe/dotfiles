final: prev: {
  haskellPackages = prev.haskellPackages.override (old: {
    overrides = prev.lib.composeExtensions (old.overrides or (_: _: { }))
      (self: super: {
        # Hmenu is like dmenu, but better! (and a wrapper)
        hmenu = self.callCabal2nix "hmenu" (builtins.fetchGit {
          url = "https://github.com/slotThe/hmenu";
          rev = "2877e58d0754acae015d28aae1ed2c94508969c5";
        }) { };

        # vmensa: Query the Stundentenwerk API from inside your terminal!
        cmdline-util = self.callCabal2nix "cmdline-util" (builtins.fetchGit {
          url = "https://github.com/slotThe/cmdline-util";
          rev = "c5d6e3832b38769be649b8dae2dd8f4659ead577";
        }) { };
        vmensa = self.callCabal2nix "vmensa" (builtins.fetchGit {
          url = "https://github.com/slotThe/vmensa";
          rev = "b6f6e54d4a1d6256e3ffa76d476bb15905faeb68";
        }) { };

        # Haskell-based xmobar configuration.
        xmobarrc = self.callCabal2nix "xmobar" ../../xmobar { };

        # Quickly generate matrices on the command line
        matrix-maker = self.callCabal2nix "matrix-converter" (builtins.fetchGit {
          url = "https://github.com/slotThe/matrix-converter";
          rev = "21a6efd650f56cec0cad7d0d84d02936806f79b5";
        }) { };

        # Look up a word on duden.de
        duden = self.callCabal2nix "duden" (builtins.fetchGit {
          url = "https://github.com/slotThe/duden";
          rev = "f1e1cf9e0c1bce9f565d8f856786581530d03ad1";
        }) { };
      });
  });
}
