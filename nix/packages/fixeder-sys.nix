{ stdenvNoCC, fetchurl }:

let
  srcs = {
    FixederSys1x = fetchurl {
      url = "http://tom7.org/fixedersys/FixederSys1x.ttf";
      hash = "sha256-TQugVcRfCz4w+oRasVIIeri3SZDt70SFdyF+ERw3KfU=";
    };
    FixederSys2x = fetchurl {
      url = "http://tom7.org/fixedersys/FixederSys2x.ttf";
      hash = "sha256-7v5dA7h+ZaxVOtwQc0/Gp4xw681hl6lrAwIM+p6ikY4=";
    };
    FixederSysLight2x = fetchurl {
      url = "http://tom7.org/fixedersys/FixederSysLight2x.ttf";
      hash = "sha256-yw4fVyLUwTp54/RVawSFUw7py1tir/SvNrfP3BtMzlY=";
    };
  };
in
stdenvNoCC.mkDerivation rec {
  pname = "fixeder-sys";
  version = "1.00";

  dontUnpack = true;

  installPhase = ''
    runHook preInstall

    install -m444 -D "${srcs.FixederSys1x}" "$out/share/fonts/truetype/${srcs.FixederSys1x.name}-${version}.ttf"
    install -m444 -D "${srcs.FixederSys2x}" "$out/share/fonts/truetype/${srcs.FixederSys2x.name}-${version}.ttf"
    install -m444 -D "${srcs.FixederSysLight2x}" "$out/share/fonts/truetype/${srcs.FixederSysLight2x.name}-${version}.ttf"

    runHook postInstall
  '';

}
