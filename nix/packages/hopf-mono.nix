{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation {
  pname   = "hopf-mono"; # Iosevka custom build
  version = "0.1";

  src = fetchFromGitHub {
    owner = "slotThe";
    repo  = "hopf-mono";
    rev   = "a38daa5137f0ac05b5683c73aec47c9ab4661c3e";
    hash  = "sha256-g7OtYj1Wjdoz5PRxNqhgX3epP2gg+gPJGu2TIF4vEYA=";
  };

  installPhase = ''
    install -m644 --target $out/share/fonts/truetype -D $src/ttf/*.ttf
  '';
}
