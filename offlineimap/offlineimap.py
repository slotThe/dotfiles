#! /usr/bin/env python
import subprocess


def run_pass(s: str) -> str:
    return subprocess.run(
        "pass " + s,
        shell=True,
        capture_output=True,
        text=True,
    ).stdout.strip("\n")


def pass_domain() -> str:
    return run_pass("hetzner/mailbox/mail@tony-zorman.com")


def pass_uni() -> str:
    return run_pass("uni/zih")


def pass_mailbox() -> str:
    return run_pass("email/mailbox")


def pass_mailbox_slot() -> str:
    return run_pass("email/mailbox-slot")
