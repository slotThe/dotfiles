#!/bin/sh

### This file is run on login.

# Force certain paths into $XDG_CONFIG_HOME
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0"
export LEDGER_FILE="$XDG_CONFIG_HOME/hledger/journal.ledger"
export HISTFILE="$XDG_STATE_HOME"/bash/history
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export BUNDLE_USER_CONFIG="$XDG_CONFIG_HOME"/bundle
export BUNDLE_USER_CACHE="$XDG_CACHE_HOME"/bundle
export BUNDLE_USER_PLUGIN="$XDG_DATA_HOME"/bundle
export RLWRAP_HOME="$XDG_DATA_HOME"/rlwrap

# Default applications.
export EDITOR="emacsclient -a \"\" -c"
export BROWSER="firefox"
export TERMINAL="kitty"

# Use dmenu as a password handler.
export SUDO_ASKPASS="$HOME/.scripts/dmenupassword"

# Add things to the path.
export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin:$HOME/.local/bin:/opt/texlive/2023/bin/x86_64-linuxmusl:$HOME/.cabal/bin:$HOME/.scripts:$CARGO_HOME/bin

# Colours for less/man.
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"; a="${a%_}"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"; a="${a%_}"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"; a="${a%_}"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"; a="${a%_}"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"; a="${a%_}"
