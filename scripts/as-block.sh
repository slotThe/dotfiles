#!/bin/sh

ACTION="DROP"
FACEBOOK_AS="AS32934"

  # create chains
  #iptables -N TCP
  #iptables -N UDP

  #flush (clear) the tables and clear the counters
  iptables -F
  iptables -Z
  iptables -X TCP
  iptables -X UDP
  #ip6tables -F
  #ip6tables -Z

  iptables -N TCP
  iptables -N UDP
  iptables -P FORWARD DROP
  iptables -P OUTPUT ACCEPT
  iptables -P INPUT DROP
  iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
  iptables -A INPUT -i lo -j ACCEPT
  iptables -A INPUT -m conntrack --ctstate INVALID -j DROP
  iptables -A INPUT -p icmp -m icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT
  iptables -A INPUT -p udp -m conntrack --ctstate NEW -j UDP
  iptables -A INPUT -p tcp --tcp-flags FIN,SYN,RST,ACK SYN -m conntrack --ctstate NEW -j TCP
  iptables -A INPUT -p udp -j REJECT --reject-with icmp-port-unreachable
  iptables -A INPUT -p tcp -j REJECT --reject-with tcp-reset
  iptables -A INPUT -j REJECT --reject-with icmp-proto-unreachable
  iptables -t raw -I PREROUTING -m rpfilter --invert -j DROP


  for AS in ${FACEBOOK_AS}
    do
      IPs=`whois -h whois.radb.net \!g${AS} | grep /`
      for IP in ${IPs}
        do
          for TARGET in INPUT OUTPUT FORWARD
            do
              iptables  -A ${TARGET} -p all -d ${IP} -j ${ACTION}
          done
      done

  done

  iptables-save > /etc/iptables/iptables.rules
