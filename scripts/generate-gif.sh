#!/bin/sh

ffmpeg -ss "$2" -t "$3" -i "$1" -filter_complex "[0:v] fps=24,scale=w=1280:h=-1,split [a][b];[a] palettegen [p];[b][p] paletteuse" out.gif
gifsicle -O3 -b out.gif
