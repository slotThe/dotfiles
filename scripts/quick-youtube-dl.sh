#!/bin/sh

xclip -o | xargs yt-dlp --output '%(uploader)s - %(title)s.%(ext)s' \
                        --add-metadata                              \
                        --no-mark-watched                           \
                        --force-ipv4                                \
                        --ignore-errors                             \
                        --embed-chapters                            \
                        &
