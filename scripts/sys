#!/usr/bin/env python

import sys
import subprocess
import os
from dataclasses import dataclass


@dataclass
class FastBuild:
    pass


BuildType = FastBuild | None


dots = os.path.expanduser("~/.dotfiles")


# `args' has type list[str], but since it's a varfun the type is just that of
# a single argument.
def run(*args: str) -> None:
    subprocess.run([a for xs in args for a in xs.split()])


def run_to_string(*args: str) -> str:
    return subprocess.run(
        [a for xs in args for a in xs.split()], capture_output=True, text=True
    ).stdout.strip()


def flupdate() -> None:
    os.chdir(dots)
    run("nix flake update")


def nomify(cmd: str) -> None:
    subprocess.call(
        f"{cmd} --log-format internal-json -v 2>&1 | nom --json",
        shell=True,
    )


def rebuild(buildType: BuildType = None) -> None:
    nomify(
        f"sudo nixos-rebuild switch --flake {dots} \
        {'--fast' if buildType == FastBuild else ''}"
    )


av = sys.argv
match av[1]:
    case "switch":
        rebuild(FastBuild())
    case "lbuild":
        nomify(f"nix-build ./ -A {av[2]}")
    case "rebuild":
        rebuild()
    case "update":
        flupdate()
        rebuild()
    case "flupdate":
        flupdate()
    case "search":
        run("nix search nixpkgs", *av[2:])
    case "locate":
        run("nix-locate", *av[2:])
    case "gc":
        run("sudo nix-collect-garbage -d")
        run("nix-collect-garbage -d")
        run("sudo nix-store --optimise")
        run("nix-store --optimise")
        rebuild()
    case "hash":
        hash = run_to_string("nix-hash --type sha256 --base64", *av[2:])
        print(f'"sha256-{hash}"')
    case arg:
        print(f"\033[91mERROR:\x1b[0m unknown argument: {arg}")
