#!/usr/bin/env bb

(ns wacom
  (:require [clojure.java.shell :refer [sh]]
            [clojure.string :as str]))

(def wacom-width  21600)
(def wacom-height 13500)

(def dims (->> (re-find #"dimensions:\s+(\d+)x(\d+)"
                        (:out (sh "xdpyinfo")))
               (drop 1)
               (map read-string)
               (zipmap [:width :height])))

(defn wacom-set [& args]
  (apply sh "xsetwacom" "set" "Wacom Intuos BT M Pen stylus" (map str args)))

(def setups
  {"Work monitors" (do (wacom-set "MapToOutput" "VGA1")
                       (wacom-set "Area" "0" "0" wacom-width (/ (* wacom-width (:height dims)) (:width dims))))
   "Home monitor" (wacom-set "Area" "0" "0" wacom-width "12994")})

(->> (sh "dmenu.sh" "-l" "5" :in (str/join "\n" (keys setups)))
     :out
     str/trim
     setups)
