{-
   __   _____  ___                      _
   \ \ / /|  \/  |                     | |
    \ V / | .  . | ___  _ __   __ _  __| |
    /   \ | |\/| |/ _ \| '_ \ / _` |/ _` |
   / /^\ \| |  | | (_) | | | | (_| | (_| |
   \/   \/\_|  |_/\___/|_| |_|\__,_|\__,_|

Available at: https://gitlab.com/slotThe/dotfiles

First Edit: 28jun2019 (I think?)

GLHF
-}

-------------------------------------------------------------------------
-- PRAGMAS
-------------------------------------------------------------------------

-- Damn you xmonad and your crazy type signatures!
{-# OPTIONS_GHC -Wno-missing-signatures #-}
{-# OPTIONS_GHC -Wno-orphans            #-}

-- Cool language extensions \o/
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE GHC2021        #-}
{-# LANGUAGE LambdaCase     #-}
{-# LANGUAGE MultiWayIf     #-}

-------------------------------------------------------------------------
-- IMPORTS
-------------------------------------------------------------------------

module Main (main) where

import Data.Map.Strict (Map)
import Data.Ratio ((%))

import XMonad.StackSet (RationalRect (RationalRect))
import XMonad.StackSet qualified as W

import XMonad
import XMonad.Prelude

import XMonad.Actions.CycleWindows (rotUnfocusedDown, rotUnfocusedUp)
import XMonad.Actions.DwmPromote (dwmpromote)
import XMonad.Actions.EasyMotion (ChordKeys (AnyKeys), EasyMotionConfig (borderPx, emFont, sKeys, cancelKey), selectWindow)
import XMonad.Actions.Prefix (PrefixArgument (Raw), usePrefixArgument, withPrefixArgument, orIfPrefixed)
import XMonad.Actions.Search (Browser, SearchEngine (SearchEngine), arXiv, clojureDocs, cratesIo, github, hoogle, noogle, openstreetmap, promptSearchBrowser', rustStd, searchEngine, searchEngineF, selectSearchBrowser, wikipedia, youtube, zbmath, nixos)
import XMonad.Actions.Submap (submap)
import XMonad.Actions.SwapPromote (masterHistoryHook, swapHybrid)
import XMonad.Actions.TopicSpace (Topic, TopicConfig (defaultTopic, defaultTopicAction, topicActions, topicDirs), TopicItem (TI), currentTopicAction, currentTopicDir, inHome, noAction, shiftNthLastFocused, switchNthLastFocusedByScreen, switchTopic, tiActions, tiDirs, topicNames, workspaceHistoryHookExclude)
import XMonad.Actions.UpKeys (upKeys, useUpKeys)
import XMonad.Actions.UpdatePointer (updatePointer)

import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Hooks.ManageDocks (SetStruts (SetStruts), ToggleStruts (ToggleStruts), manageDocks,checkDock)
import XMonad.Hooks.ManageHelpers (isDialog)
import XMonad.Hooks.RefocusLast (refocusLastLogHook, toggleFocus)
import XMonad.Hooks.Rescreen (RescreenConfig (afterRescreenHook), rescreenHook)
import XMonad.Hooks.StatusBar (StatusBarConfig, dynamicSBs, statusBarPropTo)
import XMonad.Hooks.StatusBar.PP (PP (ppCurrent, ppExtras, ppHidden, ppOrder, ppSep, ppUrgent, ppVisible), filterOutWsPP, shorten, wrap, xmobarBorder, xmobarColor, xmobarRaw, xmobarStrip)
import XMonad.Hooks.UrgencyHook (NoUrgencyHook (NoUrgencyHook), withUrgencyHook)

import XMonad.Layout.LayoutModifier (ModifiedLayout)
import XMonad.Layout.LimitWindows (limitWindows)
import XMonad.Layout.Magnifier (MagnifyThis (NoMaster), magnify)
import XMonad.Layout.NoBorders (Ambiguity (Combine, OnlyFloat, Screen), With (Union), lessBorders)
import XMonad.Layout.Reflect (reflectHoriz)
import XMonad.Layout.Renamed (Rename (Replace), renamed)
import XMonad.Layout.ResizableTile (MirrorResize (MirrorExpand, MirrorShrink), ResizableTall (ResizableTall))
import XMonad.Layout.ThreeColumns (ThreeCol (ThreeColMid))
import XMonad.Layout.TwoPane (TwoPane (TwoPane))

import XMonad.Prompt (Direction1D (Prev), XP, XPConfig (alwaysHighlight, autoComplete, bgColor, bgHLight, completionKey, fgColor, fgHLight, font, height, historyFilter, historySize, maxComplRows, position, prevCompletionKey, promptBorderWidth, promptKeymap, searchPredicate, sorter), XPPosition (Top), deleteAllDuplicates, emacsLikeXPKeymap, killWord', moveHistory, XPrompt(..), mkXPrompt, ComplFunction)
import XMonad.Prompt.FuzzyMatch (fuzzyMatch, fuzzySort)
import XMonad.Prompt.Man (manPrompt)
import XMonad.Prompt.OrgMode (orgPrompt, orgPromptPrimary)
import XMonad.Prompt.Pass (passPrompt, passTypePrompt, passOTPTypePrompt)
import XMonad.Prompt.Window (WindowPrompt (Bring, Goto), allWindows, windowPrompt)
import XMonad.Prompt.Workspace (workspacePrompt)

import XMonad.Util.Cursor (setDefaultCursor)
import XMonad.Util.EZConfig (additionalKeys, additionalKeysP)
import XMonad.Util.SpawnOnce (spawnOnce)
import XMonad.Util.Loggers (logTitlesOnScreen)
import XMonad.Util.NamedScratchpad (NamedScratchpad (NS), customFloating, namedScratchpadAction, namedScratchpadManageHook, scratchpadWorkspaceTag)
import XMonad.Util.Run (EmacsLib (ElpaLib, Special), Input, asBatch, asString, elispFun, eval, execute, executeNoQuote, findFile, getInput, inEditor, inEmacs, inProgram, inTerm, proc, progn, quote, require, safeSpawn, setFrameName, setXClass, spawnExternalProcess, termInDir, toInput, withEmacsLibs, (>&&>), (>-$), (>->), runProcessWithInput)
import XMonad.Util.XSelection (getSelection)

-------------------------------------------------------------------------
-- MAIN
-------------------------------------------------------------------------

main :: IO ()
main = xmonad
     . ewmh
     . withUrgencyHook NoUrgencyHook  -- no popups, only bar notifications
     . dynamicSBs (pure . barSpawner)
     . usePrefixArgument "M-u"
     . rescreenHook def{ afterRescreenHook = onMonitorChange }
     . spawnExternalProcess def
     . useUpKeys def{ upKeys = fromList $ dockKeys (onAllDocks lowerWindow) }
     $ myConfig `additionalKeysP` myKeys
                `additionalKeys`  dockKeys (onAllDocks raiseWindow)
 where
  barSpawner :: ScreenId -> StatusBarConfig
  barSpawner = \case
    0 -> statusBarPropTo "_XMONAD_LOG_0" "xmobar -x 0" (xmobarPP 0)
    1 -> statusBarPropTo "_XMONAD_LOG_1" "xmobar -x 1" (xmobarPP 1)
    _ -> mempty

  onMonitorChange :: X ()
  onMonitorChange = do
    spawn "timeout 3 wallpaper-changer"         -- make wallpaper pretty

-- | Putting most of the config together.
myConfig = def
  { modMask            = mod4Mask    -- Super; don't steal my Meta key, Emacs needs that!
  , borderWidth        = 0           -- in pixels
  , normalBorderColor  = colorBg
  , focusedBorderColor = colorBlue
  , terminal           = "wezterm"
  , startupHook        = setDefaultCursor xC_left_ptr
                      >> spawnOnce "emacs --daemon"        -- See Note [Emacs]
                      >> spawnOnce "emacs --daemon=eshell"
  , workspaces         = topicNames topics
  , manageHook         = myManageHook
  , layoutHook         = lessBorders (Combine Union Screen OnlyFloat) layoutOrder
  , logHook            = refocusLastLogHook
                      <> workspaceHistoryHookExclude [scratchpadWorkspaceTag]
                      <> masterHistoryHook      -- Remember where we've been²
                      <> updatePointer (0.5, 0.5) (0, 0)
                         -- When focusing a new window with the keyboard,
                         -- move pointer to exact center of that window.
  }

{- Note [Emacs]

All of the Emacs daemons are spawned here, and *not* as systemd services
or in the ~/.xinitrc. The reason is that, in this way, Emacs inherits
all environment variables that are set by the XMonad executable, like
$XMONAD_GHC. See the bottom of this file for a use-case.
-}

-- | Building my own pretty-printer.
xmobarPP :: ScreenId -> X PP
xmobarPP sid = pure . filterOutWsPP [scratchpadWorkspaceTag] $ def
  { ppSep     = magenta " • "
  , ppCurrent = (" " <>) . xmobarBorder "Top" colorCyan 2 . white
  , ppVisible = (" " <>) . wrap (blue "[") (blue "]") . white
  , ppHidden  = white . (" " <>)
  , ppUrgent  = red   . wrap (yellow "!") (yellow "!")
  , ppOrder   = \[ws, l, _, wins] -> [ws, l, wins]
  , ppExtras  = [logTitlesOnScreen sid formatFocused formatUnfocused]
  }
 where
  formatFocused, formatUnfocused :: String -> String
  formatFocused   = wrap (text     "[") (text     "]") . magenta . ppWindow
  formatUnfocused = wrap (lowWhite "[") (lowWhite "]") . blue    . ppWindow

  -- | Windows should have *some* title not exceeding a sane length.
  ppWindow :: String -> String
  ppWindow = xmobarRaw
           . (\w -> if null w then "untitled" else w)
           . shorten 30
           . xmobarStrip

{--------------------------------COLOURS---------------------------------

These are mostly inspired by the `catppuccin' colour scheme:

             https://github.com/catppuccin/catppuccin

I will grudgingly use "color" in function names so as to be consistent
with the names XMonad uses.
------------------------------------------------------------------------}

-- | Base colours to be used.
colorBg       :: String = "#1e1e2e"
colorBlue     :: String = "#b4befe"
colorCyan     :: String = "#89dceb"
colorFg       :: String = "#f8f8f2"
colorLowWhite :: String = "#bbbbbb"
colorMagenta  :: String = "#eba0ac"
colorRed      :: String = "#f38ba8"
colorText     :: String = "#cdd6f4"
colorYellow   :: String = "#f9e2af"

-- | Set the status bar colours based the ones defined above.
-- @ xmobarColor "foreground colour" "background colour" @
blue, lowWhite, magenta, red, text, white, yellow :: String -> String
blue     = xmobarColor colorBlue     ""
lowWhite = xmobarColor colorLowWhite ""
magenta  = xmobarColor colorMagenta  ""
red      = xmobarColor colorRed      ""
text     = xmobarColor colorText     ""
white    = xmobarColor colorFg       ""
yellow   = xmobarColor colorYellow   ""

{-------------------------------TOPICS-----------------------------------

Topics are like workspaces, but on crack!  They can be used to e.g. set
startup hooks for when a topic is completely empty.  For keybindings,
see the 'topicKeys' function further down.

If you want icons here use escape codes, e.g. "6:\xf012".
------------------------------------------------------------------------}

-- | Actually putting the topic config together.
topicConfig :: TopicConfig
topicConfig = def
  { topicDirs          = tiDirs    topics
  , topicActions       = tiActions topics
  , defaultTopicAction = const (pure ())
  , defaultTopic       = tHSK
  }

-- | The name of these is needed in another place as well.
tHSK :: Topic = "<fn=1>\xf120</fn>"
tVID :: Topic = "5:MPV"

-- | My topics.  Directory names are relative to the home directory.
topics :: [TopicItem]
topics =
  [ -- The first 10 topics will be the ones that are available via
    -- explicit keybindings (`123456789).
    noAction tHSK "repos"
  , inHome "1:WEB" $ spawn browser
  , TI       "2:UNI" "uni" spawnEshellInTopic
  , noAction "3:UNI" "uni"
  , only "4"
  , noAction tVID "youtube-videos"
  , only "6:WEB"
  , inHome "7:RSS" $ proc $ inEditor >-> setFrameName "elfeed" >-> eval (elispFun "elfeed")
  , inHome "8:IM"  $ spawn "telegram-desktop" *> spawn "signal-desktop"
  , inHome "9:IRC" $ proc $ inEmacs >-> eval (elispFun "slot/erc") -- oh yeah

  , TI "website" "repos/slotThe.github.io/" spawnWebsite
  , TI "anki"    "repos/org"                (spawnEditorInTopic *> spawn "anki")
  , TI "vpn"     "openvpn" $ proc $ inProgram "xterm" >-> execute "randomVPN.sh"
  ] <>
  map ($ spawnEditorInTopic)
  [ -- Abstract nonsense
    TI "duoidal"    "uni/papers/duoidal-monads"
  , TI "diss"       "uni/dissertation"
  , TI "comodkmat"  "uni/papers/chelsea-sebastian--k-matrices-for-comodule-monads"
  , TI "stringdiag" "uni/papers/anna-matti-zibbig--free-module-catys"
  , TI "gabimonads" "uni/papers/sebastian-paolo--gabi-monads"
  , TI "tambara"    "uni/papers/matti--tambara"
    -- Programming
  , TI "aoc"     "repos/advent-of-code"
  , TI "vmensa"  "repos/haskell/vm"
  , TI "hmenu"   "repos/haskell/hmenu"
  , TI "duden"   "repos/haskell/duden"
  , TI "xmobar"  "repos/xmobar"
  , TI "xmonad"  "repos/xmonad/xmonad"
  , TI "xm-con"  "repos/xmonad/xmonad-contrib"
  , TI "kmonad"  "repos/kmonad"
  , TI "botirc"  "repos/haskell/irc-bot"
  , TI "lamyrem" "repos/RemarkableLamyEraser/"
  , TI "emacs"   "repos/emacs"
  , TI "rq"      "repos/rq"
  ]
 where
  -- | Basically a normal workspace.
  only :: Topic -> TopicItem
  only n = noAction n "./"

  spawnWebsite :: X ()
  spawnWebsite = switchToLayout "Tall"
              *> spawnEditorInTopic
              *> executeInTopic "nix develop -c make watch"
              *> spawn "browser-new-window.sh localhost:8000"

-- | Go to a topic, shift a window to it, or do both at the same time.
goto :: Topic -> X ()
goto topic = withPrefixArgument \case
  Raw 1 -> shiftWin
  Raw _ -> shiftWin *> gotoWs
  _     -> gotoWs
 where
  gotoWs   = switchTopic topicConfig topic
  shiftWin = windows (W.shift topic)

-- | Prompt version of 'goto' for topics that are not available via
-- direct keybindings.
promptedGoto :: X ()
promptedGoto = workspacePrompt topicPrompt goto

-- | Modify our standard prompt a bit.
topicPrompt :: XPConfig
topicPrompt = prompt
  { autoComplete = Just 3000  -- Time is in μs.
  , historySize  = 0          -- No history in the prompt.
  }

-- | Spawn terminal in topic directory.
spawnTermInTopic :: X ()
spawnTermInTopic = proc $ wezInDir >-$ currentTopicDir topicConfig

wezInDir :: X Input
wezInDir = inTerm >-> pure (" start --cwd " <>)

-- | Execute a program in the topic directory (inside a terminal).
executeInTopic :: String -> X ()
executeInTopic p = proc $ (wezInDir >-$ currentTopicDir topicConfig) >-> executeNoQuote p

-- | Spawn editor in the current topic directory.
spawnEditorInTopic :: X ()
spawnEditorInTopic = proc $ inEditor >-$ currentTopicDir topicConfig

-- | Spawn an eshell frame in the current topic directory.
spawnEshellInTopic :: X ()
spawnEshellInTopic = currentTopicDir topicConfig >>= \dir ->
  proc $ inProgram "emacsclient -a '' -c -s eshell"
     >-> eval (progn [ "eshell" <> quote "new-shell"
                     , "eshell/cd" <> asString dir
                     , "eshell/clear-scrollback"
                     , "eshell-send-input"
                     , "set-frame-font" <> asString "FixederSys 1x-12:antialias=none"
                     ])

-- | Toggle between the current and the last topic.
toggleTopic :: X ()
toggleTopic = switchNthLastFocusedByScreen topicConfig 1

-- | Shift the currently focused window to the last visited topic.
shiftToLastTopic :: X ()
shiftToLastTopic = shiftNthLastFocused 1

{--------------------------------LAYOUT---------------------------------}

{- | Layout order; see Note [layouts] for further details.

NOTE: The first layout in this order will be the default layout on every
      workspace.
-}
layoutOrder =  Full
           ||| tall
           ||| Mirror tall
           ||| TwoPane (3 % 100) (1 % 2)
           ||| threeColMid
           ||| hacking
 where
  tall :: ModifiedLayout Rename ResizableTall a
  tall = setName "Tall" $ rTall 1 (3 % 100) (1 % 2)

  threeColMid
    = setName "ThreeCol"
    . reflectHoriz
    . magnify 1.2 (NoMaster 4) True
    $ ThreeColMid 1 (3 % 100) (11 % 30)

  hacking
    = setName "Hacking"
    . limitWindows 3
    . magnify 1.3 (NoMaster 3) True
    $ rTall 1 (3 % 100) (13 % 25)

  setName :: String -> l a -> ModifiedLayout Rename l a
  setName n = renamed [Replace n]

  rTall :: Int -> Rational -> Rational -> ResizableTall l
  rTall m r c = ResizableTall m r c []

{- Note [layouts]
   ~~~~~~~~~~~~~~~~~~~~~~
                       ~~ Individual functions ~~

* 'renamed' used on conjunction with the 'Replace' constructor simply
  renames the layout to something more convenient.

* 'reflectHoriz' reflects the layout horizontally (left <--> right).

                             ~~ Layouts ~~

* 'ResizableTall' is much like 'Tall', in that it shows all windows in a
  horizontal split.  Where it differs is that it also allows resizing
  the stack windows via some keybindings.  It takes the following
  arguments:

    - Number of initial master windows
    - Percent of screen to increment while resizing
    - Default proportions of master/stack ratio
    - Fraction to multiply the window height that would be given when
      divided equally, from top to bottom

* 'TwoPane' only shows two windows in a horizontal split, stack windows
  get stacked (hah) on top of each other.

* 'ThreeColMid' shows three columns in a horizontal split, with the
  master window being in the middle.  Arguments are the same as 'Tall'.
  There is a magnifier in there, enlarging stack windows when they're
  focused, to make using these a little more realistic.

* The 'Hacking' layout is just a small wrapper around 'Tall', creating a
  master pane that's exactly 82 columns wide (on my 12.5", 1366x768
  laptop monitor; for the font see my Emacs config) and a stack pane
  that gets enlarged (via 'magnify') when it's focused and there are
  more than two windows on the workspace overall (that's such a cool
  thing btw).  Its very suggestive name is probably enough information
  about its use case.

  I decided against using some 'FixedColumn' layout here, as (X11) Emacs
  spends most time in the master spot and 'FixedColumn' really only
  plays nicely with terminal windows.
-}

{------------------------------SCRATCHPADS-------------------------------

Scratchpads are basically floating windows that I can hide and show as I
please.  This is very convenient for something like a music client or a
calculator.  Also see Note [ICCCM].

The layout of the 'NamedScratchpad' type constructor is as follows:
  @
    data NamedScratchpad =
        NS { name  :: String     -- ^ Scratchpad name.
           , cmd   :: String     -- ^ Command used to run application.
           , query :: Query Bool -- ^ Query to find already running application.
           , hook  :: ManageHook -- ^ Window placement.
           }
  @
------------------------------------------------------------------------}

myScratchpads :: X [NamedScratchpad]
myScratchpads = do
  orgAgenda   <- getInput $
    inEditor >-> setFrameName agendaInstName
             >-> eval (progn [ "org-agenda" <> " nil " <> asString "n"
                             , "delete-other-windows"
                             ])
  ledger      <- getInput $ inEditor >-> setFrameName ledgerInstName
                                     >-> eval (findFile "~/.config/hledger/journal.ledger")
  mailSession <- getInput $ inEditor >-> setFrameName mailInstName >-> eval (elispFun "notmuch")
  viewTodos   <- getInput $ inEditor >-> setFrameName todoInstName >-$ pure "~/repos/org/todos.org"
  floatTerm   <- getInput $ inTerm   >-> wezXClass termInstName
  emacsAny    <- getInput $ inEditor >-> setFrameName eaInstName >-> eval (elispFun "emacs-anywhere")

  pure [ NS "Mail"    mailSession (appName =? mailInstName  ) floatQuakeish
       , NS "Todos"   viewTodos   (appName =? todoInstName  ) floatQuakeish
       , NS "Term"    floatTerm   (appName =? termInstName  ) floatQuakeish
       , NS "Agenda"  orgAgenda   (appName =? agendaInstName) floatQuakeish
       , NS "EA"      emacsAny    (appName =? eaInstName    ) floatMid
       , NS "Ledger"  ledger      (appName =? ledgerInstName) floatMid
       ]
 where
  floatQuakeish :: ManageHook = floating 0       0        1       (4 / 5)
  floatMid      :: ManageHook = floating (1 / 6) (1 / 6)  (2 / 3) (2 / 3)

  -- | Function for easier specification of floating windows.
  -- 'RationalRect' takes the following arguments:
  --     x coordinate, y coordinate, width of window, height of window
  floating :: Rational -> Rational -> Rational -> Rational -> ManageHook
  floating x y w h = customFloating $ RationalRect x y w h

  -- | The respective class or instance names.  See Note [ICCCM].
  mailInstName   :: String = "notmuch-scratch"
  todoInstName   :: String = "todo-file"
  termInstName   :: String = "floating-terminal"
  agendaInstName :: String = "org-agenda-day"
  eaInstName     :: String = "emacs-anywhere"
  ledgerInstName :: String = "ledger-mode"

  wezXClass :: String -> X Input
  wezXClass = \s -> pure ((" start --class=" <> s <> " ") <>)

{----------------------------WINDOW BEHAVIOUR----------------------------

Modify behaviour of certain windows based on name/class/etc by setting
up rules.  Also see Note [ICCCM].

It is possible to specify multiple rules per program, as mconcat simply
concatenates the queries.
------------------------------------------------------------------------}

myManageHook :: ManageHook
myManageHook = mconcat
  [ -- Things that should float.
    isDialog            --> doFloat
  , appName =? "telegram-desktop" <&&> title =? "Media viewer" --> doFloat
    -- Shift applications to certain workspaces.
  , className =? "mpv"  --> doShift . fromMaybe tVID =<< liftX (screenWorkspace 1)
    -- Enable named scratchpads.
  , namedScratchpadManageHook =<< liftX myScratchpads
  , checkDock --> doIgnore
  ]

{- Note [ICCCM]
   ~~~~~~~~
@className@ uses the string for the general application class in
@WM_CLASS@ (the second one).

@appName@ uses the string for the particular application instance in
@WM_CLASS@ (the first one).

See: https://tronche.com/gui/x/icccm/sec-4.html#WM_CLASS
-}

{------------------------------KEYBINDINGS-------------------------------

Note that I use my own spin of colemak-ansi-dh (or whatever its name is)
as my keyboard layout, so your mileage on the binds may vary on the
binds.

See: https://gitlab.com/slotThe/dotfiles/-/blob/master/kmonad/config.kbd
------------------------------------------------------------------------}

-- | A keybinding is a key (encoded via the 'EZConfig', Emacs-like
-- encoding), together with an action that executes once that key is
-- pressed.
type Keybinding = (String, X ())

-- | Lots of keybindings.
type Keybindings = [Keybinding]

-- | All of my keybindings.
myKeys :: Keybindings
myKeys = concat
  [ appKeys
  , emacsKeys
  , fnKeys
  , passKeys
  , volumeKeys
  , brightnessKeys
  , windowKeys
  , scratchpadKeys
  , topicKeys
  , layoutKeys
  , screenKeys
  , searchKeys
  ]

-- | Even though I only press super, as this is a keymask the signal we're
-- getting here is a bit muddled.
dockKeys :: X () -> [((KeyMask, KeySym), X ())]
dockKeys act = map (actKey . foldr1 (.|.)) . combinations $ keyMasks
 where
  actKey :: KeyMask -> ((KeyMask, KeySym), X ())
  actKey mask = ((mask, xK_Super_L), act)

  keyMasks :: [KeyMask]
  keyMasks = [ noModMask, shiftMask, controlMask, mod4Mask ]

  -- Return all combinations of a sequence of values.
  combinations :: [a] -> [[a]]
  combinations xs = concat [combs i xs | i <- [1 .. length xs]]
   where
    combs 0 _      = [[]]
    combs _ []     = []
    combs n (x:xs) = map (x:) (combs (n-1) xs) <> combs n xs

-- | Perform the given action on all docks.
onAllDocks :: (Display -> Window -> IO ()) -> X ()
onAllDocks act = withDisplay $ \dpy -> do
  rootw <- asks theRoot
  (_, _, wins) <- io $ queryTree dpy rootw
  traverse_ (io . act dpy) =<< filterM (runQuery checkDock) wins

-- | Launch some applications.
appKeys :: Keybindings
appKeys =
  [ ("M-<Return>", proc inEditor       )
  , ("M-S-i"     , spawn altBrowser    )
  , ("M-["       , spawn browser       )
  , ("M-y"       , spawn "dmenu.sh run")
  , ("M-C-y"     , spawn "xscreensaver-command -lock")
  , ("M-C-c"     , spawn "clipboard"   )
  , ("M-S-<Insert>", withPrefixArgument takeScreenshot)
  ]
 where
  takeScreenshot :: PrefixArgument -> X ()
  takeScreenshot = \case
    Raw 1 -> spawn "scrot -z -u"  -- Focused window
    Raw 2 -> spawn "scrot -z"     -- Entire screen
    -- The mouse movement via @xdotool@ is needed because otherwise,
    -- if unclutter is active, the pointer will remain hidden.  Uff.
    _ -> unGrab
      *> spawn "xdotool mousemove_relative -- -1 0"
      *> spawn "scrot -z -f -s"

-- | Keys that involve talking to Emacs in some way.
emacsKeys :: Keybindings
emacsKeys =
  [ ("M-o c", withPrefixArgument $ (\org -> org "TODO" orgFile) . \case
      Raw _ -> orgPromptPrimary promptNoHist
      _     -> orgPrompt        promptNoHist)
  , ("M-o r", withPrefixArgument $ (\org -> org "IDEA anki:" orgFile) . \case
      Raw _ -> orgPromptPrimary promptNoHist
      _     -> orgPrompt        promptNoHist)
  , ("M-o a", callArXiv "arxiv-citation")
  , ("M-o o", callArXiv "arxiv-citation-download-and-open")
  ]
 where
  orgFile :: String = "repos/org/todos.org"

  callArXiv :: String -> X () = \fun -> do
    url <- getSelection
    proc $ inEmacs
       >-> withEmacsLibs [ ElpaLib "dash", ElpaLib "s", ElpaLib "arxiv-citation"
                         , Special "~/.config/emacs/private-stuff.el" ]
       >-> asBatch
       >-> eval (progn [require "arxiv-citation", fun <> asString url])

-- | Things to do with fn keys.
fnKeys :: Keybindings
fnKeys =
  [ ("M-<F12>" , proc $
      inEditor >-> eval (elispFun "save-buffers-kill-emacs")
        >&&> (toInput "reboot" `orIfPrefixed` toInput "shutdown -h now"))
  ]

-- | Interacting with @pass(1)@.
passKeys :: Keybindings
passKeys =
  [ ("M-p p", passPrompt        promptNoHist)
  , ("M-p t", passTypePrompt    promptNoHist)
  , ("M-p o", passOTPTypePrompt promptNoHist)
  ]

-- | Thank you EZConfig for making this very easy to do.
volumeKeys :: Keybindings
volumeKeys =
  [ ("<XF86AudioRaiseVolume>", volume "set-volume" "10%+"  )
  , ("<XF86AudioLowerVolume>", volume "set-volume" "10%-"  )
  , ("<XF86AudioMute>"       , volume "set-mute"   "toggle")
  ]
 where
  volume :: String -> String -> X ()
  volume what how = spawn $ "wpctl " <> what <> " @DEFAULT_AUDIO_SINK@ " <> how

-- | I guess this is my life now.
brightnessKeys :: Keybindings
brightnessKeys =
  [ ("<XF86MonBrightnessUp>"  , spawn "brightnessctl s 1%+")
  , ("<XF86MonBrightnessDown>", spawn "brightnessctl s 1%-")
  ]

-- | Keybindings for manipulating windows.
windowKeys :: Keybindings
windowKeys =
  [ ("M-c", kill)  -- Kill the focused window.
    -- Moving focus.
  , ("M-n"  , windows W.focusDown  )  -- Next window
  , ("M-e"  , windows W.focusUp    )  -- Prev window
  , ("M-m"  , windows W.focusMaster)
  , ("M-."  , selectWindow emConfig >>= (`whenJust` windows . W.focusWindow))
  , ("M-C-[", toggleFocus)

    -- See Note [Swapping Master].
  , ("M-S-m", whenX (swapHybrid True) dwmpromote)
  , ("M-C-n", windows W.swapDown)  -- Swap with next
  , ("M-C-e", windows W.swapUp  )  -- Swap with prev

    -- Rotate all of the unfocused windows in either direction.
  , ("M-C-.", rotUnfocusedUp  )
  , ("M-C-,", rotUnfocusedDown)

    -- Resizing the master/stack ratio.
  , ("M-k", sendMessage Shrink)
  , ("M-i", sendMessage Expand)

    -- Changing the size of stack windows.
  , ("M-C-h", sendMessage MirrorShrink)
  , ("M-C-u", sendMessage MirrorExpand)

    -- Bringing and going-to windows.
  , ("M-;", windowPrompt promptNoHist{ autoComplete = (20 `ms`) } Goto  allWindows)
  , ("M-j", windowPrompt promptNoHist                             Bring allWindows)
  ]
 where
  -- | Config for X.A.EasyMotion.
  emConfig :: EasyMotionConfig
  emConfig = def
    { sKeys = AnyKeys
        [ xK_t, xK_n, xK_s, xK_e  -- home row index and middle finger, alternating
        , xK_d, xK_h              -- index finger curl
        , xK_h, xK_u              -- middle finger stretch
        , xK_r, xK_i, xK_a, xK_o  -- rest of home row, alternating
        ]
    , cancelKey = xK_Escape
    , emFont    = "xft:" <> inIosevka 80
    , borderPx  = 0
    }

{- Note [Swapping Master]
   ~~~~~~~~~~~~~~~~~~~~~~
This combines the functionality of "X.A.DwmPromote" and
"X.A.SwapPromote".  If the history is empty, pressing the keybinding
while having the master window focused will swap it with the first
window in the stack.  If the history is not empty, then invoking the
function (again while having the master window in focus) will swap the
master with the window that had master before.  Deleting a window causes
its history to be deleted as well.  The promotion of non-master windows
continues to work as expected.

'swapHybrid' ignores any non-focused floating windows.  This is to
prevent scratchpads from destroying the master history.
-}

-- | Scratchpads.  Note: The first argument to 'namedScratchadAction' is
-- now unused; hence, replace it with the empty list.
scratchpadKeys :: Keybindings
scratchpadKeys =
  [ ("M-C-t", namedScratchpadAction [] "Mail"  )
  , ("M-C-f", namedScratchpadAction [] "Todos" )
  , ("M-C-d", namedScratchpadAction [] "Term"  )
  , ("M-C-a", namedScratchpadAction [] "Agenda")
  , ("M-C-p", namedScratchpadAction [] "EA"    )
  , ("M-C-l", namedScratchpadAction [] "Ledger")
  ]

-- | Keys to deal with topicspaces
topicKeys :: Keybindings
topicKeys =
  [ ("M-a"          , currentTopicAction topicConfig)
    -- Spawn things in the topic directory.
  , ("M-<Backspace>", spawnEshellInTopic `orIfPrefixed` spawnTermInTopic)
  -- Go or shift to some arbitrary workspace (or both).
  , ("M-g"          , promptedGoto)
    -- Access the two most recently used topics.
  , ("M-C-<Space>"  , toggleTopic `orIfPrefixed` shiftToLastTopic)
  ] <>
  -- The following does two things:
  --   1. Switch topics (no modifier).
  --   2. Move focused window to topic N (shiftMask).
  [ ("M-" <> m <> k, f i)
  | (i, k) <- zip (topicNames topics) wsKeys
  , (f, m) <- [(goto, ""), (windows . W.shift, "S-")]
  ]
 where
  -- Grave (`) key is left of the "1" key on my keyboard, making it
  -- ideal for a 0th workspace.
  wsKeys :: [String]
  wsKeys = "`" : map (show @Int) [1 .. 9]

-- | Layouts get a [l]ayout submap.
layoutKeys :: Keybindings
layoutKeys =
  [ ("M-l t", switchToLayout "Tall"    )
  , ("M-l f", switchToLayout "Full"    )
  , ("M-l 2", switchToLayout "TwoPane" )
  , ("M-l 3", switchToLayout "ThreeCol")
  , ("M-l c", switchToLayout "Hacking" )
  ]

-- | Keys for switching screens.  Much like when switching workspaces,
-- this also shifts on shift.
screenKeys :: Keybindings
screenKeys
  = ("M-=", windows $ W.greedyView =<< W.tag . W.workspace . head . W.visible)
  : [ ("M-" <> m <> k, screenWorkspace sc >>= (`whenJust` windows . f))
    | (k, sc) <- zip ["w", "f"] [0 ..]
    , (f, m ) <- [(W.view, ""), (W.shift, "S-")]
    ]

-- | Search commands.
searchKeys :: Keybindings
searchKeys =
  [ ("M-s", withPrefixArgument $ submap . searchEngineMap . \case
      Raw _ -> selectSearchBrowser
      _     -> \br se -> promptSearchBrowser' (decidePrompt se) br se)
  , ("M-C-s", safeSpawn "browser-new-window.sh" ["https://www.perplexity.ai/"])
  ]
 where
  -- | Some search engines get a modified prompt.
  decidePrompt :: SearchEngine -> XPConfig
  decidePrompt se
    | se `elem` [arXiv, clojureDocs, cratesIo, hoogle, nixos, noogle, rustStd, searx, wikipedia, zbmath, github, youtube]
    = promptNoHist
    | se `elem` [reddit, git]
    = prompt{ autoComplete = (5 `ms`) }
    | otherwise
    = prompt

  -- | Open searches, possibly in a new window.
  searchEngineMap :: (Browser -> SearchEngine -> X ()) -> Map (KeyMask, KeySym) (X ())
  searchEngineMap searchIn = basicSubmapFromList
    -- Werk
    [ (xK_a, sw arXiv    )
    , (xK_z, sw zbmath   )
    , (xK_w, nw wikipedia)
    -- Wasting my time
    , (xK_y, sw youtube  )
    -- Misc
    , (xK_s, sw searx    )
    , (xK_u, sw url      )
    , (xK_t, calcPrompt  )
    -- Programming
    , (xK_g, submap $ basicSubmapFromList
              [ (xK_g, sw' git)
              , (xK_h, sw' github)
              ])
    , (xK_c, sw  clojureDocs)
    , (xK_h, sw  hoogle)
    , (xK_n, submap $ basicSubmapFromList
              [ (xK_n, sw noogle)
              , (xK_p, sw nixos)
              ])
    , (xK_r, submap $ basicSubmapFromList
               [ (xK_c, sw cratesIo)
               , (xK_r, sw rustStd )
               , (xK_u, sw rustUnstableBook)
               ])
    , (xK_m, manPrompt promptNoHist{ searchPredicate = searchPredicate def
                                   , sorter          = sorter def
                                   })
    , (xK_o, sw' openstreetmap)
    ]
   where
    -- | Same window, new window.
    sw, sw', nw :: SearchEngine -> X ()
    sw  = searchIn browser
    nw  = searchIn "browser-new-window.sh"
    sw' = searchIn altBrowser

  -- | Search engines not in X.A.Search.
  reddit = searchEngine  "reddit" "https://old.reddit.com/r/"
  searx  = searchEngine  "searx"  "https://searx.dresden.network?q="
  url    = searchEngineF "url"    \s -> if "https://" `isPrefixOf` s then s else "https://" <> s
  git    = searchEngineF "repo"   \s -> if
    | s `elem` ["irc-bot", "dotfiles"]
      -> "https://gitlab.com/slotThe/" <> s
    | s `elem` ["kbd-mode", "kmonad", "kmonad-contrib"]
      -> "https://github.com/kmonad/" <> s
    | s `elem` ["x11", "x11-xft", "xmonad", "xmonad-contrib", "xmonad-docs", "xmonad-web"]
      -> "https://github.com/xmonad/" <> s
    | s `elem` ["vc-use-package", "arXiv-citation", "hmenu", "slotThe.github.io", "query-replace-many", "advent", "change-env", "rq"]
      -> "https://github.com/slotThe/" <> s
    | s == "slotThe"    -> "https://github.com/slotThe/"
    | s == "xmobar"     -> "https://codeberg.org/jao/xmobar"
    | s == "nixpkgs"    -> "https://github.com/nixos/nixpkgs"
    | s `elem` ["emacs-overlay", "home-manager"]
      -> "https://github.com/nix-community/" <> s
    | s == "anki-editor"-> "https://github.com/anki-editor/anki-editor"
    | otherwise         -> "https://github.com/search?q=" <> s
  rustUnstableBook = searchEngine "unstable-book" "https://doc.rust-lang.org/beta/unstable-book/?search="

-- | This is an application, we're allowed to define orphan instances :)
instance Eq {- ORPHAN -} SearchEngine where
  (==) :: SearchEngine -> SearchEngine -> Bool
  (SearchEngine n _) == (SearchEngine n' _) = n == n'

-- N.b.: I wish there was an ORPHAN pragma, so I didn't have to set
--       @-Wno-orphans@ globally.

-------------------------------------------------------------------------
-- PROMPT
-------------------------------------------------------------------------

-- | Create a graphical prompt for xmonad that functions can use.
prompt :: XPConfig
prompt = def
  { fgColor           = colorFg
  , fgHLight          = colorBg
  , bgColor           = colorBg
  , bgHLight          = colorCyan
  , font              = fonts [inIosevka 10, inFontAwesome 9]
  , alwaysHighlight   = True                 -- Current best match
  , height            = 25
  , position          = Top
  , promptBorderWidth = 0                    -- Fit in with rest of config
  , historySize       = 50
  , historyFilter     = deleteAllDuplicates
  , maxComplRows      = Just 5               -- Max rows to show in completion window
  , promptKeymap      = myXPKeyMap
  , searchPredicate   = fuzzyMatch
  , sorter            = fuzzySort
  , completionKey     = (0, xK_Right)
  , prevCompletionKey = (0, xK_Left)
  }
 where
  myXPKeyMap :: Map (KeyMask, KeySym) (XP ())
  myXPKeyMap = mconcat
    [ fromList [ ((controlMask, xK_w), killWord' isSpace Prev) ]
    , emacsLikeXPKeymap
    ]

-- | I really don't want a history for some things; just clutters up the
-- @promptHistory@ file.
promptNoHist :: XPConfig
promptNoHist = prompt { historySize = 0 }

-------------------------------------------------------------------------
-- UTIL
-------------------------------------------------------------------------

fonts :: [String] -> String
fonts = ("xft:" <>) . intercalate ","

inIosevka :: Int -> String
inIosevka size = "Hopf Mono-" <> show size

inFontAwesome :: Int -> String
inFontAwesome size = "FontAwesome-" <> show size

-- | Default browser opens in new tab.
browser :: Browser
browser = "firefox"

-- | Alternative browser.
altBrowser :: Browser
altBrowser = "alt-firefox"

-- | Express the given time in milliseconds as a time in microseconds,
-- ready for consumption by @autoComplete@.
ms :: Int -> Maybe Int
ms = Just . (* 10^(4 :: Int))

-- | Switch to a certain layout.
switchToLayout :: String -> X ()
switchToLayout = sendMessage . JumpToLayout

-- | Create a basic (i.e. there is no additional 'KeyMask' to consider)
-- submap from a list of @(key, action)@ pairs.
basicSubmapFromList :: Ord key => [(key, action)] -> Map (KeyMask, key) action
basicSubmapFromList = fromList . map \(k, a) -> ((0, k), a)

-------------------------------------------------------------------------
-- CALC: a live-updating calculator prompt
-------------------------------------------------------------------------

data CalcPrompt = CalcPrompt

instance XPrompt CalcPrompt  where
  showXPrompt :: CalcPrompt -> String
  showXPrompt CalcPrompt = "calc: "

  commandToComplete :: CalcPrompt -> String -> String
  commandToComplete _ = id

calcPrompt :: X ()
calcPrompt =
  mkXPrompt CalcPrompt
            promptNoHist{ font   = fonts [inIosevka 20, inFontAwesome 18]
                        , height = 50
                        , completionKey     = (0, xK_Tab)
                        , prevCompletionKey = (shiftMask, xK_Tab)
                        }
            complFun
            (const $ pure ())
 where
  complFun :: ComplFunction
  complFun inp = ((:[]) . trim) <$>
    runProcessWithInput "sh" [] ("echo '" ++ inp ++ "' | bc --mathlib")

  trim :: String -> String
  trim = f . f where f = reverse . dropWhile isSpace

--- Local Variables:
--- mode: haskell
--- eval: (setopt haskell-process-path-ghci (concat (or (getenv "XMONAD_GHC") "ghc") "i"))
--- End:
